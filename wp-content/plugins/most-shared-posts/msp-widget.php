<?php
/*

Most Shared Posts plugin

This file contains the code for the widget. It is required by both the admin side and the front end.

*/

// Add our hook on init to load the widget
add_action( 'widgets_init', 'load_most_shared_posts_widget' );

// Standard register widget function
function load_most_shared_posts_widget() {
	register_widget( 'Most_Shared_Posts' );
}


/**
 * Most_Shared_Posts class.
 *
 */
class Most_Shared_Posts extends WP_Widget {

	private $recency_limit; 

	/**
	 * Widget setup.
	 */
	function Most_Shared_Posts() {
		global $wp_version;
		
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'most-shared-posts', 'description' => 'Showcases your most shared posts to your visitors in your blog\'s sidebar.' );

		/* Widget control settings. */
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'toma_msp' );

		/* Create the widget. */
		$this->WP_Widget( 'toma_msp', 'Most Shared Posts', $widget_ops, $control_ops );
		
		if (version_compare($wp_version,"2.8","<"))
		{
			exit ("Most Shared Posts requires Wordpress version 2.8 or later. Please update Wordpress. :)");
		}
	}
		
	function within_recency_limit( $where = '' ) {
		
		$where .= " AND post_date >= '" . date('Y-m-d', strtotime('-' . $this->recency_limit .' days')) . "'";
		
		return $where;
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */

		// Setup our recency limit for this widget instance
		$recency_limit_unit = isset( $instance['recency_limit_unit'] ) ? $instance['recency_limit_unit'] : 2;
		
		$this->recency_limit = 730;
		
		switch($recency_limit_unit)
		{
			case "days":
				$this->recency_limit = intval($instance['recency_limit_number']);
				break;
			case "months":
				$this->recency_limit = intval($instance['recency_limit_number']) * 31;
				break;
			case "years":
				$this->recency_limit = intval($instance['recency_limit_number']) * 365;
				break;
			default:
				$this->recency_limit = 730;
				break;
		}
		
		// If something went wrong then set 2 years as a fallback.
		if ($this->recency_limit <= 0)
			$this->recency_limit = 730;
		
		// Read options on which networks to include
					
		/*
        $include_fb_count = (get_option('toma_msp_include_fb') == 'on') ? true : false;
		$include_twitter_count = (get_option('toma_msp_include_twitter') == 'on') ? true : false;
		$include_google_count = (get_option('toma_msp_include_google') == 'on') ? true : false;
		$suppress_icons = (get_option('toma_msp_suppress_icons') == 'on') ? true : false;
		$h3_wrap = (get_option('toma_msp_h3_wrap') == 'on') ? true : false;
		$attribution_link = (get_option('toma_msp_attribution_link') == 'on') ? true : false;
		*/
		
		
		// Read the option on font-size
		
		$css_class_font = '';
		
		/*switch(get_option('toma_msp_font_size'))
		{
			case "smaller":
				$css_class_font = "share-counts-smaller";
				break;
			case "standard":
				$css_class_font = "";
				break;
			case "bigger":
				$css_class_font = "share-counts-bigger";
				break;
			case "even-bigger":
				$css_class_font = "share-counts-even-bigger";
				break;
			case "huge":
				$css_class_font = "share-counts-huge";
				break;
			default:
				$css_class_font = "share-counts-bigger";
				break;
		}*/
		
		// Read the option on icon-size
		
		$icon_pixel_size = 16;
		
		/*switch(get_option('toma_msp_icon_size'))
		{
			case "smaller":
				$icon_pixel_size = 12;
				break;
			case "standard":
				$icon_pixel_size = 16;
				break;
			case "bigger":
				$icon_pixel_size = 20;
				break;
			case "huge":
				$icon_pixel_size = 25;
				break;
			default:
				$icon_pixel_size = 16;
				break;
		}
		*/
		
		
		// Read the option on how many posts to display.
		$number_of_posts = intval($instance['number_of_posts_to_list']);

		if ($number_of_posts <= 0)
			$number_of_posts = 5;
		
		// Setup and run the query for getting the list of posts to show
		$args = array(
			'posts_per_page' => $number_of_posts,
			'orderby' => 'meta_value_num',
		    'meta_key' => '_msp_total_shares',
			'order' => 'DESC'
		);
		
		// Add the filter here to get only those
		// within the setting of how far back
		// to check.
		add_filter( 'posts_where', array($this, 'within_recency_limit') );
		
		$posts_in_range = new WP_Query( $args );
		
		
		// Start the loop to loop over each post we are going to
		// list in the widget.

        $pos = $instance['pos'];
		
		//echo "<!-- Begin loop for showing. -->";
        if ($pos == 'right') {
            if ( $title )
                echo $before_title . '<a href="'.get_home_url().'/chia-se-nhieu-nhat" >'.$title.'</a>'. $after_title;
            $count = 1;

            echo '<ul>';

            while ( $posts_in_range->have_posts() ) : $posts_in_range->the_post();
                $title_post = get_the_title();
                if ($count == 1) :
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'medium' );
                    $format_post = get_post_format(get_the_ID());
                    ?>
                    <li>
                        <a class="thumb resize resize-1" href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                            <?php echo echoSpan($format_post); ?>
                        </a>
                        <h4><a href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count></span></h4>
                    </li>
                <?php else : ?>
                    <li>
                        <h4><a href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>">​<?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count></span></h4>
                    </li>
                <?php endif;

                if ($count == $number_of_posts)
                    break;
                else
                    $count++;

                //echo "<!-- Next post -->";

                /*$fb_likes = get_post_meta(get_the_ID(), "_msp_fb_likes", true);
                $tweets = get_post_meta(get_the_ID(), "_msp_tweets", true);
                $plusones = get_post_meta(get_the_ID(), "_msp_google_plus_ones", true);
                $totals = get_post_meta(get_the_ID(), "_msp_total_shares", true);


                //echo "<!-- Likes fetched as = " . $fb_likes . " -->";
                //echo "<!-- Tweets fetched as = " . $tweets . " -->";
                //echo "<!-- PlusOnes fetched as = " . $plusones . " -->";
                //echo "<!-- Totals fetched as = " . $totals . " -->";

                echo '<li>';

                if ($h3_wrap)
                    echo '<h3 class="post-title" >';

                echo '<a href="' . get_permalink() . '" rel="bookmark">' . get_the_title() . '</a>';

                if ($h3_wrap)
                    echo '</h3>';

                //echo '<span class="date">' . get_the_date() . '</span>';

                if (!$suppress_icons)
                {
                    echo '<div class="share-counts ' . $css_class_font . '">';



                    if ($include_google_count)
                    {
                    echo '<img src="' . plugins_url('google_icon.png', __FILE__) . '" width="'.$icon_pixel_size.'px" height="'.$icon_pixel_size.'px" title="Google +1s" alt="Google +1 logo" />' . $plusones;

                    echo " &nbsp; ";
                    }


                    if ($include_twitter_count)
                    {
                    echo '<img src="' . plugins_url('twitter_icon.png', __FILE__) . '" width="'.$icon_pixel_size.'px" height="'.$icon_pixel_size.'px" title="Tweets" alt="Twitter logo" />' . $tweets;

                    echo " &nbsp; ";
                    }

                    if ($include_fb_count)
                    {
                    echo '<img src="' . plugins_url('facebook_icon.png', __FILE__) . '" width="'.$icon_pixel_size.'px" height="'.$icon_pixel_size.'px" title="Facebook shares" alt="Facebook logo" />' . $fb_likes;
                    }

                    //echo '<img src="' . plugins_url('shares_icon.png', __FILE__) . '" width="12px" height="12px" />' . $totals;


                    echo '</div>';
                }

                echo '</li>';
                */

            endwhile;
            // End loop over ther posts to show.

            echo '</ul>';
        }

        if ($pos == 'left') {
            if ($posts_in_range->have_posts()): ?>
                <ol class="breadcrumb">
                    <li class="active"><?php echo $title; ?></li>
                </ol>
                <?php
                $count = 1;
                while ($posts_in_range->have_posts()) : $posts_in_range->the_post();
                    //$posts->the_post();
                    //$title_post = $post->post_title;
                    $title_post = get_the_title();
                    $excerpt = stringTrunc(get_the_excerpt(), 35);
                    $format_post = get_post_format();
                    //$url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                    $link = get_permalink ();
                    //$tag = the_tags( '<a>', '</a>, <a>', '</a>' );
                    //$date = date('d.m.Y', strtotime($post->post_date));
                    //$date = the_time('j.m.Y');
                    ?>
                    <?php if ($count == 1):
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large' );
                        ?>
                        <div class="highlight-top row">
                            <article class="art-feature cate-inside">
                                <a class="resize" href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    <?php echo echoSpan($format_post); ?>
                                </a>
                                <div class="detail">
                                    <a class="type" href="#" title="">Chuyện trong nhà</a>
                                    <h2><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span></h2>
                                </div>
                            </article>
                        </div>
                    <?php else:
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                        ?>
                        <?php if ($count == 2): ?>
                            <div class="highlight-ctn row">
                            <!--<div id="custom_list">-->
                        <?php endif; ?>
                        <input type="hidden" value="<?php echo $number_of_posts; ?>" class="page_limit"/>
                        <article class="art">
                            <a class="resize" href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                <?php echo echoSpan($format_post); ?>
                            </a>
                            <h2><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h2>
                            <ul class="tool-bar list-inline">
                                <li><span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span></li> |
                                <li class="date"><?php echo the_time('d.m.Y'); ?> GMT + 7</li>
                            </ul>
                            <p><?php echo $excerpt; ?></p>
                            <div class="tag-name">
                                <i class="fa fa-tags"></i>
                                <?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?>
                            </div>
                        </article>
                    <?php endif; ?>
                    <?php
                    if ($count == $number_of_posts) {
                        break;
                    } else {
                        $count++;
                    }
                endwhile;
                wp_reset_postdata();
                        ?>
                </div>
                <?php
                    $max = intval($posts_in_range->max_num_pages );
                    if ($max < 0) { // check if the max number of pages is greater than 1 ?>
                        <nav class="pagging">
                            <ul class="pagination pagination-sm">
                                <!--<li>
                                    <a href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li> -->
                                <?php
                                $number_page = $max < 6 ? $max : 5;
                                for($i=1; $i <= $number_page; $i++) { ?>
                                    <li><a href="javascript:void(0);" class="page_number"><?php echo $i; ?></a></li>
                                <?php } ?>
                                <?php if ($number_page < $max): ?>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    <?php }
                    // wp_reset_query();
                    ?>

<!--                </div>-->
            <?php
            endif;
        }

		
		/*if ($attribution_link)
		{
			echo "<small>Plugin by <a href='http://www.tomanthony.co.uk/wordpress-plugins/most-shared-posts/'>Tom Anthony</a></small>";
		}*/
		
		wp_reset_postdata();
		
		// Remove the date range filter.
		remove_filter('posts_where', array($this, 'within_recency_limit'));

		echo $after_widget;
	}

	// Update the settings for a particular instance
	// of the widget. This is separate to the global
	// settings that apply to all our widgets.
	function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['recency_limit_unit'] = strip_tags( $new_instance['recency_limit_unit'] );
		$instance['recency_limit_number'] = intval( $new_instance['recency_limit_number'] );
		$instance['number_of_posts_to_list'] = intval( $new_instance['number_of_posts_to_list'] );
        $instance['pos'] = strip_tags( $new_instance['pos'] );

		return $instance;
	}

	// Display the form with the options for an
	// instance of the widget.
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 'title' => 'Most Shared Posts', 'recency_limit_unit' => 'years', 'recency_limit_number' => 2, 'number_of_posts_to_list' => 5, 'pos' => 'right');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>
	
        <p>
        	<label for="<?php echo $this->get_field_id( 'recency_limit_number' ); ?>">Thời gian filter tin bài:</label><br />
        	 
        	<input type="text" name="<?php echo $this->get_field_name( 'recency_limit_number' ); ?>" value="<?php echo $instance['recency_limit_number']; ?>" id="<?php echo $this->get_field_id( 'recency_limit_number' ); ?>" size="4" /> 

		    <select name="<?php echo $this->get_field_name( 'recency_limit_unit' ); ?>">
		                  <option value="days" <?php if ($instance['recency_limit_unit'] == 'days') { echo "selected=\"selected\""; } ?>>
		                    days
		                  </option>
		                  <option value="months" <?php if ($instance['recency_limit_unit'] == 'months') { echo "selected=\"selected\""; } ?>>
		                    months
		                  </option>
		                  <option value="years" <?php if ($instance['recency_limit_unit'] == 'years') { echo "selected=\"selected\""; } ?>>
		                    years
		                  </option>
	                </select>
	    </p>
	    
        <p>
        	<label for="<?php echo $this->get_field_id( 'number_of_posts_to_list' ); ?>">Số tin hiển thị:</label><br />
        	 
        	<input type="text" name="<?php echo $this->get_field_name( 'number_of_posts_to_list' ); ?>" value="<?php echo $instance['number_of_posts_to_list']; ?>" size="4" />
	    </p>

        <p>
            <label for="<?php echo $this->get_field_id( 'pos' ); ?>"><?php _e('Vị trí hiển thị:', 'hybrid'); ?></label>
            <input id="<?php echo $this->get_field_id( 'pos' ); ?>" name="<?php echo $this->get_field_name( 'pos' ); ?>" value="<?php echo $instance['pos']; ?>" style="width:100%;" />
        </p>
	<?php
	}
}




?>
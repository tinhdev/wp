/**
 * @name        :   crawler.js
 * @author      :   PhongTX
 * @copyright   :   Fpt Online
 * Date: 10/04/2015
 * Time: 08:45
 */
$(document).ready(function () {
    $("#crawler").on('click', 'button[type=submit]', function (e) {
        $('#crawler').validate({
            errorPlacement: function (error, element) {
                element = element.parents('div.form-group').length == 1 ? element.parents('div.form-group') : element;
                error.insertAfter(element);
            },
            errorElement: "div",
            rules: {
                strLink: {
                    required: true
                }
            },
            messages: {
                strLink: {
                    required: 'Hãy nhập link crawler data. (VD: http://vnexpress.net/tin-tuc/the-gioi/nha-trang-khong-duoc-bao-truoc-viec-fbi-dieu-tra-email-cua-clinton-3491162.html)'
                }
            },
            submitHandler: function (form) {
                var strLink = $('#strLink').val();
                bootbox.confirm("Bạn muốn crawler data từ ["+strLink+"]?", function (result) {
                    if (result == true) {
                        location.href = 'http://123dzo.net/wp-admin/post-new.php?link=' + encodeURIComponent(strLink);
                    }
                });
                return false;
            }
        });
    });
});
<?php
/**
 * Plugin Name: Crawl data
 * Plugin URI:
 * Description: Crawler data
 * Version: 1.0
 * Author: PhongTX
 * Author URI: https://www.facebook.com/phongtx
 * Date: 29/10/2016
 * Time: 3:17 CH
 */
class Crawl
{
    private $_name;
    private static $instance;
    static function GetInstance()
    {
        if (!isset(self::$instance))
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function CrawlMenu()
    {
        $this->_name = add_menu_page(
            'Crawl data',
            'Crawl data',
            'manage_options',
            __FILE__,
            array($this, 'RenderPage')
        );
    }
    public function RenderPage(){
        ?>
        <div class='wrap'>
            <h1 class="box-title">Crawl data theo link chi tiết</h1>
            <form id="crawler" enctype="multipart/form-data" accept-charset="utf-8" method="post" action="" role="form">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body" style="margin-bottom: 10px;">
                            <div class="alert alert-warning p-b-10" role="alert" style="margin-bottom: 0px;">Các website mà bạn có thể crawler data: *.vnexpress.net, ngoisao.net, diadiemanuong.com, foody.vn, ivivu.com</div>
                        </div>
                        <div class="box-body" style="margin-bottom: 30px;">
                            <div class="form-group frm-validate">
                                <div class="row">
                                    <div class="col-md-9">
                                        <label for="strLink">Link</label>
                                        <input name="strLink" type="text" size="150" value="" class="form-control" id="strLink">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="button button-primary">Cập nhật</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Bootstrap 3.3.5 -->
            <link href="http://123dzo.net/wp-content/plugins/crawl/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="http://123dzo.net/wp-content/plugins/crawl/css/main.css" rel="stylesheet" type="text/css" />
            <!-- jQuery 2.1.4 -->
            <script src="http://123dzo.net/wp-content/plugins/crawl/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
            <!-- Bootstrap 3.3.2 JS -->
            <script src="http://123dzo.net/wp-content/plugins/crawl/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="http://123dzo.net/wp-content/plugins/crawl/js/jquery.validate.min.js" type="text/javascript"></script>
            <script src="http://123dzo.net/wp-content/plugins/crawl/js/bootbox.min.js" type="text/javascript"></script>
            <script src="http://123dzo.net/wp-content/plugins/crawl/js/link-detail.js"></script>
        </div>
    <?php
    }

    public function InitPlugin()
    {
        add_action('admin_menu', array($this, 'CrawlMenu'));
    }
}
$crawl = Crawl::GetInstance();
$crawl->InitPlugin();
?>
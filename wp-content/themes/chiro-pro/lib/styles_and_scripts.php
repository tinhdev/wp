<?php

function chiropro_scripts() {
	wp_enqueue_style( 'chiropro-style', get_stylesheet_uri() );

	wp_enqueue_script( 'menu-dropdown', get_template_directory_uri() . '/js/menu-dropdown.js', array('jquery'), '20150422', true );	
	
	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
}
add_action( 'wp_enqueue_scripts', 'chiropro_scripts' );
<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Dzo
 * @since Dzo 1.0
 * @autho Hoanvo <vvhoan2004@gmail.com>
 */
$nickname = isset(get_post_meta(get_the_ID())['nick-name'][0]) ? get_post_meta(get_the_ID())['nick-name'][0] : get_the_author();
$original_link = isset(get_post_meta(get_the_ID())['original-link'][0]) ? get_post_meta(get_the_ID())['original-link'][0] : '';
?>

<section class="content col-sm-8">
    <ol class="breadcrumb">
        <?php the_breadcrumb(); ?>
    </ol>
    <article class="fck">
        <header>
            <h1><?php echo get_the_title()?></h1>
            <div class="bar">
                <span><?php echo $nickname;?>  '  <?php echo the_time('d.m.Y H:i'); ?> GMT+7'</span>
                <span class="txt-cm" href="#comment"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span>
            </div>
            <p class="bold"><?php echo get_the_excerpt(); ?></p>
        </header>
        <section>
            <?php
            the_content();
            ?>
        </section>
        <footer>
            <p><?php echo $nickname;?></p>
        </footer>
        <ul class="outer-tag list-inline">
            <li><a href="javascript:void(0)" title=""><i class="fa fa-tags"></i> Tags</a></li>
            <?php the_tags( '<li>', '</li><li>', '</li>' ); ?>
        </ul>
        <ul class="list-share">
            <li><i class="fa fa-share-alt"></i> chia sẻ</li>
            <li><a id="m-share" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(get_the_ID()); ?>&display=popup&ref=plugin" title="Share"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-share.png"></a></li>
            <li><i class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></i></li>
        </ul>
        <?php if ($original_link != ""): ?>
            <div class="block-link">
                <span>Nguồn</span>: <?php echo $original_link; ?>
            </div>
        <?php endif; ?>
    </article>
    <!--<div class="block-connection">
        <h3><a href="#" title="">TIN LIÊN QUAN</a></h3>
        <div class="slider-3">
            <div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-1.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Nông dân thuê... hợp tác xã làm ruộng</a></h4></div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-2.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Hải quân Việt Nam kỷ niệm 60 năm ngày thành lập</a></h4></div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-4.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Tuyển ứng viên đi học nước ngoài theo đề án Chính phủ</a></h4></div>
            </div>
            <div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-5.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Nông dân thuê... hợp tác xã làm ruộng</a></h4></div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-6.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Hải quân Việt Nam kỷ niệm 60 năm ngày thành lập</a></h4></div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-7.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Tuyển ứng viên đi học nước ngoài theo đề án Chính phủ</a></h4></div>
            </div>
            <div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-8.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Nông dân thuê... hợp tác xã làm ruộng</a></h4></div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-9.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Hải quân Việt Nam kỷ niệm 60 năm ngày thành lập</a></h4></div>
                <div class="inner-slider"><a class="resize resize-3" href="#" title=""><img style="background-image: url(images/photo/img-10.jpg)" alt="" src="images/transparent.png"></a><h4><a href="#" title="">Tuyển ứng viên đi học nước ngoài theo đề án Chính phủ</a></h4></div>
            </div>
        </div>
    </div> -->
    <p class="noted-cm">Bình luận sẽ bị <span>XÓA</span> nếu nội dung chứa từ ngữ thô tục, chóng phá, đi ngược thuần phong mỹ tục</p>
    <div class="highlight block-comment" id="comment">
        <div class="fb-comments" data-href="<?php echo get_the_permalink(); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
    </div>
</section>
<?php updateView(get_the_ID()); ?>

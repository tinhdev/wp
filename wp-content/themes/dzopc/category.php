<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();
?>
    <div class="main container">

        <?php dynamic_sidebar( 'sidebar-1' ); ?>

	</div><!-- #container -->

<?php
get_footer();

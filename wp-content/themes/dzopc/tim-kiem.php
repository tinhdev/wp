<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
<?php
    $keyword = $_GET['s'];
    $date = isset($_GET['date']) ? str_replace('-', '/', $_GET['date']) : date('d/m/Y');
    $category = isset($_GET['cat']) ? $_GET['cat'] : '';
    $type = isset($_GET['type']) ? $_GET['type'] : 1;
?>
<div class="container">
    <div class="row">
        <section class="content col-sm-8">
            <ol class="breadcrumb">
                <li class="active">Tìm kiếm</li>
            </ol>
            <div class="highlight highlight-top">
                <div class="outer-search-1">

                    <form role="search" class="form-inline frm-search-1" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="form-group">
                            <input type="text" name="s" id="s" class="form-control txt-tk" placeholder="Nội dung cần tìm" value="<?php echo $keyword; ?>"/>
                        </div>
                        <button type="submit" class="btn btn-danger">Tìm kiếm</button>
                        <div class="checkbox">
                            <label>
                                <input type="radio" name="type" value="1" <?php echo $type == 1 ? "checked" : ""; ?> > Tìm tất cả
                            </label>
                            <label>
                                <input type="radio" name="type" value="2" <?php echo $type == 2 ? "checked" : ""; ?> > Tìm theo tiêu đề
                            </label>
                            <label>
                                <input type="radio" name="type" value="3" <?php echo $type == 3 ? "checked" : ""; ?> > Tìm theo nội dung
                            </label>
                        </div>
                        <select name="cat" class="form-control">
                            <option value="0">Tất cả chuyên mục</option>
                            <?php
                            $cats = get_categories();
                            foreach($cats as $cat) {
                                if ($cat->slug == $category): ?>
                                    <option value="<?php echo $cat->slug; ?>" selected><?php echo $cat->cat_name; ?></option>
                                <?php else: ?>
                                    <option value="<?php echo $cat->slug; ?>"><?php echo $cat->cat_name; ?></option>
                                <?php endif; ?>
                            <?php } ?>
                        </select>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input name="date" type='text' class="form-control" value="<?php echo $date; ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>


                    </form>

                </div>
                <div class="highlight-ctn row">
                    <div id="custom_list">
                        <?php
                        if (isset($_GET['s']) && ($_GET['s'] == '')
                            && isset($_GET['type']) && ($_GET['type'] == '')
                            && isset($_GET['date']) && ($_GET['date'] == '') )
                        { ?>
                            <h1 class="pagetitle">0 kết quả</h1>
                        <?php
                        } else {
//                            $keyword = preg_replace('/^-|-$|[^-a-zA-Z0-9]/', '', $_GET['s']);
//                            $cat = isset($_GET['cat']) ? preg_replace('/[^0-9]/', '', $_GET['cat']) : '';
//                            add_filter('posts_where', 'filter_where_wpa89154');
//                           // apply_filters('posts_where', 'filter_where_wpa89154');
//                            //sleep(5);
//                            $args = array(
//                                'posts_per_page'  => 4,
//                                'post_type'       => 'post',
//                               // 's' => $keyword,
//                                'post_status'     => 'publish',
//                               // 'category_name' => $cat,
//                                'suppress_filters' => false
//                            );
//                            $posts = get_posts($args);
//                            remove_filter('posts_where', 'filter_where_wpa89154');



                            $postlist = mysearch();
                            global $post;
                            foreach ($postlist as $post):
                            //while ($postlist -> have_posts()) : $postlist -> the_post();
                               setup_postdata($post);
                                $title_post = get_the_title();
                                $excerpt = stringTrunc(get_the_excerpt(), 35);
                                $format_post = get_post_format(get_the_ID());
                                $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                                $cat_list = get_the_category(get_the_ID());
                                $array_check = filterCategory($cat_list, $category);
                                $cat_name = $array_check['name'];
                                if ($array_check['check'] == true) {
                                    $post_link = rewrite_link_post_vedette(get_the_permalink(), $array_check['slug']);
                                } else {
                                    $post_link = get_the_permalink();
                                }
                                //$tag = the_tags( '<a>', '</a>, <a>', '</a>' );
                                ?>
                                <article class="art">
                                    <a class="resize" href="<?php echo $post_link; ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                        <?php echo echoSpan($format_post); ?>
                                    </a>
                                    <h2><a href="<?php echo $post_link; ?>" title=""><?php echo $title_post; ?></a></h2>
                                    <ul class="tool-bar list-inline">
                                        <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li> |
                                        <li class="date"><?php echo the_time('d.m.Y'); ?> GMT + 7</li>
                                    </ul>
                                    <p><?php echo $excerpt; ?></p>
                                    <div class="tag-name">
                                        <i class="fa fa-tags"></i>
                                        <?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?>
                                    </div>
                                </article>
                            <?php
                                //endwhile;
                            endforeach;
                            wp_reset_postdata();
                        } ?>

                    </div>

                </div>
            </div>
        </section>
        <aside class="sidebar col-sm-4">
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
        </aside>
    </div> <!-- #row -->
</div><!-- #container -->

<?php
get_footer(); ?>

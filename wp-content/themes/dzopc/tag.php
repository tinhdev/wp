<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
    <div class="main container">
        <div class="row outer-ctn">
            <section class="content col-sm-8">
<!--               <ol class="breadcrumb">-->
<!--				  <li class="active">Tag</li>-->
<!--				</ol>-->
				<p class="kq-tag">Kết quả từ khóa <span style="color:#00AC77;">'<?php echo single_tag_title( '', false ); ?>'</span></p>
                <div class="highlight-ctn" id="custom_list">
                        <?php if ( have_posts() ) : ?>
                            <?php
                            // Start the Loop.
                            while ( have_posts() ) : the_post();
                                $title_post = get_the_title();
                                $excerpt = stringTrunc(get_the_excerpt(), 35);
                                $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );

                                $post_link = get_permalink(get_the_ID());
                                $cat_list = get_the_category(get_the_ID());
                                $post_link = rewrite_link_post_vedette($post_link, $cat_list);
                                ?>
                                <article class="art">
                                    <a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?= $title_post; ?>">
                                        <img src="<?= get_template_directory_uri(); ?>/images/trans.png" width="185" height="104" style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                                    </a>

                                    <h2><a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title=""><?php echo $title_post; ?></a></h2>
                                    <ul class="tool-bar list-inline">
                                        <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                        |
                                        <li class="date"><?php echo the_time('d.m.Y'); ?> GMT + 7</li>
                                    </ul>
                                    <p><?php echo $excerpt; ?></p>
                                    <span class="note"><?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?></span>
                                </article>
                            <?php  endwhile;

                        else :
                            // If no content, include the "No posts found" template.
                            get_template_part( 'content', 'none' );

                        endif;
                        ?>
                </div>
            </section>
            <aside class="sidebar col-sm-4">
                <?php dynamic_sidebar( 'sidebar-3' ); ?>
            </aside>
        </div> <!-- #row -->
    </div><!-- #container -->

<?php
get_footer();

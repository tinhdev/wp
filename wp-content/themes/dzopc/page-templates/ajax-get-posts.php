<?php
/*
Template Name: Ajax get posts
*/
$number = $_POST['numposts'];
$term_id = $_POST['category'];
$page = $_POST['page'];
$offset = ($page - 1) * $number;

$tag_slug = isset($_POST['tag']) ? $_POST['tag'] : '';
$tag = $tag_slug ? get_term_by('slug', $tag_slug, 'post_tag') : '';

$args = array(
    'numberposts' => $number,
    'offset' => $offset,
    'category' => $term_id,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    //'category__not_in' => '1',
    'post__not_in' => array(),
    'post_status' => 'publish',
    'suppress_filters' => true
);

if ($tag) {
    $args['tag_id'] = $tag->term_id;
}

$the_query = wp_get_recent_posts( $args );
$count = 0;
foreach ($the_query as $post) {
    $count++;
    $title_post = $post['post_title'];
    $link_post = get_the_permalink($post['ID']);
    $cat_list = get_the_category($post['ID']);
    $link_post = rewrite_link_post_vedette($link_post, $cat_list);

    $url = wp_get_attachment_image_src(get_post_thumbnail_id($post['ID']), 'medium');
    ?>
    <article class="art-1 art-1-style col-sm-2">
        <a class="js-view-post" data-id="<?= $post['ID'] ?>" href="<?= $link_post; ?>" title="<?php echo $title_post; ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?php echo $url[0]; ?>) no-repeat top left; "/>
        </a>

        <h3>
            <a class="js-view-post" data-id="<?= $post['ID'] ?>" href="<?= $link_post; ?>"
               title="<?php echo $title_post; ?>"><?= stringTrunc($title_post, 10); ?></a>
        </h3>
    </article>

<?php
}
wp_reset_query();
if ($count == $number) {
?>
<a class="btn-view js-load-more" href="javascript:void(0)"
   data-numposts="<?= $number; ?>"
   data-category="<?= $term_id; ?>"
   data-tag="<?= $tag_slug; ?>"
   data-page="<?= $page+1; ?>">Xem thêm</a>
<?php }
<?php
/**
 * Template Name: View Day
 *
 * @package dzopc
 * @author: Hoan Vo <vvhoan2004@gmail.com>
 * @version 1.0
 */

get_header(); ?>
    <div class="container">
        <div class="row">
        <section class="content col-sm-8">
            <ol class="breadcrumb">
                <li><a href="#" title="Trang chủ">Trang chủ</a></li>
                <li><a class="active" href="#">Xem theo ngày</a></li>
            </ol>
            <div class="outer-day">
                <div class="highlight-day">
                    <div class="slider-day">
                        <?php
                        $now = strtotime(date("Y-m-d"));
                        $yesterday = strtotime("-1 day", $now);
                        $current = empty($_GET['date']) ? $now : strtotime($_GET['date']);
                        $time = strtotime("-8 day", $current);
                        for($i=0 ; $time <= $now; $i++) {
                            $time = strtotime("+1 day", $time);

                            if ($time > $now) break;

                            $title = $time == $yesterday ? "Hôm qua" : ($time == $now ? "Hôm nay" : "");
                            $number_day = date("w", $time) + 1;
                            $number_day = $number_day == 1 ? 'CN' : "T{$number_day}";
                            ?>
                            <div<?php echo $time == $current ? ' class="current" data-index="'.$i.'"' : ''; ?> data-date="<?= date("Y-m-d", $time); ?>">
                                <div class="select-day">
                                    <p class="inner-day">
                                        <span class="i-day"><?= date("d", $time); ?></span>
                                        <span class="i-month"><?= date("m", $time); ?></span>
                                    </p>
                                    <span class="dotted"></span>
                                    <p class="inner-day-1">
                                        <span><?= $title; ?></span>
                                        <span><?= $number_day; ?></span>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <a class="btn-choose-d" href="javascript:void(0);"><input id='datetimepicker4' type="text" value="<?= date("Y-m-d", $current); ?>">Chọn ngày/<br>tháng</a>
            </div>
<?php
global $wpdb;
$wheres = array();
$number = 10;
$wheres[] = "post_status = 'publish' AND post_type = 'post'";
$date = date("Y-m-d", empty($_GET['date']) ? $current : strtotime($_GET['date']) );
$wheres[] = "post_date > '{$date} 00:00:00' AND post_date < '{$date} 23:59:59'";
$wheres = implode(" AND ", $wheres);
$posts = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE {$wheres} ORDER BY post_date DESC LIMIT {$number}");
?>
        <div class="highlight-ctn row">
        <?php foreach( $posts as $post) {

            setup_postdata($post);

            $title_post = get_the_title();
            $excerpt = stringTrunc($post->post_excerpt, 35);
            //$format_post = get_post_format($post->ID);

            $link = get_permalink ($post->ID);
            $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail' );
            ?>
            <article class="art">
                <a class="resize" href="<?php echo $link; ?>" title="<?php echo $title_post; ?>">
                    <img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                    <?php //echo echoSpan($format_post); ?>
                </a>
                <h2><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h2>
                <ul class="tool-bar list-inline">
                    <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li> |
                    <li class="date"><?php echo the_time('d.m.Y'); ?> GMT + 7</li>
                </ul>
                <p><?php echo $excerpt; ?></p>
                <div class="tag-name">
                    <i class="fa fa-tags"></i>
                    <span class="note"><?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?></span>
                </div>
            </article>
        <?php }
        wp_reset_query();
        ?>
        </div>
        </section>
        <aside class="sidebar col-sm-4">
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
        </aside>
        </div>
        <form id="frmSubmitDate" action="" method="get">
            <input type="hidden" name="date" id="hdate" value="<?= $date; ?>">
        </form>
    </div><!-- #container -->
<?php
get_footer();

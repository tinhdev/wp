<?php
/**
 * Template Name: Most-viewed
 *
 * @package dzopc
 * @author: Hoan Vo <vvhoan2004@gmail.com>
 * @version 1.0
 */

get_header(); ?>
    <div class="container">
        <div class="row">
            <section class="content col-sm-8">
                <?php dynamic_sidebar( 'sidebar-2' ); ?>
            </section>
            <aside class="sidebar col-sm-4">
                <?php dynamic_sidebar( 'sidebar-3' ); ?>
            </aside>
        </div><!-- #row -->
    </div><!-- #container -->
<?php
get_footer();

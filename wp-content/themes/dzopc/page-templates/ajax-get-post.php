<?php
/*
Template Name: Ajax get post
*/
$post = get_post($_POST['id']);
setup_postdata($post);
$nickname = isset(get_post_meta(get_the_ID())['nick-name'][0]) ? get_post_meta(get_the_ID())['nick-name'][0] : get_the_author();
$original_link = isset(get_post_meta(get_the_ID())['original-link'][0]) ? get_post_meta(get_the_ID())['original-link'][0] :
    (isset($post->source_link) ? $post->source_link : '');
?>
<article class="fck">
    <header>
        <h1><?php echo get_the_title()?></h1>
        <div class="bar">
            <span><?php echo $nickname;?>  '  <?php echo the_time('d.m.Y H:i'); ?> GMT+7 </span> '
            <span class="txt-cm" href="#comment"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span>
            <span>
                <i class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></i>
            </span>
            <span style="position: relative;top: 5px;">
                <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                <div class="g-plusone" data-size="medium" data-href="<?php echo get_permalink(get_the_ID()); ?>"></div>
            </span>
        </div>
        <p class="bold"><?php echo get_the_excerpt(); ?></p>
    </header>
    <section>
        <?php
        the_content();
        ?>
    </section>
    <footer>
        <p><?php echo $nickname;?></p>
    </footer>
    <ul class="outer-tag list-inline">
        <li><a href="javascript:void(0)" title=""><i class="fa fa-tags"></i> Tags</a></li>
        <?php the_tags( '<li>', '</li><li>', '</li>' ); ?>
    </ul>
    <ul class="list-share">
        <li>
            <div class="fb-like" data-href="<?php echo get_permalink(get_the_ID()); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
        </li>
        <li style="position: relative;top: -1px;">
            <div class="g-plusone" data-size="medium" data-href="<?php echo get_permalink(get_the_ID()); ?>"></div></li>
    </ul>

    <?php if ($original_link != ""): ?>
    <div class="block-link">
        <span>Nguồn</span>: <?php echo $original_link; ?>
    </div>
    <?php endif; ?>
</article>
<p class="noted-cm">Bình luận sẽ bị <span>XÓA</span> nếu nội dung chứa từ ngữ thô tục, chóng phá, đi ngược thuần phong mỹ tục</p>
<!--<div class="highlight block-comment">
    <?php //if ( comments_open() || get_comments_number() ) {
//comments_template();
//} ?>
</div> -->

<div class="highlight block-comment" id="comment">
    <div class="fb-comments" data-href="<?php echo get_the_permalink(); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
</div>

<?php dynamic_sidebar( 'sidebar-1' ); ?>

<?php updateView(get_the_ID()); ?>
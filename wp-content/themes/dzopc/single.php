<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Dzo
 * @since Dzo 1.0
 * @autho Hoanvo <vvhoan2004@gmail.com>
 */
$is_post = false; $empty_content=false;
while ( have_posts() ) : the_post();
    $is_post = true;
    $empty_content = empty($post->post_content);
endwhile;

if ($is_post && $empty_content) {
    $original_link = isset(get_post_meta(get_the_ID())['original-link'][0]) ? get_post_meta(get_the_ID())['original-link'][0] :
        (isset($post->source_link) ? $post->source_link : '');
    $content = file_get_contents($original_link);
    echo $content;
} else {
    get_header(); ?>
    <div class="main container">
        <?php
        // Start the Loop.
        //while (have_posts()) : the_post();
            /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
            //get_template_part( 'content', get_post_format() );
            if ($is_post) get_template_part('content');

        //endwhile;
        ?>
    </div><!-- #container -->

    <?php
    get_footer();
}
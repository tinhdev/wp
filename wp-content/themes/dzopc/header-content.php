<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 07/06/2015
 * Time: 22:09
 */
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php getTitle(); ?></title>
    <meta name="description" content="<?php getDescription(); ?>" />
    <meta name="keywords" content="<?php getKeyword(); ?>" />
    <?php if (is_front_page()): ?>
        <meta name="news_keywords" content="<?php getKeyword(); ?>" />
    <?php endif; ?>
    <meta name="robots" content="index, follow" />
    <meta property="fb:app_id" content="102484926761574" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/slick.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/screen.css" rel="stylesheet">
    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-64766660-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body <?php body_class(); ?>>
<header>
    <div class="header">
        <h1>
            <a href="<?php echo get_site_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
                <img alt="<?php bloginfo( 'name' ); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo-pc.png">
                <span>123dzo.net</span></a>
        </h1>
        <div class="social-header">
            <ul class="list-share-h">
                <li><a target="_blank" href="https://www.facebook.com/banbe2.0" title="Facebook"><i class="icon-facebook-h"></i></a></li>
                <li><a target="_blank" href="https://plus.google.com/u/0/b/117270960572010896121/117270960572010896121/about" title="Google plus"><i class="icon-google-h"></i></a></li>
            </ul>
        </div>
        <?php get_search_form(); ?>
        <nav>
            <ul>
<?php
$slug = isset($_GET['r']) ? explode('/', $_GET['r']) : array();
$cat_slug = isset($slug[1]) ? $slug[1] : '';
?>
                <li><a href="<?php echo get_home_url(); ?>" title="">Tổng quan</a></li>
                <li<?= $cat_slug=='cuoc-song-quanh-ta' ? ' class="active"' : ''; ?>>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc/cuoc-song-quanh-ta" title="">Cuộc sống quanh ta</a></li>
                <li<?= $cat_slug=='chuyen-thien-ha' || $cat_slug=='the-gioi-muon-mau' ? ' class="active"' : ''; ?>>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc/the-gioi-muon-mau" title="">Thế giới muôn màu</a></li>
                <li<?= $cat_slug=='cong-dong-mang' ? ' class="active"' : ''; ?>>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc/cong-dong-mang" title="">Cộng đồng mạng</a></li>
                <li<?= $cat_slug=='nong-tung-centimet' ? ' class="active"' : ''; ?>>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc/nong-tung-centimet" title="">Nóng từng centimet</a></li>
                <li<?= $cat_slug=='cuoi-chut-choi' ? ' class="active"' : ''; ?>>
                    <a href="<?php echo get_home_url(); ?>/tin-tuc/cuoi-chut-choi" title="">Cười chút chơi</a></li>
                <!--<li>Danh sách</li>-->
<?php
$pag_slug = isset($slug[0]) ? $slug[0] : '';
?>
                <li class="style<?= $pag_slug=='xem-nhieu-nhat' ? ' active' : ''; ?>">
                    <a href="<?php echo get_home_url(); ?>/xem-nhieu-nhat" title="Xem nhiều nhất">Xem nhiều nhất</a></li>
                <li class="style<?= $pag_slug=='chia-se-nhieu-nhat' ? ' active' : ''; ?>">
                    <a href="<?php echo get_home_url(); ?>/chia-se-nhieu-nhat" title="Chia sẻ nhiều nhất">Chia sẻ nhiều nhất</a></li>
                <li class="style<?= $pag_slug=='binh-luan-nhieu-nhat' ? ' active' : ''; ?>">
                    <a href="<?php echo get_home_url(); ?>/binh-luan-nhieu-nhat" title="Bình luận nhiều">Bình luận nhiều</a></li>
                <li class="style<?= $pag_slug=='xem-theo-ngay' ? ' active' : ''; ?>">
                    <a href="<?php echo get_home_url(); ?>/xem-theo-ngay" title="">Xem theo ngày</a></li>
            </ul>
        </nav>
        <p>® 2015 Blog tin tức tổng hợp - 123DZO.NET</p>
        <a class="btn-close-h" href="javascript:void(0)" title=""><i class="fa fa-chevron-right"></i></a>
    </div>
    <div class="menu-right">
        <a class="logo-type" href="<?php echo get_site_url(); ?>" title="<?php bloginfo( 'name' ); ?>"><img alt="<?php bloginfo( 'name' ); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
        <a class="btn-action" href="javascript:void(0)" title="">
            <i class="fa fa-th-list"></i>
            <span>Menu</span>
        </a>
        <a class="btn-action btn-s" href="javascript:void(0)" title="">
            <i class="fa fa-search"></i>
            <span>Tìm kiếm</span>
        </a>
        <a class="btn-share-type" href="javascript:void(0)" title="">
            <i class="fa fa-share-square-o"></i>
            <span>Share</span>
        </a>
        <ul class="list-share-1">
            <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();?>&display=popup&ref=plugin" title="Facebook"><i class="icon icon-facebook"></i></a></li>
            <li><a target="_blank" href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>" title="Google plus"><i class="icon icon-google"></i></a></li>
        </ul>
    </div>
</header>
<div class="header-m">
    <div class="container">
        <nav class="nav-main">
            <ul class="">
                <li><a class="btn-action-1" id="btn-menu" href="javascript:void(0)" title=""><i class="fa fa-th-list"></i></a></li>
                <li class="logo"><a href="<?php echo get_site_url(); ?>" title="<?php bloginfo( 'name' ); ?>"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a></li>
                <li class="search"><a class="btn-searchsg-1" href="javascript:void(0)" title=""><i class="fa fa-search"></i></a>
                    <form class="frm-search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" name="s" id="s" placeholder="<?php esc_attr_e( 'Nội dung cần tìm', 'dzopc' ); ?>" />
                        </div>
                    </form>
                </li>

                <li class="show-scroll dropdown">
                    <a class="dropdown-toggle" href="#" title="" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-share-square-o"></i></a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();?>&display=popup&ref=plugin"><i class="icon icon-facebook"></i> Facebook</a></li>
                        <li><a href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>"><i class="icon icon-google"></i> Google plus</a></li>
                    </ul>
                </li>

                <li class="show-scroll-1">
                    <a href="#comment" title=""><i class="fa fa-comments"></i>
                        <span>
                            <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count>
                        </span>
                    </a>
                </li>
                <!--<li class="btn-send-mail"><a href="#" title=""><i class="fa fa-envelope-o"></i></a></li>-->
            </ul>
        </nav>
    </div>
</div>
<div class="menu-main">
    <ul>
        <li><a href="<?php echo get_home_url(); ?>" title=""><i class="fa fa-home"></i>Tổng quan</a></li>
        <li><a href="<?php echo get_home_url(); ?>/tin-tuc/cuoc-song-quanh-ta" title=""><i class="fa fa-beer"></i>Cuộc sống quanh ta</a></li>
        <li><a href="<?php echo get_home_url(); ?>/tin-tuc/the-gioi-muon-mau" title=""><i class="fa fa-globe"></i>Thế giới muôn màu</a></li>
        <li><a href="<?php echo get_home_url(); ?>/tin-tuc/cong-dong-mang" title=""><i class="fa fa-users"></i>Cộng đồng mạng</a></li>
        <li><a href="<?php echo get_home_url(); ?>/tin-tuc/nong-tung-centimet" title=""><i class="fa fa-sun-o"></i>Nóng từng centimet</a></li>
        <li><a href="<?php echo get_home_url(); ?>/tin-tuc/cuoi-chut-choi" title=""><i class="fa fa-smile-o"></i>Cười chút chơi</a></li>
    </ul>
    <ul>
        <li><a href="<?php echo get_home_url(); ?>/xem-nhieu-nhat" title=""><i class="fa fa-eye"></i>Xem nhiều nhất</a></li>
        <li><a href="<?php echo get_home_url(); ?>/chia-se-nhieu-nhat" title=""><i class="fa fa-share-square"></i>Share nhiều nhất</a></li>
        <li><a href="<?php echo get_home_url(); ?>/binh-luan-nhieu-nhat" title=""><i class="fa fa-comments"></i>Bình luận nhiều</a></li>
        <li><a href="<?php echo get_home_url(); ?>/xem-theo-ngay" title=""><i class="fa fa-calendar"></i>Xem theo ngày</a></li>
        <li class="social-img">
            <div class="list-share-m">
                <a target="_blank" href="https://www.facebook.com/banbe2.0" title="Facebook"><i class="fa fa-facebook"></i></a>
                <a target="_blank" href="https://plus.google.com/u/0/b/117270960572010896121/117270960572010896121/about" title="Google plus"><i class="fa fa-google-plus"></i></a>
            </div>
        </li>
        <li class="sec"><a href="mailto:ads.123dzo@gmail.com"><i class="fa fa-envelope-o"></i>ads.123dzo@gmail.com</a></li>
        <li class="sec"><a href="tel:0946261550"><i class="fa fa-phone"></i> 0946.261.550</a></li>
    </ul>
    <p>® 2015 Blog tin tức tổng hợp<br><b>123dzo.net</b></p>
</div>
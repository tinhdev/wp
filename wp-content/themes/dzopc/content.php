<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Dzo
 * @since Dzo 1.0
 * @autho Hoanvo <vvhoan2004@gmail.com>
 */
$nickname = isset(get_post_meta(get_the_ID())['nick-name'][0]) ? get_post_meta(get_the_ID())['nick-name'][0] : get_the_author();
$original_link = isset(get_post_meta(get_the_ID())['original-link'][0]) ? get_post_meta(get_the_ID())['original-link'][0] :
    (isset($post->source_link) ? $post->source_link : '');
$category = get_the_category();
$link_category = get_term_link( $category[0] );
if (empty($post->post_content)) {
    $content = file_get_contents($original_link);
    echo $content;
    ?>
<!--    <section class="highlight-1  highlight-iframe">-->
<!--        <iframe class="i-full"-->
<!--                src="http://news.zing.vn/Chen-lan-cho-vao-sieu-thi-Han-Quoc-khai-truong-tai-Sai-Gon-post614608.html"></iframe>-->
<!--    </section>-->
<?php } else { ?>
<div class="havePad">
    <div class="outer-ctn row">
        <section class="content col-md-8 col-sm-12">
            <article class="fck">
                <header>
                    <h1><?php echo get_the_title()?></h1>
                    <div class="bar">
                        <span class="au-tg"><?php echo $nickname;?></span>-<span><?php echo the_time('d.m.Y H:i'); ?></span>

                        <span>
                            <i class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></i>
                        </span>
                        <span style="position: relative;top: 5px;">
                            <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                            <div class="g-plusone" data-size="medium" data-href="<?php echo get_permalink(get_the_ID()); ?>"></div>
                        </span>
                    </div>

                    <p class="bold"><?php echo get_the_excerpt(); ?></p>
                </header>
                <section>
                    <?php
                    the_content();
                    ?>
                </section>
                <footer>
                    <p><?php echo $nickname;?></p>
                </footer>
                <ul class="outer-tag list-inline">
                    <li><a href="javascript:void(0)" title=""><i class="fa fa-tags"></i> Tags</a></li>
                    <?php the_tags( '<li>', '</li><li>', '</li>' ); ?>
                </ul>
                <ul class="list-share">
                    <li>
                        <div class="fb-like" data-href="<?php echo get_permalink(get_the_ID()); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                    </li>
                    <li style="position: relative;top: -1px;">
                        <div class="g-plusone" data-size="medium" data-href="<?php echo get_permalink(get_the_ID()); ?>"></div>
                    </li>
                </ul>
                <div class="block-link">
                    <span>Nguồn</span>: <?php echo $original_link; ?>
                </div>
                <ul class="list-article-tool">
                    <li class="<?=pssc_facebook(get_the_ID())?>"><span class="counter-face">0</span></li>
                    <li><a data-toggle="tooltip" data-placement="right" title="Chia sẻ facebook"
                           href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>" ><i class="fa fa-facebook"></i></a></li>
                    <li class="<?=pssc_gplus(get_the_ID())?>"><span class="counter-gplus">0</span></li>
                    <li><a data-toggle="tooltip" data-placement="right" title="Google plus"
                           href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" >
                            <i class="fa fa-google-plus"></i></a></li>
                    <li><a id="scroll-cm" data-toggle="tooltip" data-placement="right" title="Bình luận" href="#comment" ><i class="fa fa-comment-o"></i></a></li>
                    <li><fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count></li>
                </ul>
            </article>
            <section class="highlight-1 highlight-food highlight-cm  block-comment" id="comment">
                <div class="fb-comments" data-href="<?php echo get_the_permalink(); ?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
            </section>

            <?php dynamic_sidebar( 'sidebar-1' ); ?>
        </section>
        <aside class="col-md-4 col-sm-12">
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
        </aside>
    </div>
</div>
<?php
}
updateView(get_the_ID()); ?>

<?php
/**
 * dzopc functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link https://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage dzopc
 * @since dzopc 1.0
 */

function get_css_version() {
    return 20160922;
}

function get_js_version() {
    return 20160922;
}
/**
 * Set up the content width value based on the theme's design.
 *
 * @see twentyfourteen_content_width()
 *
 * @since Twenty Fourteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 633;
}

if ( ! function_exists('dzo_setup') ) :
/**
 * Twenty Fourteen setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Twenty Fourteen 1.0
 */
function dzo_setup() {

	/*
	 * Make Twenty Fourteen available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Fourteen, use a find and
	 * replace to change 'twentyfourteen' to the name of your theme in all
	 * template files.
	 */
	//load_theme_textdomain( 'dzopc', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.
	//add_editor_style( array( 'css/editor-style.css', dzopc_font_url(), 'genericons/genericons.css' ) );

	// Add RSS feed links to <head> for posts and comments.
	//add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 303, 170, true );
    add_image_size( 'medium', 500, 281, true );
    add_image_size( 'large', 633, 356, true );
	//add_image_size( 'twentyfourteen-full-width', 1038, 576, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'left'   => __( 'Menu trái', 'dzopc' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
//	add_theme_support( 'html5', array(
//		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
//	) );

	/*
	 * Enable support for Post Formats.
	 * See https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
//	add_theme_support( 'custom-background', apply_filters( 'dzopc_custom_background_args', array(
//		'default-color' => 'f5f5f5',
//	) ) );

	// Add support for featured content.
//	add_theme_support( 'featured-content', array(
//		'featured_content_filter' => 'dzopc_get_featured_posts',
//		'max_posts' => 6,
//	) );

	// This theme uses its own gallery styles.
	//add_filter( 'use_default_gallery_style', '__return_false' );
}
endif; // dzo_setup
add_action( 'after_setup_theme', 'dzo_setup' );

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Twenty Fourteen 1.0
 */
//function dzopc_content_width() {
//	if ( is_attachment() && wp_attachment_is_image() ) {
//		$GLOBALS['content_width'] = 810;
//	}
//}
//add_action( 'template_redirect', 'dzopc_content_width' );

/**
 * Getter function for Featured Content Plugin.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return array An array of WP_Post objects.
 */
//function dzopc_get_featured_posts() {
//	/**
//	 * Filter the featured posts to return in Twenty Fourteen.
//	 *
//	 * @since Twenty Fourteen 1.0
//	 *
//	 * @param array|bool $posts Array of featured posts, otherwise false.
//	 */
//	return apply_filters( 'dzopc_get_featured_posts', array() );
//}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return bool Whether there are featured posts.
 */
//function dzopc_has_featured_posts() {
//	return ! is_paged() && (bool) dzopc_get_featured_posts();
//}

/**
 * Register three Twenty Fourteen widget areas.
 *
 * @since Twenty Fourteen 1.0
 */
function dzo_widgets_init() {
	//require get_template_directory() . '/inc/widgets.php';
	//register_widget( 'Twenty_Fourteen_Ephemera_Widget' );
    require get_template_directory() . '/inc/dzo_featured_widget.php';
    register_widget( 'Dzo_featured_widget' );
    require get_template_directory() . '/inc/dzo_webpart_widget.php';
    register_widget( 'Dzo_webpart_widget' );
    require get_template_directory() . '/inc/dzo_most_comment_widget.php';
    register_widget( 'dzo_most_comment_widget' );
    require get_template_directory() . '/inc/dzo_most_shared_widget.php';
    register_widget( 'dzo_most_shared_widget' );
    require get_template_directory() . '/inc/dzo_most_viewed_widget.php';
    register_widget( 'dzo_most_viewed_widget' );
    require get_template_directory() . '/inc/dzo_customlist_widget.php';
    register_widget( 'dzo_customlist_widget' );
    require get_template_directory() . '/inc/dzo_recent_post_widget.php';
    register_widget( 'dzo_recent_post_widget' );
    require get_template_directory() . '/inc/dzo_same_category_widget.php';
    register_widget( 'dzo_same_category_widget' );
    require get_template_directory() . '/inc/dzo_same_topic_widget.php';
    register_widget( 'dzo_same_topic_widget' );

    register_sidebar( array(
		'name'          => __( 'Top Region', 'dzopc' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Hiển thị dữ liệu top.', 'dzopc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Left Region', 'dzopc' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Hiển thị dữ liệu bên trái.', 'dzopc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Right Region', 'dzopc' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Hiển thị dữ liệu cột phải.', 'dzopc' ),
		'before_widget' => '<div class="block-bar block-mostview">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'dzo_widgets_init' );

/**
 * Register Lato Google font for Twenty Fourteen.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return string
 */
//function dzopc_font_url() {
//	$font_url = '';
//	/*
//	 * Translators: If there are characters in your language that are not supported
//	 * by Lato, translate this to 'off'. Do not translate into your own language.
//	 */
//	if ( 'off' !== _x( 'on', 'Lato font: on or off', 'dzopc' ) ) {
//		$query_args = array(
//			'family' => urlencode( 'Lato:300,400,700,900,300italic,400italic,700italic' ),
//			'subset' => urlencode( 'latin,latin-ext' ),
//		);
//		$font_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
//	}
//
//	return $font_url;
//}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Fourteen 1.0
 */
function dzo_scripts() {
	// Add Lato font, used in the main stylesheet.
	//wp_enqueue_style( 'dzopc-lato', dzopc_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	//wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.0.3' );

	// Load our main stylesheet.
	//wp_enqueue_style( 'dzopc-style', get_stylesheet_uri() );
    //wp_enqueue_script('disqus_embed','https://a.disquscdn.com/embed.js');

	// Load the Internet Explorer specific stylesheet.
	//wp_enqueue_style( 'dzopc-ie', get_template_directory_uri() . '/css/ie.css', array( 'dzopc-style' ), '20131205' );
	//wp_style_add_data( 'dzopc-ie', 'conditional', 'lt IE 9' );

//	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
//		wp_enqueue_script( 'comment-reply' );
//	}

//	if ( is_singular() && wp_attachment_is_image() ) {
//		wp_enqueue_script( 'twentyfourteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
//	}

//	if ( is_active_sidebar( 'sidebar-3' ) ) {
//		wp_enqueue_script( 'jquery-masonry' );
//	}

//	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
//		wp_enqueue_script( 'dzopc-slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), '20131205', true );
//		wp_localize_script( 'dzopc-slider', 'featuredSliderDefaults', array(
//			'prevText' => __( 'Previous', 'dzopc' ),
//			'nextText' => __( 'Next', 'dzopc' )
//		) );
//	}

	//wp_enqueue_script( 'twentyfourteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150315', true );
   // wp_enqueue_script( 'dzo_ajax.js', get_template_directory_uri() . "/js/dzo_ajax.js", array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'dzo_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Twenty Fourteen 1.0
 */
function dzopc_body_classes($classes) {

    global $wp_query;
    $temp = 'page-detail';
    if( !is_404() && !is_front_page() && !is_search()){
        $post_id = $wp_query->post->ID;

        if(is_search()){

           // $classes[] = get_post_meta($post_id, 'body_class', true);
            $temp = 'page-detail';

        }else{
            if (is_category()) {
                global $wp_query;
                $category = array($wp_query->get_queried_object());
            } else {
                $category = get_the_category();
            }

            $cat_slug = $term_id = empty($category[0]->slug) ? '' : $category[0]->slug;

            if ($cat_slug == "an-uong") $temp = 'food';
            elseif ($cat_slug == "du-lich") $temp = 'travel';
            elseif ($cat_slug == "giai-tri") $temp = 'enter';
            elseif ($cat_slug == "vui-choi") $temp = 'spend';
            elseif ($cat_slug == "su-kien") $temp = 'event';

            if ($temp != 'page-detail') {
                $temp = is_single($post_id) ? "{$temp} detail" : "page {$temp}";
            }
        }
    }

    if (is_front_page()) {
        $temp = 'home';
    }

    return array($temp);// return the $classes array

}

add_filter('body_class','dzopc_body_classes');
/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
//function dzopc_post_classes( $classes ) {
//	if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
//		$classes[] = 'has-post-thumbnail';
//	}
//
//	return $classes;
//}
//add_filter( 'post_class', 'dzopc_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function dzopc_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}
    if (is_single()) {
        $title = "$title $sep get_the_excerpt()";
    }


	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
//	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
//		$title = "$title $sep " . sprintf( __( 'Page %s', 'dzopc' ), max( $paged, $page ) );
//	}

	return $title;
}
//add_filter( 'wp_title', 'dzopc_wp_title', 10, 2 );

// Implement Custom Header features.
//require get_template_directory() . '/inc/custom-header_old.php';

// Custom template tags for this theme.
//require get_template_directory() . '/inc/template-tags.php';

// Add Customizer functionality.
//require get_template_directory() . '/inc/customizer.php';


/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
//if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
//	require get_template_directory() . '/inc/featured-content.php';
//}

function getPostByCatAndStick($catname, $limit, $stick=false) {
    if ($stick == true) {
        $args = array(
            'category_name' => $catname,
            'post__in' => get_option( 'sticky_posts'),
            'showposts' => $limit
        );
    } else {
        $args = array(
            'category_name' => $catname,
            'showposts' => $limit
        );
    }
    $the_query = new WP_Query( $args );
    wp_reset_query();
    return $the_query;
}

function dump($str){
    var_dump($str);
    exit;
}

function stringTrunc($phrase, $max_words) {
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
    return $phrase;
}

//Add field nick name
add_action( 'admin_menu', 'my_create_post_nickname_box' );
add_action( 'save_post', 'my_save_post_nickname_box', 10, 2 );

function my_create_post_nickname_box() {
    add_meta_box( 'my-meta-box', 'Nick name', 'my_post_nickname_box', 'post', 'normal', 'high' );
}

function my_post_nickname_box( $object) { ?>
    <p>
        <input type="text" name="nick-name" id="nick-name" style="width: 50%;" value="<?php echo wp_specialchars( get_post_meta( $object->ID, 'nick-name', true ), 1 ); ?>">
        <input type="hidden" name="my_meta_nickname_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
    </p>
<?php }

function my_save_post_nickname_box( $post_id) {

    if (!isset($_POST['my_meta_nickname_nonce']) || !wp_verify_nonce( $_POST['my_meta_nickname_nonce'], plugin_basename( __FILE__ ) ) )
        return $post_id;

    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    $meta_value = get_post_meta( $post_id, 'nick-name', true );
    $new_meta_value = stripslashes( $_POST['nick-name'] );

    if ( $new_meta_value && '' == $meta_value )
        add_post_meta( $post_id, 'nick-name', $new_meta_value, true );

    elseif ( $new_meta_value != $meta_value )
        update_post_meta( $post_id, 'nick-name', $new_meta_value );

    elseif ( '' == $new_meta_value && $meta_value )
        delete_post_meta( $post_id, 'nick-name', $meta_value );
}

//Add file original link
add_action( 'admin_menu', 'my_create_post_original_link_box' );
add_action( 'save_post', 'my_save_post_original_link_box' );

function my_create_post_original_link_box() {
    add_meta_box( 'my-original_link-box', 'Original Link', 'my_post_original_link_box', 'post', 'normal', 'high' );
}

function my_post_original_link_box( $object) { ?>
    <p>
        <input type="text" name="original-link" id="nick-name" style="width: 50%;" value="<?php echo wp_specialchars( get_post_meta( $object->ID, 'original-link', true ), 1 ); ?>">
        <input type="hidden" name="my_original_link_box_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
    </p>
<?php }

function my_save_post_original_link_box( $post_id) {

    if ( !isset($_POST['my_original_link_box_nonce']) || !wp_verify_nonce( $_POST['my_original_link_box_nonce'], plugin_basename( __FILE__ ) ) )
        return $post_id;

    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    $meta_value = get_post_meta( $post_id, 'original-link', true );
    $new_meta_value = stripslashes( $_POST['original-link'] );

    if ( $new_meta_value && '' == $meta_value )
        add_post_meta( $post_id, 'original-link', $new_meta_value, true );

    elseif ( $new_meta_value != $meta_value )
        update_post_meta( $post_id, 'original-link', $new_meta_value );

    elseif ( '' == $new_meta_value && $meta_value )
        delete_post_meta( $post_id, 'original-link', $meta_value );
}

//Disqus comment
//add_action( 'disqus_comment', 'disqus_embed' );
//function disqus_embed($disqus_shortname) {
//    global $post;
//    //wp_enqueue_script('disqus_embed','https://a.disquscdn.com/embed.js');
//    echo '<div id="disqus_thread"></div>
//    <script type="text/javascript">
//        var disqus_shortname = "'.$disqus_shortname.'";
//        var disqus_title = "'.$post->post_title.'";
//        var disqus_url = "'.get_permalink($post->ID).'";
//        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
//    </script>';
//}

function getRecentPost($limit) {
    $args = array(
        'numberposts' => $limit,
        'offset' => 0,
        'category' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'post',
        'post_status' => 'publish',
        'suppress_filters' => true );

    return wp_get_recent_posts($args);

}


//function getMostViewPost($limit) {
//    $query = new WP_Query( array(
//        'meta_key' => 'post_views_count',
//        'orderby' => 'meta_value_num',
//        'posts_per_page' => $limit
//    ) );
//    return $query->posts;
//}

//function popularPosts($num) {
//    global $wpdb;
//    $posts = $wpdb->get_results("SELECT comment_count, ID, post_title FROM $wpdb->posts ORDER BY comment_count DESC LIMIT 0 , $num");
//    return $posts;
//}
function the_breadcrumb () {

    // Settings
    $separator  = '&gt;';
    $id         = 'breadcrumbs';
    $class      = 'breadcrumbs';
    $home_title = 'Trang chủ';

    // Get the query & post information
    global $post,$wp_query;
    $category = get_the_category();

    // Build the breadcrums

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Home page
        echo '<li><a href="'.get_home_url().'" title="' . $home_title . '">' . $home_title . '</a></li>';
        if ( is_single() ) {
            echo '<li><a class="active" href="' . get_category_link($category[0]->term_id ) . '">' . $category[0]->cat_name . '</a></li>';
        } else if ( is_category() ) {

            // Category page
            //echo '<li class="item-current item-cat-' . $category[0]->term_id . ' item-cat-' . $category[0]->category_nicename . '"><strong class="bread-current bread-cat-' . $category[0]->term_id . ' bread-cat-' . $category[0]->category_nicename . '">' . $category[0]->cat_name . '</strong></li>';

        } else if ( is_page() ) {

            // Standard page
//            if( $post->post_parent ){
//
//                // If child page, get parents
//                $anc = get_post_ancestors( $post->ID );
//
//                // Get parents in the right order
//                $anc = array_reverse($anc);
//
//                $parents = '';
//                // Parent page loop
//                foreach ( $anc as $ancestor ) {
//                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
//                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
//                }
//
//                // Display parent pages
//                echo $parents;
//
//                // Current page
//                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
//
//            } else {
//
//                // Just display current page if not parents
//                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
//
//            }

        } else if ( is_tag() ) {

            // Tag page

            // Get tag information
//            $term_id = get_query_var('tag_id');
//            $taxonomy = 'post_tag';
//            $args ='include=' . $term_id;
//            $terms = get_terms( $taxonomy, $args );
//
//            // Display the tag name
//            echo '<li class="item-current item-tag-' . $terms[0]->term_id . ' item-tag-' . $terms[0]->slug . '"><strong class="bread-current bread-tag-' . $terms[0]->term_id . ' bread-tag-' . $terms[0]->slug . '">' . $terms[0]->name . '</strong></li>';

        } elseif ( is_day() ) {

            // Day archive

            // Year link
//            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
//            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
//
//            // Month link
//            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
//            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
//
//            // Day display
//            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_month() ) {

            // Month Archive

            // Year link
//            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
//            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
//
//            // Month display
//            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_year() ) {

            // Display year archive
           // echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

        } else if ( is_author() ) {

            // Auhor archive

            // Get the author information
//            global $author;
//            $userdata = get_userdata( $author );
//
//            // Display author name
//            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

        } else if ( get_query_var('paged') ) {

            // Paginated archives
           // echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

        } else if ( is_search() ) {

            // Search results page
            //echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';

        } elseif ( is_404() ) {

            // 404 page
           // echo '<li>' . 'Error 404' . '</li>';
        }

    }

}

//function my_get_posts_for_pagination() {
//    $paged = $_GET['page']; // Page number
//    $html = '';
//    $pag = 0;
//    if( filter_var( intval( $paged ), FILTER_VALIDATE_INT ) ) {
//        $pag = $paged;
//        $args = array(
//            'paged' => $pag, // Uses the page number passed via AJAX
//            'posts_per_page' => 2 // Change this as you wish
//        );
//        $loop = new WP_Query( $args );
//
//        if( $loop->have_posts() ) {
//            while( $loop->have_posts() ) {
//                $loop->the_post();
//                // Build the HTML string with your post's contents
//            }
//
//            wp_reset_query();
//        }
//    }
//
//    echo $html;
//    exit();
//
//}
//
//add_action( 'wp_ajax_my_pagination', 'my_get_posts_for_pagination' );
//add_action( 'wp_ajax_nopriv_my_pagination', 'my_get_posts_for_pagination' );

function MyAjaxFunction(){
    $paged = $_POST['page_number'];
    $cat_slug = $_POST['cat'];
    $number = $_POST['limit'];
    $query_args = array(
        'post_type' => 'post',
        'category_name' =>  $cat_slug,
        'posts_per_page' => $number,
        'paged' => $paged
    );
    $the_query = new WP_Query( $query_args );
    if ( $the_query->have_posts() ) {
        $result_return = '<div id="custom_list">';
        $result_return .= '<input type="hidden" value="'.$cat_slug.'" class="cat_name"/>';
        $result_return .= '<input type="hidden" value="'.$number.'" class="page_limit"/>';
        while ($the_query->have_posts()) : $the_query->the_post(); // run the loop
            $link_post = get_permalink();
            $title_post = get_the_title();
            $tags = wp_get_post_tags(get_the_ID());
            $tags_tag_html = '';
            $size_tags = sizeof($tags);
            $counter = 1;
            foreach($tags as $tag) {
                if($counter < $size_tags)
                    $tags_tag_html .= '<a href="'.get_tag_link($tag->term_id).'"">'.$tag->name.'</a>, ';
                else
                    $tags_tag_html .= '<a href="'.get_tag_link($tag->term_id).'"">'.$tag->name.'</a>';

                $counter++;
            }
            $excerpt = stringTrunc(get_the_excerpt(), 35);
            $format_post = get_post_format(get_the_ID());
            $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
            $date = get_the_time("d.m.Y");
            $result_return .= '<article class="art">';
            $result_return .= '<a class="resize" href="' . $link_post . '" title="'.$title_post.'"><img style="background-image: url('.$url[0].')" alt="" src="'.get_template_directory_uri().'/images/transparent.png">';
            $result_return .= echoSpan($format_post);
            $result_return .= '</a>';
            $result_return .= '<h2><a href="'.$link_post.'" title="' . $title_post . '">' . $title_post . '</a></h2>';
            $result_return .= '<ul class="tool-bar list-inline">';
            $result_return .= '<li><span class="txt-cm"><i class="fa fa-comments"></i>' . get_comments_number() . ' bình luận</span></li> |';
            $result_return .= '<li class="date">'.$date.' GMT + 7</li>';
            $result_return .= '</ul>';
            $result_return .= '<p>' . $excerpt . '</p>';
            $result_return .= '<span class="note">'.$tags_tag_html.'</span>';
            $result_return .= '</article>';
        endwhile;
        $result_return .= '</div>';
        //wp_reset_postdata();
        wp_reset_query();
        echo $result_return;
    } else {
        echo 0;
    }
    die();
}
// creating Ajax call for WordPress
add_action( 'wp_ajax_nopriv_MyAjaxFunction', 'MyAjaxFunction' );
add_action( 'wp_ajax_MyAjaxFunction', 'MyAjaxFunction' );


function updateSocial(){
    global $wpdb;
    $post_id = isset($_POST['post_id']) ? $_POST['post_id'] : '';
    $type = isset($_POST['type']) ? $_POST['type'] : '';
    $comment_count = isset($_POST['comment_count']) ? $_POST['comment_count'] : 0;
    if ($type == 'comment') {
        $comment_count = $comment_count + 1;
        $sqltr = "UPDATE dzo_posts SET comment_count = ".$comment_count." WHERE ID = ".$post_id;
        $status = $wpdb->query($sqltr);
        wp_reset_query();
        echo $status;
    } else {
        if ($type == 'share') {
            $post = $wpdb->get_results('SELECT share_count FROM dzo_posts WHERE ID = '.$post_id);
            $share_count = $post[0]->share_count + 1;
            $sqltr = "UPDATE dzo_posts SET share_count = ".$share_count." WHERE ID = ".$post_id;
            $status = $wpdb->query($sqltr);
            wp_reset_query();
            echo $status;
        } else {
            echo 0;
        }
    }
    die();
}

add_action('wp_ajax_nopriv_updateSocial', 'updateSocial');
add_action('wp_ajax_updateSocial', 'updateSocial');

function updateView($post_id) {
    global $wpdb;
    $post = $wpdb->get_results('SELECT post_view FROM dzo_posts WHERE post_type = "post" AND ID = '.$post_id);
    $post_view = $post[0]->post_view + 1;

    $sqltr = "UPDATE dzo_posts SET post_view = ".$post_view." WHERE ID = ".$post_id;
    $wpdb->query($sqltr);
    wp_reset_query();
}

function echoSpan($type) {
    if ($type == "video") {
        return '<span class="txt-video">Video</span>';
    }
    if ($type == "image") {
        return '<span class="txt-photo">Photo</span>';
    }
    return '';
}


function dzo_makeUrl($slug) {
    return get_home_url().'/tin-tuc/'.$slug;
}

// advanced search functionality
//function advanced_search_query($query) {
//
//    if($query->is_search()) {
//        // category terms search.
//        if (isset($_GET['category']) && !empty($_GET['category'])) {
//            $query->set('cat', array(array(
//                'field' => 'slug',
//                'terms' => array($_GET['category']) )
//            ));
//        }
//        return $query;
//    }
//}
//add_action('pre_get_posts', 'advanced_search_query', 1000);

function rewrite_link_post_vedette($link, $cats, $slug='') {
    if (strpos($link, '/vedette/') === false) {
        return $link;
    }

    $new_slug = $slug;
    foreach ($cats as $cat) {
        if ($cat->slug != 'vedette') {
            $new_slug = $cat->slug;
            break;
        }
    }

    return str_replace("/vedette/", "/{$new_slug}/", $link);
}
function filter_where_wpa89154($where = '') {
    $date = isset($_GET['date']) ? explode('/', $_GET['date']) : '';
    $type = isset($_GET['type']) ? $_GET['type'] : 1;
    $keyword = $_GET['s'];
    if ($date != '') {
        $temp_date_from = $date[0];
        $temp_date_to = $date[0] + 1;
        $date_from = $date[2].'-'.$date[1].'-'.$temp_date_from;
        $date_to = $date[2].'-'.$date[1].'-'.$temp_date_to;

        //$where .= " AND post_date = '" . date('Y-m-d', strtotime('-30 days')) . "'";
        //$where .= " AND post_date > '" . $date_from. "' AND post_date <= '". $date_to. "'";
        if ($type == 1) {
            $where .= " AND (post_title LIKE '%".$keyword."%' OR post_content LIKE '%".$keyword."%') AND post_date > '" . $date_from. "' AND post_date <= '". $date_to. "'";

        }
        if ($type == 2) {
            $where .= " AND post_title LIKE '%".$keyword."%' AND post_date > '" . $date_from. "' AND post_date <= '". $date_to. "'";
        }
        if ($type == 3) {
            $where .= " AND post_content LIKE '%".$keyword."%' AND post_date > '" . $date_from. "' AND post_date <= '". $date_to. "'";
        }
        return $where;
    } else {
        return;
    }

}

// add_filter('posts_where', 'filter_where_wpa89154');
function mysearch($limit = 10) {
    //$keyword = preg_replace('/^-|-$|[^-a-zA-Z0-9]/', '', $_GET['s']);
    $page = isset($_GET['page']) ? $_GET['page']: 1;
    $offset = ($page - 1) * $limit;
    $keyword = $_GET['s'];
    $category = isset($_GET['cat']) ? $_GET['cat']: '';
    global $wpdb;

    $type = isset($_GET['type']) ? $_GET['type'] : 1;

    $wheres = array();
    if ($type == 1) {
        $wheres[] = "(post_title LIKE '%".$keyword."%' OR post_content LIKE '%".$keyword."%')";
    }
    if ($type == 2) {
        $wheres[] = "post_title LIKE '%".$keyword."%'";
    }
    if ($type == 3) {
        $wheres[] = "post_content LIKE '%".$keyword."%'";
    }

    $str_date = isset($_GET['date']) ? $_GET['date'] : '';
    if ($str_date) {
        $date = date_create_from_format('d/m/Y', $str_date);
        $date_from = date_format($date, 'Y-m-d');
        $date->add(new DateInterval('P1D'));
        $date_to = date_format($date, 'Y-m-d');

        $wheres[] = "post_date > $date_from AND post_date > $date_to";
    }

    $sql = "SELECT * FROM {$wpdb->posts} as p1";
    if ($category) {
        $cate = get_category_by_slug($category);
        $wheres[] = "t1.taxonomy = 'category' AND terms1.term_id = '{$cate->term_id}'";

        $sql .= " LEFT JOIN {$wpdb->term_relationships} as r1 ON p1.ID = r1.object_ID
			LEFT JOIN {$wpdb->term_taxonomy} as t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
			LEFT JOIN {$wpdb->terms} as terms1 ON t1.term_id = terms1.term_id";
    }
    $wheres[] = "p1.post_status = 'publish' AND p1.post_type = 'post'";

    $sql .= " WHERE ". implode(' AND ', $wheres);
    $sql .= " ORDER BY post_date DESC LIMIT {$limit} OFFSET {$offset}";

    $post_results = $wpdb->get_results($sql);
    wp_reset_query();
    return $post_results;
}

function mysearch_old() {
    global $wpdb;
    $keyword = preg_replace('/^-|-$|[^-a-zA-Z0-9]/', '', $_GET['s']);
    $date = isset($_GET['date']) ? explode('/', $_GET['date']) : '';
    $cat = isset($_GET['cat']) ? preg_replace('/[^0-9]/', '', $_GET['cat']) : '';
    $type = isset($_GET['type']) ? preg_replace('/[^0-9]/', '', $_GET['type']) : '';
    $results = array();


    if ($date == '' && $cat == '' && $type == '') {
        $results = query_posts( array(
            'post_type' => 'post',
            's' => $keyword,
            'posts_per_page' => 20,
            'paged' => 1
        ));
        wp_reset_query();
        return $results;
    }
    if ($keyword != '') {
        $results = query_posts( array(
            'post_type' => 'post',
            's' => $keyword,
            'posts_per_page' => 100,
            'category_name' => $cat,
            'monthnum' => $date[1],
            'day' => $date[0],
            //'paged' => 1
        ));
        wp_reset_query();

        if ($type != 1) {
            $i = 0;
            $post_results = NULL; //empties the courseresults array in case there's anything in it from a previous serach
            if($results != NULL) {
                foreach($results as $item) : //creates an array of IDs to be used later on
                    $post_id = $item->ID;
                    $post_results[$i] = $post_id;
                    $i++;
                endforeach;
            }
            //dump($post_results);
            if ($type == 2)
                $keyword_mysql = "SELECT * FROM $wpdb->posts WHERE post_type='post' AND post_title LIKE '%$keyword%'";
            if ($type == 3)
                $keyword_mysql = "SELECT * FROM $wpdb->posts WHERE post_type='post' AND post_content LIKE '%$keyword%'";
            if($post_results != '') {
                $keyword_mysql .= " AND (";
                foreach($post_results as $item) {
                    $keyword_mysql .= "ID =  '$item' OR ";
                }
                $keyword_mysql .= "ID =  '$item' )";
            }
            //dump($keyword_mysql);
            $results = $wpdb->get_results($keyword_mysql);

        }
    }

    return $results;
}

function filterCategory($cats, $exclude_cat) {
    $array_cat = array();
    $array_cat['check'] = false;
    $array_cat['name'] = $exclude_cat;
    foreach ($cats as $cat) {
        if ($cat->name != $exclude_cat) {
            $array_cat['name'] = $cat->name;
            $array_cat['slug'] = $cat->slug;
            $array_cat['check'] = true;
            return $array_cat;
        }

    }
    return $array_cat;
}

/*function fb_change_search_url_rewrite() {
    if ( is_search() && ! empty( $_GET['s'] ) ) {
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }
}
add_action( 'template_redirect', 'fb_change_search_url_rewrite' );*/


function more_post_ajax(){
    $offset = $_POST["offset"];
    $ppp = $_POST["ppp"];
    header("Content-Type: text/html");

    $args = array(
        'post_type' => 'post',
        'posts_per_page' => $ppp,
        'cat' => 1,
        'offset' => $offset,
    );

    $loop = new WP_Query($args);
    while ($loop->have_posts()) { $loop->the_post();
        the_content();
    }

    exit;
}

//add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
//add_action('wp_ajax_more_post_ajax', 'more_post_ajax');


function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        //echo "<div class=\"pagination clearfix\">";
        //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>« First</a>";
        if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."'><<</a></li>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<li class='active'><a href='".get_pagenum_link($i)."'>".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
                //echo "<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."'> >></a></li>";
        //if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last »</a>";
       // echo "</div>\n";
    }
}

function getDescription() {
    $des = "";
    if (is_single()) $des = get_the_excerpt();
    if (is_category()) {
        $slug = isset($_GET['r']) ? explode('/', $_GET['r']) : array();
        $term = get_category_by_slug($slug[1]);
        $des = $term->description;
    }
    if (is_page() || is_front_page() || is_page_template()) {
        $p_meta = get_post_meta(get_the_ID());
        $des = isset($p_meta['description'][0]) ? $p_meta['description'][0] : get_bloginfo('description');
    }
    if (is_404()) $des = get_bloginfo('description');
    if (is_search() || is_tag()) $des = get_bloginfo('description');
    echo $des;
}

function getTitle() {
    $title = "";
    if (is_single()) $title = get_the_title();
    if (is_category()) {
        global $wp_query;
        $category = array($wp_query->get_queried_object());
        if (empty($category[0]->name)) {
            $title = get_bloginfo();
        } else {
            $title = $category[0]->name.' | '.get_bloginfo();
        }
    }
    if (is_page() ||  is_page_template()) {
        $title_page = get_the_title(get_the_ID());
        $title = $title_page. ' | '.get_bloginfo();
    }

    if (is_front_page()) {
        //$title_page = get_the_title(get_the_ID());
        $title = get_bloginfo();
    }
    if (is_404()) $title = "Not found".' | '.get_bloginfo();
    if (is_search()) $title = "Kết quả tìm kiếm với từ khóa '{$_GET['s']}'" .' | '. get_bloginfo();
    if (is_tag()) $title = "Tin tức, chủ đề có liên quan đến từ khóa '".single_tag_title( '', false )."' | ". get_bloginfo();
    echo $title;
}

function getKeyword() {
    echo get_bloginfo('description');
}

function buildLinkPost($url, $post_id) {
    if (strpos($url, 'vedette') !== false) {
        $cat_list = get_the_category($post_id);
        $array_term = filterCategory($cat_list, 'vedette');
        return str_replace('vedette', $array_term['slug'], $url);
    } else {
        return $url;
    }
}

function get_category_tags($args) {
    global $wpdb;
    $tags = $wpdb->get_results
    ("
		SELECT DISTINCT terms2.term_id as tag_id, terms2.name as tag_name, terms2.slug as tag_slug
		FROM
			{$wpdb->posts} as p1
			LEFT JOIN {$wpdb->term_relationships} as r1 ON p1.ID = r1.object_ID
			LEFT JOIN {$wpdb->term_taxonomy} as t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
			LEFT JOIN {$wpdb->terms} as terms1 ON t1.term_id = terms1.term_id,

			{$wpdb->posts} as p2
			LEFT JOIN {$wpdb->term_relationships} as r2 ON p2.ID = r2.object_ID
			LEFT JOIN {$wpdb->term_taxonomy} as t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
			LEFT JOIN {$wpdb->terms} as terms2 ON t2.term_id = terms2.term_id
		WHERE
			t1.taxonomy = 'category' AND p1.post_status = 'publish' AND terms1.term_id IN (".$args['categories'].") AND
			t2.taxonomy = 'post_tag' AND p2.post_status = 'publish'
			AND p1.ID = p2.ID
		ORDER by tag_name
	");

    $count = 0;
    foreach ($tags as $tag) {
        $tags[$count]->tag_link = get_tag_link($tag->tag_id);
        $count++;
    }
    return $tags;
}
<form class="frm-search active" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="text" class="txt-search" name="s" id="s" value="<?= isset($_GET['s']) ? $_GET['s'] : ''; ?>" placeholder="<?php esc_attr_e( 'Nội dung cần tìm', 'dzopc' ); ?>" />
    <button type="submit" id="searchsubmit" class="btn-search"><i class="fa fa-search"></i></button>
</form>

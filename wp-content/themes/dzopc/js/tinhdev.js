$(document).on( "click", ".js-view-post", function( e ) {
    //e.preventDefault();
    //var url = $(this).attr('href');
    //var link_location = window.location.href;
    ////$(".detail-box").modal('show');
    //$.post( '/ajax-get-post', {id: $(this).attr('data-id')}, function( data ) {
    //    $(".detail-box .modal-body").html(data);
    //    $(".detail-box").modal('show');
    //
    //    if(url != link_location) {
    //        window.history.pushState({path: url}, '', url);
    //    }
    //
    //    FB.XFBML.parse();
    //
    //    $('.detail-box').unbind( 'hidden.bs.modal' );
    //    $('.detail-box').on('hidden.bs.modal', function (e) {
    //        $(".detail-box .modal-body").html('');
    //        window.history.pushState({path: link_location}, '', link_location);
    //    });
    //});
});

$(document).on( "click", ".js-load-more", function( e ) {
    e.preventDefault();

    var obj = $(this);

    var params = {category: obj.attr('data-category'),
        page: obj.attr('data-page'),
        tag: obj.attr('data-tag'),
        numposts: obj.attr('data-numposts')};

    var url = updateUrlParameter('page', obj.attr('data-page'), window.location.href);
    window.history.pushState({path: url}, '', url);

    $.post( '/ajax-get-posts', params, function( data ) {
        obj.replaceWith(data);
    });
});

$(document).on( "click", ".js-search-view-more", function( e ) {
    e.preventDefault();

    var p = $('#page').val();
    $('#page').val(parseInt(p) + 1);

    $('#searchform-page').trigger('submit');
});
$( "#searchform-page" ).submit(function() {
    $.each( $( this ).serializeArray(), function( i, field ) {
        if (field.value=='') {
            $('#'+field.name).removeAttr('name');
        }
    });
});

var accessTokenFB = '';
function getAccessTokenFB () {
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            accessTokenFB = response.authResponse.accessToken;
            console.log(accessTokenFB);
        }
    });
}
$(function() {
    if ($('#datetimepicker1').length > 0) {
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    }

    $('.selectpicker').on( "change", function( ) {
        window.location.href = $(this).val();
    });

    if ($('span.counter-face').length > 0) {
        //setTimeout(function() {
        //    getAccessTokenFB();
        //
        //    setTimeout(function() {
        //        var url = window.location.href;
        //        $.get( 'https://graph.facebook.com/v2.7/', {id: url, access_token: accessTokenFB}, function( fbdata ) {
        //            console.log(fbdata);
        //            $('span.counter-face').html(fbdata.share.share_count);
        //        }, 'json');
        //
        //    }, 1000);
        //
        //}, 1000);

        setTimeout(function() {
            var url = window.location.href;
            var key = window.btoa(url);

            var share_count = $.jStorage.get(key);
            if (share_count) {
                console.log('get from jStorage');
                $('span.counter-face').html(share_count);
            } else {
                $.get( 'https://graph.facebook.com/', {id: url}, function( fbdata ) {
                    console.log(fbdata);
                    $('span.counter-face').html(fbdata.share.share_count);
                    $.jStorage.set(key, fbdata.share.share_count, {TTL: 1000*60*60});
                }, 'json');
            }
        }, 1000);
    }

    if ($('span.counter-gplus').length > 0) {
        var url = window.location.href;
        /* Social Share: Google Plus JSON */
        $.ajax({
            type: 'POST',
            url: 'https://clients6.google.com/rpc',
            processData: true,
            contentType: 'application/json',
            data: JSON.stringify({
                'method': 'pos.plusones.get',
                'id': url,
                'params': {
                    'nolog': true,
                    'id': url,
                    'source': 'widget',
                    'userId': '@viewer',
                    'groupId': '@self'
                },
                'jsonrpc': '2.0',
                'key': 'p',
                'apiVersion': 'v1'
            }),
            success: function(response) {
                $('span.counter-gplus').text(response.result.metadata.globalCounts.count);
            }
        });
    }
});

function updateUrlParameter(key, value, url){
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}
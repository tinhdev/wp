/**
 * Website start here
 */

$(window).load(function () {
    /* if ($('.scroll-top').length > 0) {
        var top = $('.scroll-top').offset().top - parseFloat($('.scroll-top').css('marginTop').replace(/auto/, 0));
        var footTop = $('.footer').offset().top - parseFloat($('.footer').css('marginTop').replace(/auto/, 0));
        var maxY = footTop - $('.scroll-top').outerHeight();
        $(window).scroll(function () {
           
            var y = $(this).scrollTop();
            if (y > top) {
                if (y < maxY) {
                    $('.scroll-top').addClass('fixed').removeAttr('style');
                } else {
                    $('.scroll-top').removeClass('fixed').css({
                        position: 'absolute',
                        top: ($('.content').outerHeight() - $('.scroll-top').outerHeight()) + "px"
                    });

                }
            } else {
                $('.scroll-top').removeClass('fixed');
            }
        });
    } */
	if ($(window).width() > 991) {
		active_scroll_block_news_latest();
	}
	
    $("table").each(function () {
        if ($(this).find('.caption').length > 0) {
            var wi_ = $(this).find('.caption').width();
            $(this).find('.ck_image').css("width", wi_ + 16);

        }
        else {
            if ($(this).find('.ck_image').width() > $(this).find('.fck').width()) {
                $(this).find('.ck_image').css("width", "100%");

            }
            else {
                $(this).find('.ck_image').css("width", "auto");
            }
        }
    });
});
$(document).ready(function ($) {
	if($('#scroll-cm').length > 0){
		$('#scroll-cm').click(function(){			
			var val = $(this).attr('href');
			$('html,body').animate({
				scrollTop: $(val).offset().top
			}, 500);
		});
	}
    $('[data-toggle="tooltip"]').tooltip();
	$('.detail-box').on('shown.bs.modal', function () {
	  $("img.lazy").lazyload({
		 container:$(".detail-box"),
		 effect : "fadeIn"
	  });
	});
	
	if ($(window).width() < 768) {
		 $(".frm-search").removeClass("active");
	}
    setNavigation(["ul.navbar-nav li", "ul.list-chuyenmuc li"]);
    if ($(".btn-action").length > 0) {
        $(".btn-action").click(function () {
            if ($(".layer-menu").hasClass("active")) {
                $(".layer-menu").removeClass("active");
            }
            else {
                $(".layer-menu").addClass("active");
                if (window.innerHeight > $(".layer-menu").height() + $(".header").height()) {
                    $(".layer-menu").css("height", "auto");
                }
                else {
                    $(".layer-menu").css({"height": window.innerHeight - $(".header").height(), "overflow-y": "auto"});
                }
            }
        });
    }
    if ($(".btn-search").length > 0) {
        $(".btn-search").click(function () {
            if ($(".frm-search").hasClass("active")) {
                $(".frm-search").removeClass("active");
            }
            else {
                $(".frm-search").addClass("active");
                return false;
            }
        });
    }
    if ($("#link-search").length > 0) {

        $("#link-search").click(function () {
            if ($(".frm-search").hasClass("show")) {
                $(".frm-search").removeClass("show");
            }
            else {
                $(".frm-search").addClass("show");
            }
            if ($(this).hasClass("normal")) {
                $(this).removeClass("normal");
            }
            else {
                $(this).addClass("normal");
            }

        });
    }
    if ($(".btn-top").length > 0) {
        $(window).scroll(function () {
            var e = $(window).scrollTop();
            if (e > 300) {
                $(".btn-top").css("display", "block");
            } else {
                $(".btn-top").css("display", "none");
            }
        });
        $(".btn-top").click(function () {
            $('body,html').animate({
                scrollTop: 0
            })
        });
    }
    if ($(".list-chuyenmuc").length > 0) {
        $(window).scroll(function () {
            var e = $(window).scrollTop();
            if (e > 50) {
                $(".list-chuyenmuc").addClass("active");
            } else {
                $(".list-chuyenmuc").removeClass("active");
            }
        });
    }
    $(".btn-menu").click(function () {
        if ($(".main-menu").hasClass("active")) {
            $(".main-menu").removeClass("active");
        }
        else {
            $(".main-menu").addClass("active");
        }
    });
    if ($('.mCustomScrollbar').length > 0) {
        $('.mCustomScrollbar').mCustomScrollbar({
            scrollInertia: 500
        });
    }
    if ($('.slider-ph').length > 0) {
        $('.slider-ph').flexslider({
            animation: "none"
        });
    }
    if ($(".flexslider-new").length > 0) {
		 var $window = $(window),
			  flexslider;
		 
		  // tiny helper function to add breakpoints
		  function getGridSize() {
			return (window.innerWidth < 640) ? 2 :
				   (window.innerWidth < 1024) ? 4 :
				   (window.innerWidth > 1200) ? 6 : 6;
		  }
		 

$window.load(function() {

        $('.flexslider-new').flexslider({
                    animation: "slide",
                    slideshowSpeed: 4000,
                    animationSpeed: 400,
                    animationLoop: true,
                    slideshow: true,
                    itemWidth: 165,
                    itemMargin: 10,
					minItems: getGridSize(), // use function to pull in initial value
					maxItems: getGridSize(), // use function to pull in initial value

        });
		 });

           $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });


      
    }
    if ($(".slide_content").length > 0) {
        $('.slide_content').each(function () {
            var srcs = $(this).find("img").attr('src');
            if (srcs != undefined) {
                $(this).attr('href', srcs.replace("i/s626", "r"));
                var title_page = $("#object_title").text();
                var caption_ = $(this).find('.ck_legend.caption').text();
                $(this).attr('data-loading-text', caption_);
                $(this).find("img").attr('alt', caption_);
                $(this).attr('data-fancybox-group', 'gallery');
                $(this).attr('title', title_page);

            }
        });

        var detail_url;
        $('.slide_content').fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn: false,
            helpers: {
                buttons: {},
                thumbs: {
                    width: 100,
                    height: 100

                }
            },
            beforeLoad: function () {
                detail_url = document.URL;
            },
            afterLoad: function () {
                var toolbar = '<div class="tool-box"><a href="#" title=""></a><ul class="socialbar"><li><iframe src="//www.facebook.com/plugins/like.php?href=' + detail_url + '&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe></li></ul></div>';
                $(".fancybox-wrap").append(toolbar);
                var alt = $(this.element).parents().find("#object_title").text();
                $(".tool-box > a").text(alt);


                var tooltitle = '<div class="tool-title"></div>';
                $(".fancybox-wrap").append(tooltitle);
                var title = $(this.element).attr('data-loading-text');
                $(".tool-title").text(title);
            },
            afterShow: function () {
                var wh = $(window).height();
                var imgh = $(".fancybox-image").height();
                if (imgh > wh) {
                    $(".fancybox-image").css({"height": "100%", "width": "auto"});
                }
                else {
                    $(".fancybox-image").css({"height": "auto", "width": "100%", "margin-top": (wh - imgh) / 2});
                }
            }
        });
    }
});
$(document).click(function () {
    $(".list-cata, .frm-search, .layer-menu, .main-menu").removeClass("active");
    $(".frm-search").removeClass("show");
    $("#link-search").removeClass("normal");
});
$('.list-cata, .btn-menu, #link-search, .frm-search,.frm-search-1, .layer-menu, .btn-action, .main-menu').click(function (event) {
    event.stopPropagation();
});


function setNavigation(elements) {
    var path = window.location.pathname;
    var host = "http://" + window.location.host;
    path = path.replace(/\/$/, "");
    path = host + decodeURIComponent(path);

    elements.forEach(function (element, index, array) {
        if (path == host) {
            $(element + ":first-child").addClass('active');
            $(element + ":first-child").addClass('active');
        } else {
            $(element + " a").each(function () {
                var href = $(this).attr('href');
                if (path.substring(0, href.length) == href && href != host) {
                    $(this).parent('li').addClass('active');
                }
            });
            $(element + " a").each(function () {
                var href = $(this).attr('href');
                if (path.substring(0, href.length) == href && href != host) {
                    $(this).parent('li').addClass('active');
                }
            });
        }
    });
}
function active_scroll_block_news_latest() {
		$(document).scroll(function () {
			if($(".sidebar .block-mostview").length > 0)
			{
				stickyBlockNewsLatest();
			}
		});

		if($(".sidebar .block-mostview").length > 0)
		{
			stickyBlockNewsLatest();
		}
}
function stickyBlockNewsLatest() {
		var hH = $('.outer-ctn .content').offset().top - 59;
		var tH = $(".sidebar .block-mostview").height();
		var scroll = $(window).scrollTop();

		var rH = $('.outer-ctn .content').height();
		if (scroll > hH && rH > tH) {
			var _top = rH - scroll - tH + hH;
			if (_top > 0) _top = 60;

			$(".sidebar .block-mostview").css({
				"position": "fixed",
				"width": "328px",
				"top": _top + "px"
			});
		} else {
			$(".sidebar .block-mostview").css({
				"position": "",
				"width": "auto"
			});
		}
}
function onReady(){    
    $('#loading').fadeOut(500, function(){
        $(this).remove();
    });   
}
function loading() {
    var startTime = new Date().getTime();
    $('<div id="loading"><img src="/wp-content/themes/dzopc/images/loading.gif" /></div>').appendTo('body');
    var allImages = $('img'),
    imgLength = allImages.length,
    countImage = 0;
    for(var i = 0; i < imgLength; i++){
        var img = new Image();
        img.onload = function(){
            countImage++;
            if(countImage == imgLength){
                var endTime = new Date().getTime();
                if(endTime - startTime > 500){
                    onReady();
                }else{
                    setTimeout(function(){
                        onReady();
                    },500 - (endTime - startTime));
                }
            }
        };
        img.onerror = function(){
            countImage++;if(countImage == imgLength){
               var endTime = new Date().getTime();
                if(endTime - startTime > 2000){
                    onReady();
                }else{
                    setTimeout(function(){
                        onReady();
                    },1000 - (endTime - startTime));
                }
            }
        };
        img.src = allImages.eq(i).attr('src');
    }    
}
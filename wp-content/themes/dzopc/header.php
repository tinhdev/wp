<!doctype html>
<!--[if IE 7]><html lang="en" class="ie7"></html><![endif]-->
<!--[if IE 8]><html lang="en" class="ie8"></html><![endif]-->
<!--[if IE 9]><html lang="en" class="ie9"></html><![endif]-->
<!-- [if gt IE 9] <!-->
<html lang="en">
<!-- <![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php getTitle(); ?></title>
    <meta name="keywords" content="<?php getKeyword(); ?>" />
    <?php if (is_front_page()): ?>
        <meta name="news_keywords" content="<?php getKeyword(); ?>" />
    <?php endif; ?>
    <meta name="robots" content="index, follow" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />

    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/print.css?v=<?= get_css_version(); ?>" media="print">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/lib.css?v=<?= get_css_version(); ?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/plugin.css?v=<?= get_css_version(); ?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-select.css?v=<?= get_css_version(); ?>" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/screen.css?v=<?= get_css_version(); ?>" media="screen" />

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.1.min.js"></script>

    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-64766660-1', 'auto');
        ga('send', 'pageview');

        var site_url = '<?php echo get_site_url(); ?>/';
    </script>

</head>
<body <?php body_class(); ?>>
    <header class="header">
        <div class="container">
            <nav class="main-nav">
                <a class="btn-menu" href="javascript:void(0);"><i class="fa fa-bars"></i></a>
                <a  class="logo" href="<?php echo get_site_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
                    <img class="logo-pc" alt="<?php bloginfo( 'name' ); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo-2.png?v=<?= get_css_version(); ?>" />
                    <img class="logo-mobile" alt="<?php bloginfo( 'name' ); ?>" src="<?php echo get_template_directory_uri(); ?>/images/logo-pc.png?v=<?= get_css_version(); ?>" />
                </a>
                <ul class="main-menu">
                    <li<?= is_front_page()?' class="active"':''; ?>><a href="<?php echo get_home_url(); ?>" title="Tổng quan"><i class="fa fa-home"></i> Tổng quan</a></li>
                    <li<?= is_category('an-uong')?' class="active"':''; ?>><a href="<?php echo get_home_url(); ?>/tin-tuc/an-uong" title="Ăn uống"><i class="fa fa-cutlery"></i> Ăn uống</a></li>
                    <li<?= is_category('du-lich')?' class="active"':''; ?>><a href="<?php echo get_home_url(); ?>/tin-tuc/du-lich" title="Du lịch"><i class="fa fa-plane"></i> Du lịch</a></li>
                    <li<?= is_category('vui-choi')?' class="active"':''; ?>><a href="<?php echo get_home_url(); ?>/tin-tuc/vui-choi" title="Vui chơi"><i class="fa fa-users"></i> Vui chơi</a></li>
                    <li<?= is_category('giai-tri')?' class="active"':''; ?>><a href="<?php echo get_home_url(); ?>/tin-tuc/giai-tri" title="Giải trí"><i class="fa fa-trophy"></i> Giải trí</a></li>
                    <li<?= is_category('su-kien')?' class="active"':''; ?>><a href="<?php echo get_home_url(); ?>/tin-tuc/su-kien" title="Sự kiện"><i class="fa fa-users"></i> Sự kiện</a></li>
                </ul>

                <?php get_search_form(); ?>
            </nav>
        </div>
    </header>
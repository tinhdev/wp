        <footer class="footer ">
            <div class="container">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="<?php echo get_home_url(); ?>" title="Tổng quan" class="active"> Tổng quan</a>
                        <a href="<?php echo get_home_url(); ?>/tin-tuc/an-uong" title="Ăn uống"> Ăn uống</a>
                    </li>
                    <li>
                        <a href="<?php echo get_home_url(); ?>/tin-tuc/du-lich" title="Du lịch"> Du lịch</a>
                        <a href="<?php echo get_home_url(); ?>/tin-tuc/giai-tri" title="Giải trí"> Giải trí</a>
                    </li>
                    <li>
                        <a href="<?php echo get_home_url(); ?>/tin-tuc/vui-choi" title="Vui chơi"> Vui chơi</a>
                        <a href="<?php echo get_home_url(); ?>/tin-tuc/su-kien" title="Sự kiện"> Sự kiện</a>
                    </li>
                </ul>
                <div class="pull-right">
                    <a class="logo-footer" href="<?php echo get_site_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo-pc.png" />
                    </a>
                    <p><a href="mailto:ads.123dzo@gmail.com" title="">ads.123dzo@gmail.com</a></p>
                    <p><a href="tel:0973173929" title="">0973.173.929</a></p>
                </div>

                <a class="btn-round btn-round-g" href="https://www.facebook.com/banbe2.0" title="google plus"><i class="fa fa-google-plus"></i></a>
                <a class="btn-round" href="https://plus.google.com/u/0/b/117270960572010896121/117270960572010896121/about" title="facebook"><i class="fa fa-facebook"></i></a>
            </div>
        </footer>

        <?php wp_footer(); ?>

        <ul class="list-right">
            <li><a class="btn-round btn-top" href="javascript:void(0)" title=""><i class="fa fa-chevron-up"></i></a></li>
        </ul>

        <!-- modal detail -->
        <div class="modal fade detail-box" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/lib.js?v=<?= get_js_version(); ?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugin.js?v=<?= get_js_version(); ?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-select.min.js?v=<?= get_js_version(); ?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/start.js?v=<?= get_js_version(); ?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jstorage.min.js?v=<?= get_js_version(); ?>"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/tinhdev.js?v=<?= get_js_version(); ?>"></script>

        <!-- Facebook -->
        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '958535724235083',
                    cookie     : true,  // enable cookies to allow the server to access
                    // the session
                    xfbml      : true,  // parse social plugins on this page
                    version    : 'v2.7'
                });
            };

            function delete_callback() {
                fb_tag = $(".fb_comments_count_zero");
                span_tag = fb_tag.parent();
                lig_tag = span_tag.parent(".txt-cmt-li");
                lig_tag.remove();
                span_tag.remove();
            }

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/vi_VN/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

        </script>
        <!-- End Facebook -->

		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MBLV45"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-MBLV45');</script>
		<!-- End Google Tag Manager -->
    </body>
</html>
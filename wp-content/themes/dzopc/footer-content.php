<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 07/06/2015
 * Time: 22:12
 */
?>
<footer class="footer">
    <div class="container">
        <ul class="row">
            <li class="col-sm-4 col-xs-6">
                <dl>
                    <dt><a href="<?php echo get_site_url(); ?>">TRANG CHỦ</a></dt>
                    <dd><a href="<?php get_home_url(); ?>/tin-tuc/cong-dong-mang" title="">Cộng đồng mạng</a></dd>
                    <dd><a href="<?php get_home_url(); ?>/tin-tuc/cuoc-song-quanh-ta" title="">Cuộc sống quanh ta</a></dd>
                    <dd><a href="<?php get_home_url(); ?>/tin-tuc/cuoi-chut-choi" title="">Cười chút chơi</a></dd>
                    <dd><a href="<?php get_home_url(); ?>/tin-tuc/nong-tung-centimet" title="">Nóng từng centimet</a></dd>
                    <dd><a href="<?php get_home_url(); ?>/tin-tuc/the-gioi-muon-mau" title="">Thế giới muôn màu</a></dd>
                </dl>
            </li>
            <li class="col-sm-4 col-xs-6">
                <dl>
                    <dt><a href="<?php get_home_url(); ?>">DANH SÁCH</a></dt>
                    <dd><a href="<?php get_home_url(); ?>/xem-nhieu-nhat">Xem nhiều nhất</a></dd>
                    <dd><a href="<?php get_home_url(); ?>/binh-luan-nhieu-nhat">Bình luận nhiều nhất</a></dd>
                    <dd><a href="<?php get_home_url(); ?>/chia-se-nhieu-nhat">Chia sẻ nhiều nhất</a></dd>
                    <dd><a href="<?php get_home_url(); ?>/xem-theo-ngay">Xem theo ngày</a></dd>
                </dl>
            </li>
            <li class="col-sm-4 col-xs-6">
                <dl>
                    <dt><a href="#">LIÊN HỆ</a></dt>
                    <dd><a href="mailto:ads.123dzo@gmail.com">EMAIL: ads.123dzo@gmail.com</a></dd>
                    <dd><a href="tel:0946261550">0946.261.550</a></dd>
                    <dd>
                        <a target="_blank" href="https://www.facebook.com/banbe2.0" title="Facebook"><i class="icon-facebook-h"></i></a>
                        <a target="_blank" href="https://plus.google.com/u/0/b/117270960572010896121/117270960572010896121/about" title="Google plus"><i class="icon-google-h"></i></a>
                    </dd>
                    <dd>® 2015 Blog tin tức tổng hợp - 123DZO.NET</dd>
                </dl>
            </li>
        </ul>
    </div>
</footer>
<a class="btn-top" href="#" title="top"><i class="fa fa-chevron-up"></i></a>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/moment.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/start.js"></script>
<!--<script src="<?php //echo get_template_directory_uri(); ?>/js/dzo_ajax.js"></script>-->

<div id="fb-root"></div>
<span id = "comment_count_post" style="display: none" class="fb-comments-count" data-href="<?php echo get_the_permalink(); ?>"></span>
<script type="text/javascript">
    window.fbAsyncInit = function () {
        FB.init({
            appId: '102484926761574',
            xfbml: true,
            version: 'v2.3'
        });

        FB.Event.subscribe('comment.create', comment_callback);
        FB.Event.subscribe('comment.remove', comment_callback);
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function comment_callback (response) {
        var ajaxUrl = "<?php echo admin_url('admin-ajax.php')?>";
        //var ajaxUrl1 = '<?php //echo get_home_url(); ?>/wp-admin/admin-ajax.php';
        var comment_count = $("#comment_count_post > .fb_comments_count").html();

        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            data: {
                action: 'updateSocial',
                post_id: '<?php echo get_the_ID(); ?>',
                type: 'comment',
                comment_count: comment_count
            }
        });
    };

    $(function() {
        $( "#m-share" ).click(function() {
            var ajaxUrl = "<?php echo admin_url('admin-ajax.php')?>";
            $.ajax({
                type: 'POST',
                url: ajaxUrl,
                data: {
                    action: 'updateSocial',
                    post_id: '<?php echo get_the_ID(); ?>',
                    type: 'share'
                }
            });
        });
    });
</script>

</body>
</html>
<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
//var_dump("error");
//dump($_GET);
?>
    <div class="container">
        <div class="row">
            <section class="content col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="#" title="Trang chủ">Trang chủ</a></li>
                    <li><a class="active" href="#">404</a></li>
                </ol>
                <div class="highlight-ctn highlight-404">
                    <p>Nội dung không tìm thấy !</p>
                    <form class="frm-404" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="form-group">
                            <input type="text" class="form-control" name="s" id="s" placeholder="<?php esc_attr_e( 'Nội dung cần tìm', 'dzopc' ); ?>" />
                        </div>
                    </form>
                    <div class="block-absolute">
                        <img class="img-404" alt="" src="<?php echo get_template_directory_uri(); ?>/images/404.png">
                    </div>
                </div>
            </section>
        </div>
    </div>

<?php get_footer(); ?>

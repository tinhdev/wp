<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
<?php
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    $keyword = $_GET['s'];
    $date = isset($_GET['date']) ? str_replace('-', '/', $_GET['date']) : date('d/m/Y');
    $category = isset($_GET['cat']) ? $_GET['cat'] : '';
    $type = isset($_GET['type']) ? $_GET['type'] : 1;
?>
<div class="main container">
    <div class="row outer-ctn">
        <div class="content col-sm-8">
            <section class="highlight-1  highlight-food highlight-search">
                <h2><a href="#" title="">Tìm kiếm</a></h2>

                <div class="outer-search-1">
                    <form role="search" class="frm-search-1" method="get" id="searchform-page" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-9">
                                    <input type="text" name="s" id="s" class="form-control txt-tk" placeholder="Nội dung cần tìm" value="<?php echo $keyword; ?>"/>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-timkiem">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="radio" name="type" value="1" <?php echo $type == 1 ? "checked" : ""; ?> > Tất cả
                            </label>
                            <label>
                                <input type="radio" name="type" value="2" <?php echo $type == 2 ? "checked" : ""; ?> > Tiêu đề
                            </label>
                            <label>
                                <input type="radio" name="type" value="3" <?php echo $type == 3 ? "checked" : ""; ?> > Nội dung
                            </label>
                        </div>
                        <div class="form-group">
                            <select name="cat" id="cat" class="form-control">
                                <option value="">Tất cả chuyên mục</option>
                                <?php
                                $cats = get_categories();
                                foreach($cats as $cat) {
                                    if ($cat->slug == $category): ?>
                                        <option value="<?php echo $cat->slug; ?>" selected><?php echo $cat->cat_name; ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $cat->slug; ?>"><?php echo $cat->cat_name; ?></option>
                                    <?php endif; ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input name="date" type='text' class="form-control" value="<?php echo $date; ?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <input type="hidden" name="page" id="page" value="<?= $page; ?>">
                    </form>
                    <p>Kết quả tìm kiếm cho từ khóa "<span style="color: #00AC77;"><?= $keyword; ?></span>"</p>
                </div>
            </section>
            <div class="highlight-ctn" id="custom_list">
                <?php
                if (isset($_GET['s']) && ($_GET['s'] == '')
                    && isset($_GET['type']) && ($_GET['type'] == '')
                    && isset($_GET['date']) && ($_GET['date'] == '') )
                { ?>
                    <h1 class="pagetitle">0 kết quả</h1>
                <?php
                } else {
                    $limit = 10;
                    $count = 0;
                    $posts = mysearch($limit);
                    global $post;
                    foreach ($posts as $post):
                    //while ($postlist -> have_posts()) : $postlist -> the_post();
                        $count++;
                        setup_postdata($post);
                        $title_post = get_the_title();
                        $excerpt = stringTrunc(get_the_excerpt(), 35);
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );

                        $post_link = get_permalink(get_the_ID());
                        $cat_list = get_the_category(get_the_ID());
                        $post_link = rewrite_link_post_vedette($post_link, $cat_list);
                        ?>
                        <article class="art">
                            <a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?= $title_post; ?>">
                                <img src="<?= get_template_directory_uri(); ?>/images/trans.png" width="185" height="104" style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                            </a>

                            <h2><a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title=""><?php echo $title_post; ?></a></h2>
                            <ul class="tool-bar list-inline">
                                <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                |
                                <li class="date"><?php echo the_time('d.m.Y'); ?> GMT + 7</li>
                            </ul>
                            <p><?php echo $excerpt; ?></p>
                            <span class="note"><?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?></span>
                        </article>
                    <?php
                        //endwhile;
                    endforeach;
                    wp_reset_postdata();
                    if ($count >= $limit) {
                        echo '<a class="btn-view js-search-view-more" href="javascript:void(0)" title="">Xem thêm</a>';
                    }
                } ?>
            </div>
        </div>
        <aside class="sidebar col-sm-4">
            <?php dynamic_sidebar( 'sidebar-3' ); ?>
        </aside>
    </div> <!-- #row -->
</div><!-- #container -->

<?php
get_footer(); ?>

<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 10/06/2015
 * Time: 17:12
 */

class dzo_most_viewed_widget extends WP_Widget {
    public function dzo_most_viewed_widget() {
        $dzo_most_viewed_widget_options = array(
            'classname' => 'dzo_most_comment_widget_class', //ID của widget
            'description' => 'Hiển thị tin bài nhiều comment nhất'
        );
        $this->WP_Widget('dzo_most_viewed_widget_id', 'dzo_most_viewed_widget', $dzo_most_viewed_widget_options);
    }

    public function widget( $args, $instance ) {
        $title = isset($instance['title']) ? $instance['title'] : "";
        $pos = isset($instance['pos']) ? $instance['pos'] : "";
        $date = isset($instance['date']) ? $instance['date'] : "10";
        $number = isset($instance['number']) ? $instance['number'] : 5;
        //$date = 100;
        $date_filter_string = '-'.$date.' days';
        $date_condition = date('Y-m-d H:i:s', strtotime($date_filter_string));

        //if (!isset($_GET['r'])) return;
        //else
        $slug = explode('/', $_GET['r']);
        $size_slug = sizeof($slug);
        if (isset($slug[$size_slug-2]) && $slug[$size_slug-2] == 'page')
            $paged = $slug[$size_slug-1];
        else
            $paged = 1;

        $offset = $paged*$number - $number;

        global $wpdb;
        $sqlstr = "SELECT * FROM dzo_posts WHERE post_type = 'post' AND post_status = 'publish' AND post_date > '".$date_condition."' ORDER BY post_view DESC, post_date DESC limit ".$number." OFFSET ".$offset;
        //dump($sqlstr);
        $posts = $wpdb->get_results($sqlstr);

        if ($pos == "home") {
            if (sizeof($posts) > 0) {
                //shuffle ($posts); ?>
                <section class="highlight-top">
                    <div class="flexslider flexslider-new">
                        <ul class="slides">
                <?php
                    foreach ($posts as $i => $post) {
                        $title_post = $post->post_title;
                        $link = buildLinkPost(get_permalink($post->ID), $post->ID);
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                ?>
                        <li>
                            <article class="art-1">
                                <a class="js-view-post" href="<?= $link; ?>" data-id="<?= $post->ID; ?>" title="<?= $title_post; ?>">
                                    <img src="<?= get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?= $url[0]; ?>) no-repeat top left; height: 92px !important;"/>
                                </a>
                                <h3><a class="js-view-post" href="<?= $link; ?>" data-id="<?= $post->ID; ?>"  title="<?= $title_post; ?>"><?= stringTrunc($title_post, 7); ?></a></h3>
                            </article>
                        </li>
                <?php
                    }
                ?>
                        </ul>
                    </div>
                </section>
            <?php
            }
        }
        else if ($pos == "right") :
            if (sizeof($posts) > 0): ?>
                <div class="block-bar block-mostview">
                    <h3><a href="<?php echo get_home_url().'/xem-nhieu-nhat'; ?>" title="Xem nhiều nhất">Xem nhiều nhất</a></h3>
                    <ul>
                <?php
                $counter = 1;
                foreach ($posts as $post):
                    //setup_postdata($post);
                    $title_post = $post->post_title;
                    //$link = get_permalink($post->ID);
                    $link = buildLinkPost(get_permalink($post->ID), $post->ID);
                    if ($counter == 1):
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
                        $format_post = get_post_format($post->ID);
                        ?>
                        <li>
                            <a class="thumb resize resize-1" href="<?php echo $link; ?>" title=""><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"> <span class="number">1</span>
                                <?php echo echoSpan($format_post); ?>
                            </a>
                            <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                        </li>
                    <?php else: ?>
                        <li>
                            <span class="number"><?php echo $counter; ?></span>
                            <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                        </li>
                    <?php
                    endif;
                    $counter++;
                endforeach;
                wp_reset_query();
                ?>
                </ul>
                </div>
                <?php
            endif;
        endif;
        if ($pos == "left") :
            if (sizeof($posts) > 0): ?>
                <ol class="breadcrumb">
                    <li class="active"><?php echo $title; ?></li>
                </ol>

                        <?php
                        global $post;
                        $count = 1;
                        foreach ($posts as $post):
                            setup_postdata($post);
                            //$posts->the_post();
                            //$title_post = $post->post_title;
                            $title_post = get_the_title();
                            $excerpt = stringTrunc($post->post_excerpt, 35);
                            $format_post = get_post_format($post->ID);

                            //$link = get_permalink ($post->ID);
                            $link = buildLinkPost(get_permalink($post->ID), $post->ID);
                            ?>
                            <?php if ($count == 1):
                            $count++;
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large' );
                            ?>
                            <div class="highlight-top row">
                                <article class="art-feature cate-inside">
                                    <a class="resize" href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                        <?php echo echoSpan($format_post); ?>
                                    </a>
                                    <div class="detail">
                                        <a class="type" href="#" title="">Chuyện trong nhà</a>
                                        <h2><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count> bình luận</span></h2>
                                    </div>
                                </article>
                            </div>
                            <?php else:
                                $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail' );
                            ?>
                            <?php if ($count == 2): ?>
                                <div class="highlight-ctn row">
                                <!--<div id="custom_list">-->
                            <?php endif; ?>
                                <input type="hidden" value="<?php echo $number; ?>" class="page_limit"/>
                                <article class="art">
                                    <a class="resize" href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                        <?php echo echoSpan($format_post); ?>
                                    </a>
                                    <h2><a href="<?php echo $link; ?>" title=""><?php echo $title_post; ?></a></h2>
                                    <ul class="tool-bar list-inline">
                                        <li><span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count> bình luận</span></li> |
                                        <li class="date"><?php echo the_time('d.m.Y H:i'); ?> GMT + 7</li>
                                    </ul>
                                    <p><?php echo $excerpt; ?></p>
                                    <div class="tag-name">
                                        <i class="fa fa-tags"></i>
                                        <?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?>
                                    </div>
                                </article>
                            <?php
                            $count++;
                            endif; ?>

                        <?php endforeach;
                        wp_reset_postdata();
                        wp_reset_query();
                        ?>
                    </div>
                    <?php
                    $max = sizeof($posts);
                    if ($max < 0) { // check if the max number of pages is greater than 1 ?>
                        <nav class="pagging">
                            <ul class="pagination pagination-sm">
                                <!--<li>
                                    <a href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li> -->
                                <?php
                                $number_page = $max < 6 ? $max : 5;
                                for($i=1; $i <= $number_page; $i++) { ?>
                                    <li><a href="javascript:void(0);" class="page_number"><?php echo $i; ?></a></li>
                                <?php } ?>
                                <?php if ($number_page < $max): ?>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    <?php }
                    // wp_reset_query();
                    ?>

<!--                </div>-->
                <?php
            endif;
        endif;
    }

    function update( $new_instance, $old_instance) {
        parent::update( $new_instance, $old_instance );
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['pos'] = strip_tags($new_instance['pos']);
        $instance['date'] = strip_tags($new_instance['date']);
        $instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Title',
            'pos' => 'right',
            'date' => 'Number last day',
            'number' => 'Number'
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $pos = esc_attr( $instance['pos'] );
        $date = esc_attr( $instance['date'] );
        $number = esc_attr( $instance['number'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title); ?>"></p>


        <p><label for="<?php echo esc_attr( $this->get_field_id( 'date' ) ); ?>"><?php _e( 'Number last day:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'date' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'date' ) ); ?>" type="text" value="<?php echo esc_attr( $date ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'pos' ) ); ?>"><?php _e( 'Position:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'pos' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'pos' ) ); ?>" type="text" value="<?php echo esc_attr( $pos ); ?>"></p>

    <?php
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 12/06/2015
 * Time: 09:49
 */

class dzo_customlist_widget extends WP_Widget {
    public function dzo_customlist_widget() {
        $dzowidget_options = array(
            'classname' => 'dzo_customlist_widget_id', //ID của widget
            'description' => 'Hiển thị tin bài featured news'
        );
        $this->WP_Widget('dzo_customlist_widget_id', 'dzo_customlist_widget', $dzowidget_options);
    }
    function update( $new_instance, $old_instance ) {
        parent::update( $new_instance, $old_instance );

        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Tiêu đề widget',
            'number' => 5
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $number = esc_attr( $instance['number'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"></p>
        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>
    <?php
    }

    public function widget( $args, $instance )
    {
        //$title = isset($instance['title']) ? $instance['title'] : "";
        $number = isset($instance['number']) ? $instance['number'] : 5;
        //$cat = isset($instance['category']) ? $instance['category'] : "";
        if (!isset($_GET['r'])) return;
        else $slug = explode('/', $_GET['r']);
        $size_slug = sizeof($slug);
        if($size_slug < 2) return;
        //$cat_slug = isset($slug[$size_slug-1]) ? $slug[$size_slug-1] : '';
        if (isset($slug[$size_slug-2]) && $slug[$size_slug-2] == 'page')
            $paged = $slug[$size_slug-1];
        else
            $paged = 1;

        $cat_slug = isset($slug[1]) ? $slug[1] : '';
        //$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

        $query_args = array(
            'post_type' => 'post',
            'category_name' =>  $cat_slug,
            'posts_per_page' => $number,
            'paged' => $paged
        );
        $the_query = new WP_Query( $query_args );
        ?>

        <?php if ( $the_query->have_posts() ) :
                $count = 1;
            while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop
                $title_post = get_the_title();
                $excerpt = stringTrunc(get_the_excerpt(), 35);
                $format_post = get_post_format(get_the_ID());
                $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                //$tag = the_tags( '<a>', '</a>, <a>', '</a>' );
                ?>
                <?php
                if ($paged == 1 && $count >=1 && $count <=3) :
                    if ($count == 1):
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
                        ?>
                        <div class="highlight-top row">
                        <article class="art-feature cate-inside">
                            <a href="<?php echo the_permalink(); ?>" title="">
                                <img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                <?php echo echoSpan($format_post); ?>
                            </a>
                            <div class="detail">
                                <a class="type" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $cat->name; ?></a>
                                <h2><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a>
                                    <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span>
                                </h2>
                            </div>
                        </article>
                    <?php
                    endif;
                    if ($count != 1):
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                        ?>
                        <article class="art-top-2 cate-inside">
                            <a href="<?php echo the_permalink(); ?>" title=""><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                            <div class="detail">
                                <a class="type" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $cat->name; ?></a>
                                <h2><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a>
                                    <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span>
                                </h2>
                            </div>
                        </article>
                    <?php endif;
                    if ($count == 3) echo "</div>";
                    ?>
                <?php else:
                    if ($paged == 1 && $count == 4)
                        echo '<div class="highlight-ctn row">';
                    if ($paged != 1 && $count == 1)
                        echo '<div class="highlight-ctn row">'; ?>

                    <article class="art">
                        <a class="resize" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                            <?php echo echoSpan($format_post); ?>
                        </a>
                        <h2><a href="<?php echo the_permalink(); ?>" title=""><?php echo $title_post; ?></a></h2>
                        <ul class="tool-bar list-inline">
                            <li class="txt-cmt-li"><span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận </span> |</li>
                            <li class="date"><?php echo the_time('d.m.Y H:i'); ?> GMT + 7</li>
                        </ul>
                        <p><?php echo $excerpt; ?></p>
                        <div class="tag-name">
                            <i class="fa fa-tags"></i>
                            <?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?>
                        </div>
                    </article>
                <?php endif;
                $count++;
                ?>
            <?php endwhile; ?>
        <?php
        $max = intval($the_query->max_num_pages );
        if ($max > 1) { // check if the max number of pages is greater than 1 ?>
            <nav class="pagging">
                <ul class="pagination pagination-sm">
                    <?php pagination(); ?>
                </ul>
            </nav>
        <?php }
        wp_reset_postdata();
        wp_reset_query();
        ?>
        <?php endif; ?>
        </div>
    <?php
    }

//    public function apply_body_class($slug) {
//        if ($slug == "")
//    }
}
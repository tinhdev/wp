<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 08/06/2015
 * Time: 13:06
 */

class Dzo_webpart_widget extends WP_Widget {
    public function dzo_webpart_widget() {
        $dzo_webpart_widget_options = array(
            'classname' => 'dzo_webpart_widget_class', //ID của widget
            'description' => 'Hiển thị tin bài webpart trang chủ'
        );
        $this->WP_Widget('dzo_webpart_widget_id', 'dzo_webpart_widget', $dzo_webpart_widget_options);
    }
    public function widget( $args, $instance ) {
        $title = isset($instance['title']) ? $instance['title'] : "";
        $color = isset($instance['color']) ? $instance['color'] : "food";
        $cat = isset($instance['category']) ? $instance['category'] : "";
        $number = isset($instance['number']) ? $instance['number'] : 6;
        $cate_link = dzo_makeUrl($cat);

        if ($color == "food") {
            $postlist = getPostByCatAndStick($cat, $number, true);
            if (sizeof($postlist->posts) > 0) { ?>
                <section class="highlight-1 highlight-food">
                    <h2><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
                    <div class="row">
                    <?php
                    while ($postlist -> have_posts()) : $postlist -> the_post();
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        $excerpt = stringTrunc(get_the_excerpt(), 30);
                        $title_post = get_the_title();

                        $post_link = get_permalink(get_the_ID());
                        $cat_list = get_the_category(get_the_ID());
                        $post_link = rewrite_link_post_vedette($post_link, $cat_list);

                        ?>
                            <article class="art-1 col-md-2 col-sm-3">
                                <a href="<?= $post_link; ?>" title="<?php echo $title_post; ?>">
                                    <img src="<?= get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                                </a>
                                <h3><a href="<?= $post_link; ?>" title="<?php echo $title_post; ?>"><?= stringTrunc($title_post, 10); ?></a></h3>
                            </article>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    wp_reset_query();
                    ?>
                        <a class="btn-view" href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>">Xem tất cả</a>
                    </div>
                </section>
            <?php
            }
        }
        elseif ($color == 'travel') {
            $posts = getPostByCatAndStick($cat, $number, true);
            if (sizeof($posts->posts) > 0) {
                ?>
                <section class="highlight-1 highlight-travel">
                    <h2><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
                    <div class="row">
                <?php
                while ($posts -> have_posts()) : $posts -> the_post();
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                    $title_post = get_the_title();
                    $excerpt = stringTrunc(get_the_excerpt(), 35);

                    $post_link = get_permalink(get_the_ID());
                    $cat_list = get_the_category(get_the_ID());
                    $post_link = rewrite_link_post_vedette($post_link, $cat_list);
                    ?>
                        <article class="art-1 col-sm-2">
                            <a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>">
                                <img src="<?= get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                            </a>
                            <h3><a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>"><?php echo stringTrunc($title_post, 10); ?></a></h3>
                        </article>
                    <?php
                endwhile;
                wp_reset_postdata();
                wp_reset_query();
                ?>
                        <a class="btn-view" href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>">Xem tất cả</a>
                    </div>
                </section>
                <?php
            }
        }
        elseif ($color == "enter") {
            $posts = getPostByCatAndStick($cat, $number, true);
            if (sizeof($posts->posts) > 0) {?>
                <section class="highlight-1 highlight-enter">
                    <h2><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
                    <div class="row">
                    <?php
                    while ($posts -> have_posts()) : $posts -> the_post();
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        $title_post = stringTrunc(get_the_title(), 12);

                        $post_link = get_permalink(get_the_ID());
                        $cat_list = get_the_category(get_the_ID());
                        $post_link = rewrite_link_post_vedette($post_link, $cat_list);
                        ?>
                        <article class="art-1 col-sm-2">
                            <a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>">
                                <img src="<?= get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                            </a>
                            <h3><a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>"><?php echo stringTrunc($title_post, 10); ?></a></h3>
                        </article>
                    <?php endwhile;
                    wp_reset_postdata();
                    wp_reset_query();
                    ?>
                        <a class="btn-view" href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>">Xem tất cả</a>
                    </div>
                </section>
            <?php
            }
        }
        elseif ($color == "spent") {
            if ($number%3 != 0) $limit = $number - ($number%3);
            else $limit = $number;
            $postlist = getPostByCatAndStick($cat, $limit, true);
            if (sizeof($postlist->posts) > 0) {?>
                <section class="highlight-1 highlight-spent">
                    <h2><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
                    <div class="row">
                <?php
                    while ($postlist -> have_posts()) : $postlist -> the_post();
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        $title_post = get_the_title();

                        $post_link = get_permalink(get_the_ID());
                        $cat_list = get_the_category(get_the_ID());
                        $post_link = rewrite_link_post_vedette($post_link, $cat_list);
                        ?>
                        <article class="art-1 col-md-2 col-sm-3">
                            <a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>">
                                <img src="<?= get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                            </a>
                            <h3><a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>"><?php echo stringTrunc($title_post, 10); ?></a></h3>
                        </article>
                    <?php
                    endwhile;
                    wp_reset_postdata();
                    wp_reset_query();
                    ?>
                        <a class="btn-view" href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>">Xem tất cả</a>
                    </div>
                </section>
            <?php
            }
        }
        elseif ($color == "event") {
            if ($number%5 != 0) $limit = $number - ($number%5);
            else $limit = $number;
            $postlist = getPostByCatAndStick($cat, $limit, true);
            if (sizeof($postlist->posts) > 0) {?>
                <section class="highlight-1 highlight-event">
                    <h2><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
                    <div class="row">
                <?php
                    while ($postlist -> have_posts()) : $postlist -> the_post();
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        $excerpt = stringTrunc(get_the_excerpt(), 14);
                        $title_post = get_the_title();

                        $post_link = get_permalink(get_the_ID());
                        $cat_list = get_the_category(get_the_ID());
                        $post_link = rewrite_link_post_vedette($post_link, $cat_list);
                        ?>
                        <article class="art-1 col-sm-2">
                            <a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>">
                                <img src="<?= get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                            </a>
                            <div class="ctn">
                                <h3><a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?= $post_link; ?>" title="<?php echo $title_post; ?>"><?php echo stringTrunc($title_post, 10); ?></a></h3>
                                <p><?= $excerpt; ?></p>
                            </div>
                        </article>
                    <?php
                    endwhile;
                    wp_reset_postdata();
                    wp_reset_query();
                    ?>
                        <a class="btn-view" href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>">Xem tất cả</a>
                    </div>
                </section>
            <?php
            }
        }
        //Begin - PhongTX
        //Widget Thoi Trang
        elseif ($color == "thoitrang") {
            ?>
            <!-- TRACKING GOOGLE -->
            <script type="text/javascript">
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-78641839-1', 'auto');
                ga('send', 'pageview');

            </script>
            <!-- END TRACKING GOOGLE -->
            <?php
            $siteUrl = 'http://thoitrangsaigon.vn';
            $apiUrl = 'http://thoitrangsaigon.vn/api/index/secretcode/16d67d0243508c07157ea35d0a0c89db/limit/10';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            $result = curl_exec($ch);
            curl_close($ch);
            $arrData = json_decode($result,true);
            if (!empty($arrData['data'])) {?>
                <section class="highlight-1 highlight-travel">
                    <h2><a href="<?php echo $siteUrl; ?>" target="_blank" title="<?php echo htmlspecialchars($title, ENT_QUOTES); ?>"><?php echo $title; ?></a></h2>
                    <div class="row">
                        <?php
                        foreach($arrData['data'] as $row) {
                            $imgUrl = $siteUrl.'/static/uploads/thumbnails/thumbs/' . preg_replace('/\.[^.]+$/', '_490x294' . '$0', $row['thumb_url']);
                            ?>
                            <article class="art-1 col-sm-2">
                                <a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?php echo $siteUrl . $row['share_url']; ?>" target="_blank" title="<?php echo htmlspecialchars($row['title'], ENT_QUOTES); ?>">
                                    <img alt="<?php echo htmlspecialchars($row['title'], ENT_QUOTES); ?>" src="<?= get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?php echo $imgUrl; ?>) no-repeat top left;"/>
                                </a>
                                <h3><a class="js-view-post" data-id="<?= get_the_ID(); ?>" href="<?php echo $siteUrl . $row['share_url']; ?>" target="_blank" title="<?php echo htmlspecialchars($row['title'], ENT_QUOTES); ?>"><?php echo stringTrunc($row['title'], 10); ?></a></h3>
                            </article>
                        <?php
                        }
                        ?>
                        <a class="btn-view" href="<?php echo $siteUrl; ?>" target="_blank" title="Thời Trang">Xem tất cả</a>
                    </div>
                </section>
            <?php
            }
        }
        //End - PhongTX
        if ($color == "green_3_column"):
            global $wpdb;
            //$number = 5;
            $offset = 0;

            $date = 30;
            $date_filter_string = '-'.$date.' days';
            $date_condition = date('Y-m-d H:i:s', strtotime($date_filter_string));

            $most_viewed_sql_str = "SELECT * FROM dzo_posts WHERE post_type = 'post' AND post_status = 'publish' AND post_date > '".$date_condition."' ORDER BY post_view DESC, post_date DESC limit ".$number." OFFSET ".$offset;
            $most_viewed_posts = $wpdb->get_results($most_viewed_sql_str);

            $most_shared_sql_str = "SELECT * FROM dzo_posts WHERE post_type = 'post' AND post_status = 'publish' AND post_date > '".$date_condition."' ORDER BY share_count DESC, post_date DESC limit ".$number." OFFSET ".$offset;
            $most_shared_posts = $wpdb->get_results($most_shared_sql_str);

            $most_commented_sql_str = "SELECT * FROM dzo_posts WHERE post_type = 'post' AND post_status = 'publish' AND post_date > '".$date_condition."' ORDER BY comment_count DESC, post_date DESC limit ".$number." OFFSET ".$offset;
            $most_commented_posts = $wpdb->get_results($most_commented_sql_str);

            wp_reset_query();
        ?>
        <div class="highlight-1 row">
			<div class="block-bar block-mostview col-sm-4 col-xs-4">
				<h3><a href="<?php echo get_home_url().'/xem-nhieu-nhat'; ?>" title="">Xem nhiều nhất</a></h3>
				<ul>
				<?php
                    $counter = 1;
                    foreach ($most_viewed_posts as $post):
                        $title_post = $post->post_title;
                        $link = get_permalink($post->ID);
                        if ($counter == 1):
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
                            //$format_post = get_post_format($post->ID);
                            ?>
                            <li>
                                <a class="thumb resize resize-1" href="<?php echo $link; ?>" title=""><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"> <span class="number">1</span>
                                    <?php //echo echoSpan($format_post); ?>
                                </a>
                                <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php else: ?>
                            <li>
                                <span class="number"><?php echo $counter; ?></span>
                                <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php
                        endif;
                        $counter++;
                    endforeach;
                ?>
				</ul>
			</div>
			<div class="block-bar block-mostview col-sm-4 col-xs-4"">
				<h3><a href="<?php echo get_home_url().'/chia-se-nhieu-nhat'; ?>" title="">CHIA SẺ NHIỀU NHẤT</a></h3>
				<ul>
				<?php
                    $counter = 1;
                    foreach ($most_shared_posts as $post):
                        $title_post = $post->post_title;
                        $link = get_permalink($post->ID);
                        if ($counter == 1):
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
                            //$format_post = get_post_format($post->ID);
                            ?>
                            <li>
                                <a class="thumb resize resize-1" href="<?php echo $link; ?>" title=""><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    <?php //echo echoSpan($format_post); ?>
                                </a>
                                <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php else: ?>
                            <li>
                                <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php
                        endif;
                        $counter++;
                    endforeach;
                ?>
				</ul>
			</div>
			<div class="block-bar block-mostview col-sm-4 col-xs-4"">
				<h3><a href="<?php echo get_home_url().'/binh-luan-nhieu-nhat'; ?>" title="">BÌNH LUẬN NHIỀU NHẤT</a></h3>
				<ul>
				<?php
                    $counter = 1;
                    foreach ($most_commented_posts as $post):
                        $title_post = $post->post_title;
                        $link = get_permalink($post->ID);
                        if ($counter == 1):
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
                            //$format_post = get_post_format($post->ID);
                            ?>
                            <li>
                                <a class="thumb resize resize-1" href="<?php echo $link; ?>" title=""><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    <?php //echo echoSpan($format_post); ?>
                                </a>
                                <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php else: ?>
                            <li>
                                <h4><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink($post->ID); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php
                        endif;
                        $counter++;
                    endforeach;
                ?>
				</ul>
			</div>
		</div>
        <?php endif;


    }

    function update( $new_instance, $old_instance) {
        parent::update( $new_instance, $old_instance );
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['color'] = strip_tags($new_instance['color']);
        $instance['category'] = strip_tags($new_instance['category']);
        $instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Title',
            'color' => 'Color',
            'category' => 'Category',
            'number' => 'Number'
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $color = esc_attr( $instance['color'] );
        $category = esc_attr( $instance['category'] );
        $number = esc_attr( $instance['number'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>"><?php _e( 'Color:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'color' ) ); ?>" type="text" value="<?php echo esc_attr( $color); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php _e( 'Category:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>" type="text" value="<?php echo esc_attr( $category ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>

    <?php
    }
}
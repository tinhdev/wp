<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 12/06/2015
 * Time: 09:49
 */

class dzo_customlist_widget extends WP_Widget {
    public function dzo_customlist_widget() {
        $dzowidget_options = array(
            'classname' => 'dzo_customlist_widget_id', //ID của widget
            'description' => 'Hiển thị tin bài featured news'
        );
        $this->WP_Widget('dzo_customlist_widget_id', 'dzo_customlist_widget', $dzowidget_options);
    }
    function update( $new_instance, $old_instance ) {
        parent::update( $new_instance, $old_instance );

        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Tiêu đề widget',
            'number' => 5
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $number = esc_attr( $instance['number'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"></p>
        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>
    <?php
    }

    public function widget( $args, $instance )
    {
        //$title = isset($instance['title']) ? $instance['title'] : "";
        $number = isset($instance['number']) ? $instance['number'] : 5;
        //$cat = isset($instance['category']) ? $instance['category'] : "";
        if (!isset($_GET['r'])) return;
        else $slug = explode('/', $_GET['r']);
        $size_slug = sizeof($slug);
        $cat_slug = isset($slug[$size_slug-1]) ? $slug[$size_slug-1] : '';
        $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

        var_dump($_GET);

        $query_args = array(
            'post_type' => 'post',
            'category_name' =>  $cat_slug,
            'posts_per_page' => $number,
            'paged' => $paged
        );
        $the_query = new WP_Query( $query_args );
        ?>
        <div class="highlight-ctn row">
            <div id="custom_list">
                <input type="hidden" value="<?php echo $cat_slug; ?>" class="cat_name"/>
                <input type="hidden" value="<?php echo $number; ?>" class="page_limit"/>
                <?php if ( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop
                    $title_post = get_the_title();
                    $excerpt = stringTrunc(get_the_excerpt(), 35);
                    $format_post = get_post_format(get_the_ID());
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                    //$tag = the_tags( '<a>', '</a>, <a>', '</a>' );
                    ?>
                    <article class="art">
                        <a class="resize" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                            <?php echo echoSpan($format_post); ?>
                        </a>
                        <h2><a href="<?php echo the_permalink(); ?>" title=""><?php echo $title_post; ?></a></h2>
                        <ul class="tool-bar list-inline">
                            <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li> |
                            <li class="date"><?php echo the_time('d.m.Y'); ?> GMT + 7</li>
                        </ul>
                        <p><?php echo $excerpt; ?></p>
                        <div class="tag-name">
                            <i class="fa fa-tags"></i>
                            <?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?>
                        </div>
                    </article>
                <?php endwhile; ?>
            </div>
            <?php
            $max = intval($the_query->max_num_pages );
            if ($max > 1) { // check if the max number of pages is greater than 1 ?>
                <nav class="pagging">
                    <ul class="pagination pagination-sm">
                        <!--<li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li> -->
                        <?php
                        $number_page = $max < 6 ? $max : 5;
                        for($i=1; $i <= $number_page; $i++) { ?>
                            <li><a href="javascript:void(0);" class="page_number"><?php echo $i; ?></a></li>
                        <?php } ?>
                        <?php if ($number_page < $max): ?>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </nav>
            <?php }
            wp_reset_postdata();
            wp_reset_query();
            ?>
            <?php endif; ?>

        </div>

        <script src="<?php echo get_home_url(); ?>/wp-content/themes/dzopc/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $(".page_number").click(function(){
                    var page_number = parseInt($(this).text());
                    var cat = $('.cat_name').val();
                    var limit = $('.page_limit').val();
                    $.ajax({
                        type: 'GET',
                        url: '<?php echo get_home_url(); ?>/wp-admin/admin-ajax.php',
                        data: {
                            action: 'MyAjaxFunction',
                            page_number: page_number,
                            cat: cat,
                            limit: limit
                        },
                        success: function(data, textStatus, XMLHttpRequest){
                            if (data != 0 && data != '0') {
                                data = data.substring(0, data.length - 1);
                                // alert(data);
                                $('#custom_list').replaceWith(data);
                            }
                        },
                        error: function(MLHttpRequest, textStatus, errorThrown){
                            alert(errorThrown);
                        }
                    });
                });
            });
        </script>

    <?php
    }

//    public function apply_body_class($slug) {
//        if ($slug == "")
//    }
}
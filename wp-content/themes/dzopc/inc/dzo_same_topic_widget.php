<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 08/06/2015
 * Time: 11:06
 */

class Dzo_same_topic_widget extends WP_Widget {
    public function dzo_same_topic_widget() {
        $dzowidget_options = array(
            'classname' => 'dzo_same_topic_widget_class', //ID của widget
            'description' => 'Hiển thị tin bài featured news'
        );
        $this->WP_Widget('dzo_same_topic_widget_id', 'dzo_same_topic_widget', $dzowidget_options);
    }

    public function widget( $args, $instance ) {
        $title = isset($instance['title']) ? $instance['title'] : "";
        $number = isset($instance['number']) ? $instance['number'] : 10;
        $link = isset($instance['link']) ? $instance['link'] : '#';
        $pos = isset($instance['pos']) ? $instance['pos'] : "right";

        $tags = wp_get_post_tags(get_the_ID());
        $string_tags = array();

        foreach($tags as $tag)
        {
            $string_tags[] = $tag->term_id;
        }
        //dump($string_tags);
        $args = array(
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'post__not_in' => array(get_the_ID()),
            'order' => 'DESC',
            'posts_per_page' => $number,
            'post_type' => 'post',
            'tag__in' => $string_tags
        );
        $the_query = new WP_Query( $args );
        if ($pos == "right") :
            ?>
            <div class="block-bar block-mostview">
                <h3><a href="<?php echo $link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
                <ul>
                    <?php
                    $count = 1;
                    while ( $the_query->have_posts() && $count < $number) : $the_query->the_post();
                        $title_post = get_the_title();

                        if ($count == 1):
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                            $format_post = get_post_format(get_the_ID());
                            ?>
                            <li>
                                <a class="thumb resize resize-1" href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    <?php echo echoSpan($format_post); ?>
                                </a>
                                <h4><a href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php else: ?>
                            <li>
                                <h4><a href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>">​<?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count></span></h4>
                            </li>
                        <?php endif; ?>
                        <?php
                        if ($count == $number) {
                            break;
                        } else {
                            $count++;
                        }
                    endwhile; ?>
                </ul>
            </div>
            <?php
            wp_reset_postdata();
            //wp_reset_query();
        endif;

        // For left block
        if ($pos == "left") :
            if ($the_query->have_posts()): ?>
                <ol class="breadcrumb">
                    <li class="active"><?php echo $title; ?></li>
                </ol>
                <?php
                $count = 1;
                while ($the_query->have_posts()) : $the_query->the_post();
                    //$posts->the_post();
                    //$title_post = $post->post_title;
                    $title_post = get_the_title();
                    $excerpt = stringTrunc(get_the_excerpt(), 35);
                    $format_post = get_post_format();
                    $link = get_permalink ();
                    //$tag = the_tags( '<a>', '</a>, <a>', '</a>' );
                    //$date = date('d.m.Y', strtotime($post->post_date));
                    //$date = the_time('j.m.Y');
                    ?>
                    <?php if ($count == 1):
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large' );
                        ?>
                        <div class="highlight-top row">
                            <article class="art-feature cate-inside">
                                <a class="resize" href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    <?php echo echoSpan($format_post); ?>
                                </a>
                                <div class="detail">
                                    <a class="type" href="#" title="">Chuyện trong nhà</a>
                                    <h2><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span></h2>
                                </div>
                            </article>
                        </div>
                    <?php else:
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                        ?>
                        <?php if ($count == 2): ?>
                            <div class="highlight-ctn row">
                            <!--<div id="custom_list">-->
                        <?php endif; ?>
                        <input type="hidden" value="<?php echo $number; ?>" class="page_limit"/>
                        <article class="art">
                            <a class="resize" href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                <?php echo echoSpan($format_post); ?>
                            </a>
                            <h2><a href="<?php echo $link; ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h2>
                            <ul class="tool-bar list-inline">
                                <li><span class="txt-cm"><i class="fa fa-comments"></i> <fb:comments-count href="<?php echo get_permalink(get_the_ID()); ?>"></fb:comments-count> bình luận</span></li> |
                                <li class="date"><?php echo the_time('d.m.Y H:i'); ?> GMT + 7</li>
                            </ul>
                            <p><?php echo $excerpt; ?></p>
                            <div class="tag-name">
                                <i class="fa fa-tags"></i>
                                <?php the_tags( '<a>', '</a>; <a>', '</a>' ); ?>
                            </div>
                        </article>
                    <?php endif; ?>
                            <?php
                            if ($count == $number) {
                                break;
                            } else {
                                $count++;
                            }
                        endwhile;
                        wp_reset_postdata();
                        //wp_reset_query();
                        ?>
                    </div>
                    <?php
                    $max = intval($the_query->max_num_pages );
                    if ($max < 0) { // check if the max number of pages is greater than 1 ?>
                        <nav class="pagging">
                            <ul class="pagination pagination-sm">
                                <!--<li>
                                    <a href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li> -->
                                <?php
                                $number_page = $max < 6 ? $max : 5;
                                for($i=1; $i <= $number_page; $i++) { ?>
                                    <li><a href="javascript:void(0);" class="page_number"><?php echo $i; ?></a></li>
                                <?php } ?>
                                <?php if ($number_page < $max): ?>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </nav>
                    <?php }
                    // wp_reset_query();
                    ?>

<!--                </div>-->
            <?php
            endif;
        endif;
    }

    function update( $new_instance, $old_instance ) {
        parent::update( $new_instance, $old_instance );

        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
        $instance['link'] = strip_tags($new_instance['link']);
        $instance['pos'] = strip_tags($new_instance['pos']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Tiêu đề widget',
            'number' => 'number',
            'pos' => 'pos',
            'link' => 'Link'
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $number = esc_attr( $instance['number'] );
        $pos = esc_attr( $instance['pos'] );
        $link = esc_attr( $instance['link'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"></p>
        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Số tin hiển thị:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php _e( 'Link:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'pos' ) ); ?>"><?php _e( 'Position:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'pos' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'pos' ) ); ?>" type="text" value="<?php echo esc_attr( $pos ); ?>"></p>
    <?php
    }
}
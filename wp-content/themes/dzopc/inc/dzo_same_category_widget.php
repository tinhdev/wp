<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 08/06/2015
 * Time: 11:06
 */

class Dzo_same_category_widget extends WP_Widget {
    public function dzo_same_category_widget() {
        $dzowidget_options = array(
            'classname' => 'dzo_same_category_widget_class', //ID của widget
            'description' => 'Hiển thị tin bài featured news'
        );
        $this->WP_Widget('dzo_same_category_widget_id', 'dzo_same_category_widget', $dzowidget_options);
    }

    public function widget( $args, $instance ) {
        $tag_slug = isset($_GET['tag']) ? $_GET['tag'] : '';
        $tag = $tag_slug ? get_term_by('slug', $tag_slug, 'post_tag') : '';

        $title = isset($instance['title']) ? $instance['title'] : "";
        $number = intval( isset($instance['number']) ? $instance['number'] : 10 );

        if (is_category()) {
            global $wp_query;
            $category = array($wp_query->get_queried_object());
        } else {
            $category = get_the_category();
        }

        $term_id = isset($category[0]) ? $category[0]->term_id : null;
        $link = get_home_url().'/tin-tuc/'.$category[0]->slug;
        $title = $category[0]->cat_name;
        if ($term_id == null)  return;

        $page = intval( isset($_GET['page']) ? $_GET['page'] : 1 );
        $offset = ($page - 1) * $number;

        $args = array(
            'numberposts' => $number,
            'offset' => $offset,
            'category' => $term_id,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            //'category__not_in' => '1',
            'post__not_in' => array(),
            'post_status' => 'publish',
            'suppress_filters' => true
        );

        if (!is_category()) {
            $args['post__not_in'] = array(get_the_ID());
        }

        if ($tag) {
            $args['tag_id'] = $tag->term_id;
        }
        //var_dump($args); die;
        $the_query = wp_get_recent_posts( $args );
        $link_tag_prefix = $link . "?tag=";
if (is_category()) {
        ?>
        <section class="highlight-1 highlight-2 highlight-food">

            <h1><a href="<?= $link; ?>" title=""><?= $title; ?></a></h1>

            <div class="inner-btn">
                <a href="<?= $link; ?>" class="btn ">Tất cả</a>
                <a href="<?= $link_tag_prefix?>bac" class="btn ">Bắc</a>
                <a href="<?= $link_tag_prefix?>trung" class="btn ">Trung</a>
                <a href="<?= $link_tag_prefix?>nam" class="btn ">Nam</a>
                <a href="<?= $link_tag_prefix?>tay-nguyen" class="btn ">Tây Nguyên</a>

                <div class="form-group">
                    <select id="tokens" class="selectpicker" multiple data-live-search="true" title="...">
                        <?php
                        $all_tags = get_category_tags(array('categories' => "{$term_id}"));
                        foreach ($all_tags as $tag)
                        { ?>
                            <option value="<?= $link_tag_prefix . $tag->tag_slug; ?>"><?= $tag->tag_name; ?></option>
                        <?php
                        } ?>
                    </select>
                </div>
            </div>
    <?php if ($the_query) { ?>
            <section class="highlight-main highlight-main-1">
            <?php
            $count = 1;
            foreach ($the_query as $post) {
                $title_post = $post['post_title'];
                $link_post = get_permalink($post['ID']);

                $cat_list = get_the_category($post['ID']);
                $link_post = rewrite_link_post_vedette($link_post, $cat_list, $category[0]->slug);

                if ($count <= 3) {
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id($post['ID']), 'large');
                ?>
                <article class="art-main">
                    <a class="js-view-post" href="<?= $link_post; ?>" data-id="<?= $post['ID']; ?>" title="<?php echo $title_post; ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?php echo $url[0]; ?>) no-repeat top left; "/>
                    </a>
                    <h2>
                        <a class="js-view-post" href="<?= $link_post; ?>" data-id="<?= $post['ID']; ?>"
                           title="<?php echo $title_post; ?>"><?= stringTrunc($title_post, 15); ?></a>
                    </h2>
                </article>
                <?php
                } else {
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id($post['ID']), 'medium');
                    ?>
                <article class="art-1 art-1-style col-sm-2">
                    <a class="js-view-post" href="<?= $link_post; ?>" data-id="<?= $post['ID']; ?>"
                       title="<?php echo $title_post; ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/trans.png" style="background: url(<?php echo $url[0]; ?>) no-repeat top left; "/>
                    </a>

                    <h3>
                        <a class="js-view-post" href="<?= $link_post; ?>" data-id="<?= $post['ID']; ?>"
                           title="<?php echo $title_post; ?>"><?= stringTrunc($title_post, 10); ?></a>
                    </h3>
                </article>
                <?php
                }
                if ($count == 3) {
            ?>
            </section>
            <div class="row">
            <?php
                }
                $count++;
            }

            if ($count > $number) {
            ?>
                <a class="btn-view js-load-more"
                   href="<?= $link; ?>"
                   data-numposts="<?= $number; ?>"
                   data-category="<?= $term_id; ?>"
                   data-page="<?= $page+1; ?>"
                   title="<?= $title; ?>"
                   data-tag="<?= $tag_slug; ?>">Xem thêm</a>
            <?php } elseif ($count <= 3) { ?>
            </section>
            <div class="row">
            <?php } ?>

            </div>
    <?php } ?>
        </section>
<?php } else { ?>
        <section class="highlight-1 highlight-qt highlight-food">
            <h2><a href="javascript:void(0)" title="">Bạn quan tâm</a></h2>
            <div class="row">
                <?php
                $counter = 1;
                foreach ($the_query as $post) {
                    $title_post = $post['post_title'];
                    $link = get_permalink($post['ID']);
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id($post['ID']), 'medium');
                    //$format_post = get_post_format(get_the_ID());
                    ?>

                    <article class="art-1 col-sm-4 col-xs-6">
                        <a href="<?php echo $link; ?>" title="">
                            <img src="<?= get_template_directory_uri(); ?>/images/trans.png"
                                 style="background: url(<?= $url[0]; ?>) no-repeat top left;"/>
                            <?php //echo echoSpan($format_post); ?>
                        </a>

                        <h3><a href="<?php echo $link; ?>"
                               title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a>
                        </h3>
                    </article>
                    <?php
                    if ($counter == $number) {
                        break;
                    } else {
                        $counter++;
                    }
                } ?>
            </div>
        </section>
<?php
}
        wp_reset_query();
    }

    function update( $new_instance, $old_instance ) {
        parent::update( $new_instance, $old_instance );

        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
        $instance['link'] = strip_tags($new_instance['link']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Tiêu đề widget',
            'number' => 'number',
            'link' => null
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $number = esc_attr( $instance['number'] );
        $link = esc_attr( $instance['link'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"></p>
        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Số tin hiển thị:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php _e( 'Link:', 'dzo' ); ?></label>
         <input id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>"></p>
    <?php
    }
}
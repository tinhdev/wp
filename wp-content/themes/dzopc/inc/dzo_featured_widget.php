<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 08/06/2015
 * Time: 11:06
 */

class Dzo_featured_widget extends WP_Widget {
    public function dzo_featured_widget() {
        $dzowidget_options = array(
            'classname' => 'dzo_featured_widget_class', //ID của widget
            'description' => 'Hiển thị tin bài featured news'
        );
        $this->WP_Widget('dzo_featured_widget_id', 'dzo_featured_widget', $dzowidget_options);
    }

    public function widget( $args, $instance ) {
        //$title = isset($instance['title']) ? $instance['title'] : "";
        $page = isset($instance['page']) ? $instance['page'] : "home";
        if ($page == 'category_old') {
            if (!isset($_GET['r'])) return;
            else $slug = explode('/', $_GET['r']);
            $size_slug = sizeof($slug);
            $cat_slug = isset($slug[$size_slug-1]) ? $slug[$size_slug-1] : '';
            $cat = get_category_by_slug ($cat_slug);
            $postlist = getPostByCatAndStick($cat_slug, 3, false);
            if ( sizeof($postlist->posts) >= 3) :
                ?>
                <div class="highlight-top row">
                    <?php
                    $count = 1;
                    while ($postlist->have_posts()) : $postlist->the_post();
                        $title_post = get_the_title();
                        if ($count == 1):
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
                            ?>
                            <div class="highlight-top row">
                            <article class="art-feature cate-inside">
                                <a href="<?php echo the_permalink(); ?>" title="">
                                    <img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                </a>
                                <div class="detail">
                                    <a class="type" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $cat->name; ?></a>
                                    <h2><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a>
                                        <span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span>
                                    </h2>
                                </div>
                            </article>
                        <?php
                        endif;
                        if ($count != 1):
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                            ?>
                            <article class="art-top-2 cate-inside">
                                <a href="<?php echo the_permalink(); ?>" title=""><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                                <div class="detail">
                                    <a class="type" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $cat->name; ?></a>
                                    <h2><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a>
                                        <span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span>
                                    </h2>
                                </div>
                            </article>
                        <?php
                        endif;
                        $count++;
                    endwhile;
                    wp_reset_postdata();
                ?>
                </div>
            <?php
            endif;
        }
        if ($page == "home") {
            $cat = isset($instance['category']) ? $instance['category'] : "";
            $postlist = getPostByCatAndStick($cat, 5, false);
            $count = 1;//var_dump($postlist); die;
            if (sizeof($postlist->posts) >= 5) {
                ?>
                <section class="highlight-main">
                <?php
                while ($postlist->have_posts()) {
                    $postlist->the_post();

                    if ($count == 1)
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
                    else
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');

                    $title_post = get_the_title();

                    $post_link = get_permalink(get_the_ID());
                    $cat_list = get_the_category(get_the_ID());
                    $post_link = rewrite_link_post_vedette($post_link, $cat_list);

                    $view_post = !get_the_content() ? '' : (' class="js-view-post" data-id="'.get_the_ID().'"');
                ?>
                    <article class="art-main">
                        <a<?=$view_post;?> href="<?php echo $post_link; ?>" title="<?php echo $title_post; ?>">
                            <img style="background: url(<?= $url[0]; ?>) no-repeat top left;" src="<?= get_template_directory_uri(); ?>/images/trans.png"/>
                        </a>

                        <h2>
                            <a<?=$view_post;?> href="<?php echo $post_link; ?>" title="<?php echo $title_post; ?>">
                                <?php echo $title_post; ?>
                            </a>
                        </h2>
                    </article>

                <?php
                }
                ?>
                </section>
                <?php
                wp_reset_postdata();
            }
        }
    }

    function update( $new_instance, $old_instance ) {
        parent::update( $new_instance, $old_instance );

        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['page'] = strip_tags($new_instance['page']);
        $instance['category'] = strip_tags($new_instance['category']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Tiêu đề widget',
            'page' => 'home',
            'category' => 'Nổi bật home'
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $page = esc_attr( $instance['page'] );
        $category = esc_attr( $instance['category'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"></p>
        <p><label for="<?php echo esc_attr( $this->get_field_id( 'page' ) ); ?>"><?php _e( 'Page:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'page' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'page' ) ); ?>" type="text" value="<?php echo esc_attr( $page ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php _e( 'Category:', 'dzo' ); ?></label>
         <input id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>" type="text" value="<?php echo esc_attr( $category ); ?>"></p>
    <?php
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 10/06/2015
 * Time: 14:58
 */

class dzo_most_comment_widget extends WP_Widget {
    public function dzo_most_comment_widget() {
        $dzo_most_comment_widget_options = array(
            'classname' => 'dzo_most_comment_widget_class', //ID của widget
            'description' => 'Hiển thị tin bài nhiều comment nhất'
        );
        $this->WP_Widget('dzo_most_comment_widget_id', 'dzo_most_comment_widget', $dzo_most_comment_widget_options);
    }

    public function widget( $args, $instance ) {
        $title = isset($instance['title']) ? $instance['title'] : "";
       // $color = isset($instance['color']) ? $instance['color'] : "red";
        $date = isset($instance['date']) ? $instance['date'] : "";
        $number = isset($instance['number']) ? $instance['number'] : 5;
        $date_filter_string = '-'.$date.' days';
        //add_filter( 'posts_where', 'filter_where' );
        $the_query = new WP_Query(array('post_date' => date('Y-m-d', strtotime($date_filter_string)), 'posts_per_page' => $number, 'order by' => 'comment_count', 'order'=> 'DESC' ));
        //remove_filter( 'posts_where', 'filter_where' );
        ?>
        <div class="block-bar block-mostview">
            <h3><a href="#" title=""><?php echo $title; ?></a></h3>
            <ul>
        <?php
        $count = 1;
        while ( $the_query->have_posts() && $count < $number) : $the_query->the_post();
            $title_post = get_the_title();

            if ($count == 1):
                $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
            ?>
                <li>
                    <a class="thumb resize resize-1" href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                    <h4><a href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?></span></h4>
                </li>
            <?php else: ?>
                <li>
                    <h4><a href="<?php echo get_permalink(); ?>" title="<?php echo $title_post; ?>">​<?php echo $title_post; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?></span></h4>
                </li>
            <?php endif; ?>
        <?php
        $count++;
        endwhile; ?>
            </ul>
            </div>
        <?php wp_reset_postdata();
    }

    function update( $new_instance, $old_instance) {
        parent::update( $new_instance, $old_instance );
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        //$instance['color'] = strip_tags($new_instance['color']);
        $instance['date'] = strip_tags($new_instance['date']);
        $instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Title',
           // 'color' => 'Color',
            'date' => 'Number last day',
            'number' => 'Number'
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        //$color = esc_attr( $instance['color'] );
        $date = esc_attr( $instance['date'] );
        $number = esc_attr( $instance['number'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'date' ) ); ?>"><?php _e( 'Number last day:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'date' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'date' ) ); ?>" type="text" value="<?php echo esc_attr( $date ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>

    <?php
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 08/06/2015
 * Time: 13:06
 */

class Dzo_webpart_widget extends WP_Widget {
    public function dzo_webpart_widget() {
        $dzo_webpart_widget_options = array(
            'classname' => 'dzo_webpart_widget_class', //ID của widget
            'description' => 'Hiển thị tin bài webpart trang chủ'
        );
        $this->WP_Widget('dzo_webpart_widget_id', 'dzo_webpart_widget', $dzo_webpart_widget_options);
    }
    public function widget( $args, $instance ) {
        $title = isset($instance['title']) ? $instance['title'] : "";
        $color = isset($instance['color']) ? $instance['color'] : "red_article";
        $cat = isset($instance['category']) ? $instance['category'] : "";
        $number = isset($instance['number']) ? $instance['number'] : 6;
        $cate_link = dzo_makeUrl($cat);

        if ($color =="red_article") {
            $postlist = getPostByCatAndStick($cat, 4, true);
            $count = 1;
            if (sizeof($postlist->posts) > 0) {?>
                <div class="highlight-1 highlight-inside row">
                    <h2 class="col-sm-12">
                        <a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a>
                        <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Xem tất cả</a>
                    </h2>
                    <?php
                    while ($postlist -> have_posts()) : $postlist -> the_post();
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        $excerpt = stringTrunc(get_the_excerpt(), 30);
                        $title_post = get_the_title();

                        ?>
                        <?php if ($count == 1): ?>
                            <div class=" col-sm-4">
                                <article class="art-3">
                                    <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                                    <div class="info">
                                        <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                        <ul class="tool-bar list-inline">
                                            <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                            <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                        </ul>
                                    </div>
                                </article>
                        <?php endif; ?>
                        <?php if ($count == 2): ?>
                                <article class="art-3">
                                    <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                                    <div class="info">
                                        <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                        <ul class="tool-bar list-inline">
                                            <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                            <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                        </ul>
                                    </div>
                                </article>
                            </div>
                        <?php endif; ?>
                        <?php if ($count == 3): ?>
                            <article class="art-3 col-sm-4">
                                <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                                <div class="info">
                                    <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                    <ul class="tool-bar list-inline">
                                        <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                        <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                    </ul>
                                    <p><?php echo $excerpt; ?></p>
                                </div>
                            </article>

                        <?php endif; ?>
                        <?php if ($count == 4): ?>
                            <article class="art-3 art-3-style col-sm-4">
                                <div class="info">
                                    <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                    <ul class="tool-bar list-inline">
                                        <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                        <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                    </ul>
                                    <p><?php echo $excerpt; ?></p>
                                </div>
                                <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                            </article>
                        <?php endif; ?>
                        <?php $count++; ?>
                    <?php
                    endwhile;
                    ?>
                    <div class="block-qc-1 col-sm-8">
                        <img height="150" width="640" alt="" src="<?php echo get_template_directory_uri(); ?>/images/qc.jpg">
                    </div>
                </div>
            <?php
            }
        }
        if ($color == 'purple_article') {
            $posts = getPostByCatAndStick($cat, $number, true);
            if (sizeof($posts->posts) > 0) {
                $count = 1;
                ?>
                <div class="highlight-1 highlight-outside row">
                    <h2 class="col-sm-12"><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Xem tất cả</a></h2>
                <?php
                while ($posts -> have_posts()) : $posts -> the_post();
                    $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                    $title_post = get_the_title();
                    $excerpt = stringTrunc(get_the_excerpt(), 35);
                    if($count == 1) { ?>
                        <article class="art-3 art-3-style col-sm-4">
                            <div class="info">
                                <h3><a href="<?php echo the_permalink(); ?>" title=""><?php echo $title_post; ?></a></h3>
                                <ul class="tool-bar list-inline">
                                    <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                    <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                </ul>
                                <p><?php echo $excerpt; ?></p>
                            </div>
                            <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                        </article>
                    <?php
                    }
                    if($count == 2) {
                       // $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        ?>
                        <article class="art-3 col-sm-4">
                            <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title=""><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                            <div class="info">
                                <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                <ul class="tool-bar list-inline">
                                    <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                    <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                </ul>
                                <p><?php echo $excerpt; ?></p>
                            </div>
                        </article>
                    <?php
                    }

                    if($count == 3) {
                        //$url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        ?>
                        <article class="art-3 art-3-style col-sm-4">
                            <div class="info">
                                <h3><a href="<?php echo the_permalink(); ?>" title=""><?php echo $title_post; ?></a></h3>
                                <ul class="tool-bar list-inline">
                                    <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li>
                                    <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                </ul>
                                <p><?php echo $excerpt; ?></p>
                            </div>
                            <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                        </article>
                    <?php
                    }

                    $count++;
                endwhile;
                ?>
                </div>
                <?php
            }
        }

        if ($color =="yellow_image") {
            $posts = getPostByCatAndStick($cat, $number, true);
            if (sizeof($posts->posts) > 0) {?>
                <div class="highlight-1 highlight-network row">
                    <h2 class="col-sm-12"><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Xem tất cả</a></h2>
                    <?php
                    while ($posts -> have_posts()) : $posts -> the_post();
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        $title_post = get_the_title();
                        ?>
                        <article class="col-sm-4">
                            <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                            <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                        </article>
                    <?php endwhile;
                    ?>
                </div>
            <?php
            }
        }

        if ($color =="red_image") {
            if ($number%3 != 0) $limit = $number - ($number%3);
            else $limit = $number;
            $postlist = getPostByCatAndStick($cat, $limit, true);
            $count = 1;
            if (sizeof($postlist->posts) > 0) {?>
                <div class="highlight-1 highlight-hot row">
                    <h2 class="col-sm-12"><a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
                    <div class="slider-1 col-sm-12">
                    <?php
                    while ($postlist -> have_posts()) : $postlist -> the_post();
                        $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                        $excerpt = stringTrunc(get_the_excerpt(), 12);
                        $title_post = get_the_title();
                        ?>
                        <?php if ($count == 1): ?>
                            <div>
                                <div class="inner-slider">
                                    <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                    <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>">
                                        <img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    </a>
                                    <p><?php echo $excerpt; ?></p>
                                    <a class="link-more" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>">Xem ngay</a>
                                </div>
                        <?php endif; ?>
                        <?php if ($count == 2): ?>
                                <div class="inner-slider">
                                    <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                    <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>">
                                        <img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    </a>
                                    <p><?php echo $excerpt; ?></p>
                                    <a class="link-more" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>">Xem ngay</a>
                                </div>
                        <?php endif; ?>
                        <?php if ($count == 3): ?>
                                <div class="inner-slider">
                                    <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                    <a class="resize resize-1" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>">
                                        <img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                    </a>
                                    <p><?php echo $excerpt; ?></p>
                                    <a class="link-more" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>">Xem ngay</a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($count == 3) {
                            $count = 1;
                        } else {
                            $count++;
                        }
                        ?>
                    <?php
                        endwhile;
                    ?>
                    </div>
                </div>
            <?php
            }
        }

        if ($color =="blue_image") {
            if ($number%5 != 0) $limit = $number - ($number%5);
            else $limit = $number;
            $postlist = getPostByCatAndStick($cat, $limit, true);
            $count = 1;
            if (sizeof($postlist->posts) > 0) {?>
                <div class="highlight-1 highlight-fun row">
                    <h2 class="col-sm-12">
                        <a href="<?php echo $cate_link; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a>
                        <a class="link-more" href="<?php echo $cate_link; ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Xem tất cả</a>
                    </h2>
                    <div class="slider-2 col-sm-12">
                        <?php
                        while ($postlist -> have_posts()) : $postlist -> the_post();
                            $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium' );
                            //$excerpt = stringTrunc(get_the_excerpt(), 15);
                            $title_post = get_the_title();
                            ?>
                            <?php if ($count == 1): ?>
                                <div>
                                    <div class="inner-slider">
                                        <article>
                                            <a class="resize resize-2" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                                            <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                        </article>
                                <?php $count++; ?>
                            <?php else: ?>
                                <article>
                                    <a class="resize resize-3" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="<?php echo $title_post; ?>" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png"></a>
                                    <h3><a href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><?php echo $title_post; ?></a></h3>
                                </article>
                                <?php if ($count == 5): ?>
                                    </div>
                                    </div>
                                    <?php $count = 1; ?>
                                <?php else:
                                    $count++;
                                    endif;
                                ?>
                            <?php endif; ?>
                        <?php
                        endwhile;
                        ?>
                    </div>
                </div>
            <?php
            }
        }
    }

    function update( $new_instance, $old_instance) {
        parent::update( $new_instance, $old_instance );
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['color'] = strip_tags($new_instance['color']);
        $instance['category'] = strip_tags($new_instance['category']);
        $instance['number'] = strip_tags($new_instance['number']);
        return $instance;
    }
    function form( $instance ) {
        parent::form( $instance );
        $default = array(
            'title' => 'Title',
            'color' => 'Color',
            'category' => 'Category',
            'number' => 'Number'
        );
        $instance = wp_parse_args( (array) $instance, $default);
        $title = esc_attr( $instance['title'] );
        $color = esc_attr( $instance['color'] );
        $category = esc_attr( $instance['category'] );
        $number = esc_attr( $instance['number'] );
        ?>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>"><?php _e( 'Color:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'color' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'color' ) ); ?>" type="text" value="<?php echo esc_attr( $color); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php _e( 'Category:', 'dzo' ); ?></label>
        <input id="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'category' ) ); ?>" type="text" value="<?php echo esc_attr( $category ); ?>"></p>

        <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php _e( 'Number:', 'dzo' ); ?></label>
            <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>"></p>

    <?php
    }
}
<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage 123share
 * @since 123share 1.0
 */
get_header();
?>
    <div class="container-fluid main">
        <div class="banner-top"><img alt="" src="<?php bloginfo('template_directory'); ?>/images/banner-5.jpg"></div>
        <div class="row">
            <section class="content col-md-8 col-sm-7">

                <?php get_template_part('partial-templates/featured_news');?>
                <?php get_template_part('partial-templates/webpart');?>
            </section>
            <aside class="sidebar col-md-4 col-sm-5">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>

<?php get_footer(); ?>
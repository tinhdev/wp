<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<!DOCTYPE HTML>
<!--[if IE 9]><html lang="en" class="ie9"></html><![endif]-->
<!-- [if gt IE 9] <!-->
<html lang="en">
<!-- <![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Home | News </title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
    <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/slick.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/screen.css" rel="stylesheet">
</head>
<body>
<header class="header">
    <div class="container-fluid">
        <nav class="nav-main">
            <ul class="">
                <li><a class="btn-action" id="btn-menu" href="javascript:void(0)" title=""><i class="fa fa-th-list"></i></a></li>
                <li class="logo"><a href="#" title=""><img alt="" src="<?php bloginfo('template_directory'); ?>/images/logo.png"></a></li>
                <li class="search"><a class="btn-searchsg" href="javascript:void(0)" title=""><i class="fa fa-search"></i></a>
                    <form class="frm-search">
                        <div class="form-group">
                            <input type="text" name="" id="" class="form-control" placeholder="Tìm kiếm...">
                        </div>
                    </form>
                </li>
                <li class="btn-send-mail"><a href="#" title=""><i class="fa fa-envelope-o"></i></a></li>
            </ul>
        </nav>
    </div>
</header>
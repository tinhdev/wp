<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 31/05/2015
 * Time: 09:43
 */
function detect_shortcode_featured_news()
{
    global $post;
    //$pattern = get_shortcode_regex();
    //preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches);
    $pattern = "/\[featured(.*?)\]/";
//    if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
//        && array_key_exists( 2, $matches )
//        && in_array( 'featured', $matches[2] ) )
//    {
    if (preg_match_all($pattern, $post->post_content, $matches))
    {
        $attrs = parse_atts($matches[0][0]);
        if($attrs['page']='home') {
            $tin_noi_bat_1 = get_posts($attrs['categorytop']);
            $tin_noi_bat_2 = get_posts($attrs['categorybottom']);

            $layout = '<div class="highlight highlight-top">';
            //Build tin bai noi bat 1
            $layout .= '<h3 class="title"><a href="javascript:void(0)">'.$attrs['titletop'].'</a></h3>';
            $layout .= '
                <article class="art-new art-feature">
                    <div class="inner-art">
                        <a href="'.get_post_permalink($tin_noi_bat_1[0]->ID).'" title="">'.get_the_post_thumbnail($tin_noi_bat_1[0]->ID, 'featured_home_top_img').'</a>
                        <a href="'.get_post_permalink($tin_noi_bat_1[0]->ID).'" title="">'.$tin_noi_bat_1[0]->post_title.'</a>
                        <span class="txt-absolute txt-video">Video</span>
                    </div>
                    <h2><div><a href="'.get_post_permalink($tin_noi_bat_1[0]->ID).'" title="">'.$tin_noi_bat_1[0]->post_title.'</a><span class="txt-cm"><i class="fa fa-comments"></i> '.$tin_noi_bat_1[0]->comment_count.'</span></div></h2>
                </article>';
            //Finished tin bai noi bat 1
            $count = 1;

            foreach($tin_noi_bat_2 as $item) {
                if ($count == 1) $layout .= '<table class="tbl-1">';
                if ($count % 2 == 1) {
                    $layout .= '
                            <tr>
                                <td>
                                    <article class="art-new">
                                        <div class="inner-art">
                                            <a href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'featured_home_bottom_img').'</a>
                                        </div>
                                        <h2><div><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a><span class="txt-cm"><i class="fa fa-comments"></i> '.$item->comment_count.'</span></div></h2>
                                    </article>
                                </td>';
                } else {
                    $layout .= '
                                <td>
                                    <article class="art-new">
                                        <div class="inner-art">
                                            <a href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'featured_home_bottom_img').'</a>
                                        </div>
                                        <h2><div><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a><span class="txt-cm"><i class="fa fa-comments"></i> '.$item->comment_count.'</span></div></h2>
                                    </article>
                                </td>
                            </tr>
                ';
                }
                if ($count == 4) {
                    $layout .= '</table>';
                    break;
                }
                $count++;

            }
            $layout .= '</div>';
            echo $layout;
        }
        return;

    }
}
add_action( 'featured_news_shortcode', 'detect_shortcode_featured_news' );

do_action('featured_news_shortcode');
?>



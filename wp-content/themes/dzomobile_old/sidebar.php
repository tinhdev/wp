<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 31/05/2015
 * Time: 09:31
 */
?>
<div class="block-bar block-mostview">
    <h3><a href="#" title="">Xem nhiều nhất</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
    <ul>
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-6.jpg"> <span class="number">1</span></a>
            <h4><a href="" title="">Nông dân thuê... hợp tác xã làm ruộng</a> <span class="txt-cm"><i class="fa fa-comments"></i> 11</span></h4>
        </li>
        <li>
            <span class="number">2</span>
            <h4><a href="" title="">​Hải quân Việt Nam kỷ niệm 60 năm ngày thành lập</a> <span class="txt-cm"><i class="fa fa-comments"></i> 11</span></h4>
        </li>
        <li>
            <span class="number">3</span>
            <h4><a href="" title="">Tuyển ứng viên đi học nước ngoài theo đề án Chính phủ</a></h4>
        </li>
        <li>
            <span class="number">4</span>
            <h4><a href="" title="">Giá sầu riêng đầu mùa giảm mạnh</a></h4>
        </li>
        <li>
            <span class="number">5</span>
            <h4><a href="" title="">Cầu mong đất nước luôn yên bình</a></h4>
        </li>
    </ul>
</div>
<div class="block-bar">
    <h3><a href="#" title="">Mới nhất</a></h3>
    <ul class="list-new">
        <li>
            <a href="" title="">Các cặp đôi xì-tai trên phố</a>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a href="" title="">​HLV Takashi tự tin trước Myanmar</a>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện trong nhà</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a href="" title="">Indonesia đề xuất luật cấm rượu bia</a>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Cười chút chơi</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>

        <li>
            <a href="" title="">Indonesia đề xuất luật cấm rượu bia</a>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Cười chút chơi</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a href="" title="">Trung Dũng đóng cảnh nóng với hai diễn viên nữ</a>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Cười chút chơi</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
    </ul>
</div>
<div class="block-bar">
    <h3><a href="#" title="">Share nhiều nhất</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
    <ul class="list-new-1">
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-6.jpg"></a>
            <h4><a href="" title="">Nông dân thuê... hợp tác xã làm ruộng</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <h4><a href="" title="">​Hải quân Việt Nam kỷ niệm 60 năm ngày thành lập</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><span class="txt-video">Video</span><img alt="" src="images/photo/img-7.jpg"></a>
            <h4><a href="" title="">Tuyển ứng viên đi học nước ngoài theo đề án Chính phủ</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><span class="txt-photo">photo</span><img alt="" src="images/photo/img-8.jpg"></a>
            <h4><a href="" title="">Giá sầu riêng đầu mùa giảm mạnh</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-9.jpg"></a>
            <h4><a href="" title="">Cầu mong đất nước luôn yên bình</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
    </ul>
</div>
<div class="block-bar">
    <img width="100%" alt="" src="images/qc.jpg">
</div>
<div class="block-bar">
    <h3><a href="#" title="">Bình luận nhiều nhất</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
    <ul class="list-new-1">
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-6.jpg"></a>
            <h4><a href="" title="">Nông dân thuê... hợp tác xã làm ruộng</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-14.jpg"></a>
            <h4><a href="" title="">​Hải quân Việt Nam kỷ niệm 60 năm ngày thành lập</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><span class="txt-video">Video</span><img alt="" src="images/photo/img-7.jpg"></a>
            <h4><a href="" title="">Tuyển ứng viên đi học nước ngoài theo đề án Chính phủ</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <h4><a href="" title="">Giá sầu riêng đầu mùa giảm mạnh</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-9.jpg"></a>
            <h4><a href="" title="">Cầu mong đất nước luôn yên bình</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
    </ul>
</div>
<div class="block-bar">
    <img width="100%" alt="" src="images/fb-sample.jpg">
</div>

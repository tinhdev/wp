	<?php 
	
	/**
	 *	
	 *	Template Name: Contact page
	 * @package  magExpress
	 * @file     contact.php
	 * @author   WpFreeware Team
	 * @link 	 http://wpfreeware.com
	 */			
	
	get_header();?>
	  
	  
      <!-- start site main content -->
      <section id="ContactContent">        
       <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <!-- start contact area -->
           <div class="contact_area">
             <h1><?php the_title();?></h1>
			 
			<?php if (have_posts()) : ?><?php while(have_posts())  : the_post(); ?>

				<?php the_content();?>
				
			<?php endwhile; ?>
			
			<?php else : ?>
					<h3 style="text-align:center;margin:100px auto;font-weight:bold;font-size:30px;line-height:35px;"><?php _e('Sorry! Nothing Found', 'wpf'); ?></h3>
			<?php endif; ?>		
					
             <div class="contact_bottom">
               <div class="contact_us wow fadeInRightBig">
               <h2>Leave a Message</h2>
				<?php echo do_shortcode('[contact-form-7 id="88" title="Contact form 1"]'); ?>
               </div>
             </div>
           </div>
           <!-- End contact area -->
         </div>
       </div>
               
      </section><!-- End site main content -->
	  
    </div> <!-- /.container -->
	
	<?php get_footer();?>
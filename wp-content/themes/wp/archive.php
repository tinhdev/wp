	<?php 
	
	/**
	 *
	 * @package  magExpress
	 * @file     archive.php
	 * @author   WpFreeware Team
	 * @link 	 http://wpfreeware.com
	 */			
	
	get_header();?>
	  
	  
      <!-- start site main content -->
      <section id="mainContent">
	  
		  
          <!-- start main content bottom -->
          <div class="content_bottom">
				
			<!--Start Main content -->  
			
            <div class="col-lg-8 col-md-8">
            
              <div class="content_bottom_left">		
				
				<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
				
                <!-- start business category -->
                <div class="single_category wow fadeInDown">
			  
				<div class="archive_style_1">	

				<h2>                  
					<span class="bold_line"><span></span></span>
					<span class="solid_line"></span>
					<span class="title_text">
					
						<?php if (have_posts()) : ?>
								<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
									<?php /* If this is a category archive */ if (is_category()) { ?>
									<?php echo single_cat_title(); ?> <?php _e('Archive', 'magExpress'); ?>
									<?php /* If this is a tag archive */  } elseif( is_tag() ) { ?>
									<?php single_tag_title(); ?> <?php _e('Archive', 'magExpress'); ?>
									<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
									<?php the_time('F jS, Y'); ?> <?php _e('Archive', 'magExpress'); ?>						
									<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
									<?php the_time('F, Y'); ?> <?php _e('Archive', 'magExpress'); ?>					
									<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
									<?php the_time('Y'); ?> <?php _e('Archive', 'magExpress'); ?>
									<?php /* If this is a search */ } elseif (is_search()) { ?>
										<?php _e('Search Results', 'magExpress'); ?>
									<?php /* If this is an author archive */ } elseif (is_author()) { ?>
										<?php _e('Author Archive', 'magExpress'); ?>
									<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
										<?php _e('Blog Archives', 'magExpress'); ?>
							<?php } ?>	
					</span>
				</h2>	
				
					<?php while(have_posts())  : the_post(); ?>

						<?php get_template_part('includes/templates/excerpt-template/post-excerpt');?>
						
					<?php endwhile; ?>
					
					<?php else : ?>
							<h3 style="text-align:center;margin:100px auto;font-weight:bold;font-size:30px;line-height:35px;"><?php _e('Sorry! Nothing Found', 'wpf'); ?></h3>
					<?php endif; ?>					
				  
				  
				</div>

  
                </div>
                <!-- End business category -->
			  

				
              </div>  
			  
			  <!--Start pagination-->
			  
			  <div class="pagination_area">
				<?php echo blog_pagination();?>
			  </div>

				<!--End pagination-->			  
			  
            </div>
			
			<!--End Main content -->  
			
            <!-- start Sidebar -->
			<?php get_sidebar();?>
			<!-- End Sidebar -->
			
		</div><!-- end main content bottom -->        
      </section><!-- End site main content -->
    </div> <!-- /.container -->
	
	<?php get_footer();?>
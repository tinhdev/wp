<?php 

///////////////////
// Load enqueue
///////////////////

include_once( 'includes/functions/enqueue.php' );

///////////////////////
// shortcodes
//////////////////////

include_once( 'includes/functions/shortcodes.php' );

// breadcrumbs
require_once('includes/functions/breadcrumbs.php');

////////////////////////////////////////////
// TGM activation
///////////////////////////////////////////

require_once('includes/require-plugin/class-tgm-plugin-activation.php');
require_once('includes/require-plugin/example.php');

///////////////////////////
// top menu support
///////////////////////////

	add_action('init', 'wpf_register_menu');
	function wpf_register_menu() {
		if (function_exists('register_nav_menu')) {
			register_nav_menu( 'wpf-main-menu', __( 'Main Menu', 'magExpress' ) ); // main menu
			register_nav_menu( 'wpf-top-menu', __( 'Top Menu', 'magExpress' ) ); // top menu
		}
	}
	
																			
	function wpf_default_menu() {																	
		echo '<div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav custom_nav">';
		if ('page' != get_option('show_on_front')) {
			echo '<li><a href="'. home_url() . '/">Home</a></li>';
		}
		wp_list_pages('title_li=');
		echo '</ul></div>';
	}

///////////////////////////////////////	
// Register Custom Navigation Walker
//////////////////////////////////////

require_once('includes/functions/wp_bootstrap_navwalker.php');

//////////////////////////
// featured image support
//////////////////////////
	
add_theme_support( 'post-thumbnails', array( 'post' ) );
add_image_size( 'top_slider_img', 550, 425, true ); // top slider img
add_image_size( 'top_featured_img', 300, 250, true ); // top featured img
add_image_size( 'bottom_featured_left_img', 292, 150, true ); // bottom featured left img
add_image_size( 'bottom_slider_img', 567, 330, true ); // bottom sldier img
add_image_size( 'small_thmub_img', 112, 112, true ); // small thumb img
add_image_size( 'home_content_featured_img', 390, 240, true ); // home content feature img
add_image_size( 'archive_style_3_thmub', 740, 300, true ); // archive style 3 thmubnail img

//////////////////////////////////////////////
// Allow shortcodes in widgets in sidebars 
///////////////////////////////////////////
	add_filter( 'comment_text'  , 'do_shortcode' );
	add_filter( 'widget_text'   , 'do_shortcode' );
	add_filter( 'the_excerpt'   , 'do_shortcode' );

/////////////////////	
// sidebar register
/////////////////////

function wpf_magExpress_widgets() {

	register_sidebar( array(
		'name' => __( 'Sidebar', 'magExpress' ),
		'id' => 'main_sidebar',
		'description' => 'Display your widgets in the sidebar. This will appear on home page, archive pages, posts',
		'before_widget' => '<div class="single_bottom_rightbar">',
		'after_widget' => '</div>',
	    'before_title' => '<h2>',
	    'after_title' => '</h2>',
	) );
	

	register_sidebar( array(
		'name' => __( 'Featured Left Sidebar', 'magExpress' ),
		'id' => 'featured_left_sidebar',
		'description' => 'Display your widgets in the Featured Left sidebar on the home page template',
		'before_widget' => '<div class="single_category">',
		'after_widget' => '</div>',
	    'before_title' => '<h2>                  
                  <span class="bold_line"><span></span></span>
                  <span class="solid_line"></span>
                  <span class="title_text">',
	    'after_title' => '</span></h2>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Featured Right Sidebar', 'magExpress' ),
		'id' => 'featured_right_sidebar',
		'description' => 'Display your widgets in the Featured Right sidebar on the home page template',
		'before_widget' => '<div class="single_category">',
		'after_widget' => '</div>',
	    'before_title' => '<h2>                  
                  <span class="bold_line"><span></span></span>
                  <span class="solid_line"></span>
                  <span class="title_text">',
	    'after_title' => '</span></h2>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Widgets', 'magExpress' ),
		'id' => 'footer_sidebar',
		'description' => 'Display your widgets in the footer section.',
		'before_widget' => '<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="single_footer_top">',
		'after_widget' => '</div></div>',
	    'before_title' => '<h2>',
	    'after_title' => '</h2>',
	) );	
	
	
}
add_action('widgets_init', 'wpf_magExpress_widgets');


// Returns a youtube or vimeo embed code ----------------------------------------------------//

function magExpress_parse_url($path){
	$parsedUrl  = parse_url($path);
	
	if($parsedUrl['host'] == 'youtube.com' || $parsedUrl['host'] == 'www.youtube.com'){
		// for youtube  
	    $embed	= $parsedUrl['query'];  
	    parse_str($embed, $out);  
	    $embedUrl   = $out['v']; 
	    return  "http://www.youtube.com/embed/$embedUrl"; 
	}
	
	if($parsedUrl['host'] == 'vimeo.com' || $parsedUrl['host'] == 'www.vimeo.com'){
		// for vimeo
		$embed	= $parsedUrl['path'];
		return "http://player.vimeo.com/video$embed";
	}
}	


///////////////////////////////////
// post excerpt in shortcodes
///////////////////////////////////

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_trim_excerpt');

function custom_trim_excerpt($text = '')
{
	$raw_excerpt = $text;
	if ( '' == $text ) {
		$text = get_the_content('');
 
		//$text = strip_shortcodes( $text );
 
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]&gt;', ']]&gt;', $text);
		$excerpt_length = apply_filters('excerpt_length', 18); // word number that you want to show in excerpt
		$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}
add_filter('get_the_excerpt','do_shortcode');

//////////////////
// popular posts
//////////////////
require_once('includes/functions/popular_posts.php');


// theme colors
include_once( 'includes/functions/theme-colors.php' );


// pagination activation
require_once('includes/functions/pagination.php');



/////////////////////
//backend styles
/////////////////////

function custom_css_adminpanel() {
	echo '<link href="'.get_template_directory_uri() .'/css/magExpress-backend.css" rel="stylesheet" media="screen">';
//	echo '<link href="'.get_template_directory_uri() .'/css/more-themes.css" rel="stylesheet" media="screen">';
}
add_action('admin_head', 'custom_css_adminpanel');

/////////////////////
// google analytics
////////////////////
function magexpress_analytics() {
	require_once('includes/templates/google_analytics.php');
}
add_action('wp_footer', 'magexpress_analytics');

////////////////////////
// Redux Framework
////////////////////////

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/includes/ReduxFramework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/includes/ReduxFramework/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/includes/functions/theme-options.php' ) ) {
    require_once( dirname( __FILE__ ) . '/includes/functions/theme-options.php' );
}




?>
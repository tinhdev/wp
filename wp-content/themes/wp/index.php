	<?php 
	
	/**
	 *
	 * 
	 * This is the 2nd style post archive page template.
	 * You can select this page as your blog post archive page.
	 *
	 * @package  magExpress
	 * @file     index.php
	 * @author   WpFreeware Team
	 * @link 	 http://wpfreeware.com
	 */			
	
	get_header();?>
	  
	  
      <!-- start site main content -->
      <section id="mainContent">
	  
			<!-- start main content top -->
				<?php get_template_part('includes/templates/top_slider_area');?>
			<!-- End main content top -->
			
			<!-- start main content Middle -->
				<?php get_template_part('includes/templates/bottom_slider_area');?>
			<!-- End main content middle -->
		  
          <!-- start main content bottom -->
          <div class="content_bottom">
				
			<!--Start Main content -->  
			
            <div class="col-lg-8 col-md-8">
            
              <div class="content_bottom_left">			  
				
                <!-- start business category -->
                <div class="single_category wow fadeInDown">
			  
				<div class="archive_style_1">	

				  <h2>                  
					<span class="bold_line"><span></span></span>
					<span class="solid_line"></span>
					<span class="title_text">Latest Updates</span>
				  </h2>	
				
					<?php if(have_posts()) : ?><?php while(have_posts())  : the_post(); ?>


						<?php get_template_part('post-styles');?>
					
						
					<?php endwhile; ?>

					<?php else : ?>
						<h3 style="text-align:center;margin:100px auto;font-weight:bold;font-size:30px;line-height:35px;"><?php _e('Sorry! Nothing Found', 'magExpress'); ?></h3>
					<?php endif; ?>					
				  
				  
				</div>

  
                </div>
                <!-- End business category -->
			  

				
              </div>  
			  
			  <!--Start pagination-->
			  
			  <div class="pagination_area">
				<?php echo blog_pagination();?>
			  </div>

				<!--End pagination-->			  
			  
            </div>
			
			<!--End Main content -->  
			
            <!-- start Sidebar -->
			<?php get_sidebar();?>
			<!-- End Sidebar -->
			
		</div><!-- end main content bottom -->        
      </section><!-- End site main content -->
    </div> <!-- /.container -->
	
	<?php get_footer();?>
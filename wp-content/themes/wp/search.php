	<?php 
	
	/**
	 *
	 * @package  magExpress
	 * @file     search.php
	 * @author   WpFreeware Team
	 * @link 	 http://wpfreeware.com
	 */			
	
	get_header();?>
	  
	  
      <!-- start site main content -->
      <section id="mainContent">
	  
		  
          <!-- start main content bottom -->
          <div class="content_bottom">
				
			<!--Start Main content -->  
			
            <div class="col-lg-8 col-md-8">
            
              <div class="content_bottom_left">		
				
				<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
				
                <!-- start business category -->
                <div class="single_category wow fadeInDown">
			  
				<div class="archive_style_1">	

				<h2>                  
					<span class="bold_line"><span></span></span>
					<span class="solid_line"></span>
					<span class="title_text">
					
						<?php printf( __( 'Search for: %s', 'wpf' ),'' . get_search_query()); ?>
						
					</span>
				</h2>	
					<?php if(have_posts()) : ?><?php while(have_posts())  : the_post(); ?>

						<?php get_template_part('includes/templates/excerpt-template/post-excerpt');?>
						
					<?php endwhile; ?>
					
					<?php else : ?>
							<h3 style="text-align:center;margin:100px auto;font-weight:bold;font-size:30px;line-height:35px;"><?php _e('Sorry! Nothing Found', 'wpf'); ?></h3>
					<?php endif; ?>					
				  
				  
				</div>

  
                </div>
                <!-- End business category -->
			  

				
              </div>  
			  
			  <!--Start pagination-->
			  
			  <div class="pagination_area">
				<?php echo blog_pagination();?>
			  </div>

				<!--End pagination-->			  
			  
            </div>
			
			<!--End Main content -->  
			
            <!-- start Sidebar -->
			<?php get_sidebar();?>
			<!-- End Sidebar -->
			
		</div><!-- end main content bottom -->        
      </section><!-- End site main content -->
    </div> <!-- /.container -->
	
	<?php get_footer();?>
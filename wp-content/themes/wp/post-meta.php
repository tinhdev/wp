	<div class="post_commentbox">
		  <span><i class="fa fa-user"></i> <?php the_author(); ?></span>
		  <span><i class="fa fa-calendar"></i> <?php the_time('d M, Y') ?></span>
		  <span><?php if(function_exists("the_category")) ?> <i class="fa fa-tasks"></i> <?php the_category(', '); ?> </span>		  
		  <span><?php if(function_exists("the_tags")) the_tags('<i class="fa fa-tags"></i> ', ', ', ''); ?></span>
	</div>
    <footer id="footer">
	
      <div class="footer_top">
        <div class="container">
          <div class="row">
		  
			<?php dynamic_sidebar('footer_sidebar'); ?>
			
          </div>
        </div>
      </div>
	  
      <div class="footer_bottom">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="footer_bottom_left">
                
				
			   <?php global $magExpress;if(!empty($magExpress['footer_text'])) { ?>
					<p><?php echo $magExpress['footer_text'];?></p>
				<?php } else { ?>
						<p><?php _e('Footer text goes here', 'wpf'); ?></p>
				<?php } ?>
				
              </div>   
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="footer_bottom_right">
                <p>Developed BY <a href="http://wpfreeware.com/">Wpfreeware</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
  
    </footer>

  <!-- =========================
        //////////////This Theme Design and Developed //////////////////////
        //////////// by www.wpfreeware.com======================-->
    
  <?php wp_footer();?> 
  </body>
</html>
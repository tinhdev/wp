	<?php 
	
	/*************
	 * Template Name: Home Template
	 *
	 * @package  magExpress
	 * @file     home-page.php
	 * @author   WpFreeware Team
	 * @link 	 http://wpfreeware.com
	 * @license	 Creative Common v4.0 and later
	 * @license url: http://creativecommons.org/licenses/by/4.0/
	 ***************/		
	
	get_header();?>
	  
	  
      <!-- start site main content -->
      <section id="mainContent">
	  
			<!-- start main content top -->
				<?php get_template_part('includes/templates/top_slider_area');?>
			<!-- End main content top -->
			
			<!-- start main content Middle -->
				<?php get_template_part('includes/templates/bottom_slider_area');?>
			<!-- End main content middle -->
		  
          <!-- start main content bottom -->
          <div class="content_bottom">
				
			<!--Start Main content -->  
			<?php get_template_part('includes/templates/main_content');?>
			<!--End Main content -->  
			
            <!-- start Sidebar -->
			<?php get_sidebar();?>
			<!-- End Sidebar -->
			
		</div><!-- end main content bottom -->        
      </section><!-- End site main content -->
    </div> <!-- /.container -->
	
	<?php get_footer();?>
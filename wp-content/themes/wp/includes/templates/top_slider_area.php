        <div class="content_top">
          <div class="row">
             <!-- start content top latest slider -->
            <div class="col-lg-6 col-md-6 col-sm6">             
              <div class="latest_slider">
                 <!-- Set up your HTML -->
                <div class="slick_slider">
				
					<?php
					global $post;
					global $magExpress;
					$top_slider_cat_id = $magExpress['top_slider_category']; // category ids
					$top_slide_num = $magExpress['top_slider_slide_number']; // post numbers
					
					$args = array( 'posts_per_page' => ''.$top_slide_num.'', 'post_type'=> 'post','category__in' => array( $top_slider_cat_id ));
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) : setup_postdata($post); ?>
						
					  <div class="single_iteam">
						<?php the_post_thumbnail('top_slider_img', array('alt' => get_the_title())); ?>
						<h2><a class="slider_tittle" href="<?php the_permalink();?>"><?php the_title();?></a></h2>
					  </div>	
					  
						
					<?php endforeach; ?>		

                </div>
              </div>
            </div> <!-- End content top latest slider -->

            <div class="col-lg-6 col-md-6 col-sm6">
              <div class="content_top_right">
                <ul class="featured_nav wow fadeInDown">
				
					<?php
					global $post;
					global $magExpress;
					$featured_post_ids = $magExpress['top_featured_content']; // id's
					
					$args = array( 'posts_per_page' => '4', 'post_type'=> 'post','category__in' => array( $featured_post_ids ));
					$myposts = get_posts( $args );
					foreach( $myposts as $post ) : setup_postdata($post); ?>
					  
					  <li>
						<?php the_post_thumbnail('top_featured_img', array('alt' => get_the_title())); ?>
						<div class="title_caption">
						  <a href="<?php the_permalink();?>">
							<?php the_title();?>
						  </a>
						</div>
					  </li>					  
					  
						
					<?php endforeach; ?>	
				  
                </ul>
              </div>
            </div>
			
          </div>
        </div>
            <div class="col-lg-8 col-md-8">
            <!-- start content bottom left -->
              <div class="content_bottom_left">
			  
				<?php if(have_posts()) : ?><?php while(have_posts())  : the_post(); ?>

					<?php the_content();?>
					
				<?php endwhile; ?>

				<?php else : ?>
					<h3 style="text-align:center;margin:100px auto;font-weight:bold;font-size:30px;line-height:35px;"><?php _e('Sorry! Nothing Found', 'magExpress'); ?></h3>
				<?php endif; ?>	
			  

				
              </div> <!--End content_bottom_left-->  
            </div>
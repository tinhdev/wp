              <div class="post_pagination">
					<?php
					$previous_post = get_adjacent_post(false, '', true);
					$next_post = get_adjacent_post(false, '', false);
					?>
								  
									
					<?php if ($previous_post): // if there are older articles ?>
						<div class="prev wow fadeInLeftBig">				
						  <div class="pagincontent">
							<span>Previous Post</span>
							<?php previous_post_link('%link', '%title'); ?>
						  </div>
						</div>
					<?php endif; ?>				

									
					<?php if ($next_post): // if there are newer articles ?>
						<div class="next wow fadeInRightBig">                       
						  <div class="pagincontent">
							<span>Next Post</span>
							<?php next_post_link('%link', '%title'); ?>
						  </div>
						</div>    
					<?php endif; ?>				

              </div>
			  
			 		  
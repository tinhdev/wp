        <div class="content_middle">
		
		  <!-- featured sidebar left -->
          <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="content_middle_leftbar">
			
				<?php dynamic_sidebar('featured_left_sidebar'); ?>				
			  
            </div>
          </div>
		  
          <div class="col-lg-6 col-md-6 col-sm-6">
		  
			<!-- bottom slider -->
            <div class="content_middle_middle">
              <div class="slick_slider2">
			  
				<?php
				global $post;
				global $magExpress;
				$bottom_slider_cat_id = $magExpress['bottom_slider_category']; // category ids
				$bottom_slide_num = $magExpress['bottom_slider_slide_number']; // post numbers
				
				$args = array( 'posts_per_page' => ''.$bottom_slide_num.'', 'post_type'=> 'post','category__in' => array( $bottom_slider_cat_id ));
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) : setup_postdata($post); ?>

				<div class="single_featured_slide">
				  <a href="<?php the_permalink();?>"><?php the_post_thumbnail('bottom_slider_img', array('alt' => get_the_title())); ?></a>
				  <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
				  <p><?php the_excerpt(); ?></p>
				</div>				  
				  
					
				<?php endforeach; ?>
			  
				
              </div>
            </div>
          </div>
		  
		  <!-- featured sidebar right -->
          <div class="col-lg-3 col-md-3 col-sm-3">
            <div class="content_middle_rightbar">
			
				<?php dynamic_sidebar('featured_right_sidebar'); ?>	
				
              </div>
            </div>
          </div>
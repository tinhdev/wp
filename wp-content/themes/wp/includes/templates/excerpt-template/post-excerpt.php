	  <div class="business_category_left wow fadeInDown">
		<ul class="fashion_catgnav">
		  <li>
			<div class="catgimg2_container">
			  <a href="<?php the_permalink();?>"><?php the_post_thumbnail('home_content_featured_img', array('alt' => get_the_title())); ?></a>
			</div>
			<h2 class="catg_titile"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<div class="comments_box">
			  <span class="meta_date"><?php the_time('d M, Y') ?></span>
			  <span class="meta_comment"><?php if ( comments_open() ) : ?> <?php comments_popup_link('No Comment', '1 Comment', '% Comments'); ?><?php endif; ?></span>
			  <span class="meta_more"><a  href="<?php the_permalink();?>">Read More...</a></span>
			</div>
			<p><?php the_excerpt(); ?></p>
		  </li>
		  
		</ul>
	  </div> 
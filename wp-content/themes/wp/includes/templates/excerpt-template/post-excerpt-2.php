	<div class="business_category_right">
	  <ul class="small_catg wow fadeInDown">
		<li>
		  <div class="media wow fadeInDown">
			<a class="media-left" href="<?php the_permalink();?>">
			  <?php the_post_thumbnail('small_thmub_img', array('alt' => get_the_title())); ?>
			</a>
			<div class="media-body">
			  <h4 class="media-heading"><a href="<?php the_permalink();?>"><?php the_title();?></a></h4> 
			  <div class="comments_box">
				<span class="meta_date"><?php the_time('d M, Y') ?></span>
				<span class="meta_comment"><?php if ( comments_open() ) : ?> <?php comments_popup_link('No Comment', '1 Comment', '% Comments'); ?><?php endif; ?></span>
			  </div>
			</div>
		  </div>
		</li>
	  </ul>
	</div>	
	<!-- start single archive style 3 -->						
	 <div class="single_archive wow fadeInDown">
	   <div class="archive_imgcontainer">
		 <?php the_post_thumbnail('archive_style_3_thmub', array('alt' => get_the_title())); ?>
	   </div>
	   <div class="archive_caption">
		 <h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
		 <p><?php the_excerpt(); ?></p>
	   </div>
	   <a class="read_more" href="<?php the_permalink();?>"><span>Read More</span></a>
	 </div>
	 <!-- end single archive style 3 -->	
<?php

///////////////////////////////////////////////
// magExpress Developed by WpFreeware.com
//////////////////////////////////////////////

////////////////////////////////
// jquery scripts
/////////////////////////////////

if (!is_admin()) add_action("wp_enqueue_scripts", "magExpress_CybarMagazine_scripts", 11);
function magExpress_CybarMagazine_scripts() {

   wp_deregister_script('jquery');
   wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", false, '1.11.1', true);
   wp_enqueue_script('jquery');
  // comment ajax reply
  if ( is_singular() ) wp_enqueue_script( 'comment-reply' );   

	// wow animation js
	wp_register_script( 'magExpress_wowjs', get_template_directory_uri() . '/js/wow.min.js', false, null, true );   
	// bootstrap min js
	wp_register_script( 'magExpress_bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', false, null, true );
	// slick slider
	wp_register_script( 'magExpress_slick', get_template_directory_uri() . '/js/slick.min.js', false, null, true );
	// custom js functions
	wp_register_script( 'magExpress_customjs', get_template_directory_uri() . '/js/custom.js', false, null, true );
	
    wp_enqueue_script( 'magExpress_bootstrapjs' );	// hook bootstrap
    wp_enqueue_script( 'magExpress_wowjs' ); // hook wow js
    wp_enqueue_script( 'magExpress_slick' ); // hook slick js
    wp_enqueue_script( 'magExpress_customjs' ); // hook custom functions
}


////////////////////////////////
// CSS styles
/////////////////////////////////

function magExpress_CybarMagazine_styles()
{
    // Register the style like this for a theme:
    wp_register_style( 'magExpress_bootstrapmin', get_template_directory_uri() . '/css/bootstrap.min.css','all' );
    wp_register_style( 'magExpress_fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css','all' );
    wp_register_style( 'magExpress_animation', get_template_directory_uri() . '/css/animate.css','all' );
    wp_register_style( 'magExpress_googlefont_oswald','http://fonts.googleapis.com/css?family=Oswald','all' );
    wp_register_style( 'magExpress_slickslider', get_template_directory_uri() . '/css/slick.css','all' );
	wp_register_style( 'magExpress_comments', get_template_directory_uri() . '/css/comments.css','all' );	
    wp_register_style( 'style', get_template_directory_uri() . '/style.css','all' );
 
    // For either a plugin or a theme, you can then enqueue the style:
    wp_enqueue_style( 'magExpress_bootstrapmin' );
    wp_enqueue_style( 'magExpress_fontawesome' );
    wp_enqueue_style( 'magExpress_animation' );
    wp_enqueue_style( 'magExpress_googlefont_oswald' );
    wp_enqueue_style( 'magExpress_newsticker' );
    wp_enqueue_style( 'magExpress_slickslider' );
    wp_enqueue_style( 'magExpress_default_color' );
    wp_enqueue_style( 'magExpress_comments' );
    wp_enqueue_style( 'style' );
}
add_action( 'wp_enqueue_scripts', 'magExpress_CybarMagazine_styles' );

?>
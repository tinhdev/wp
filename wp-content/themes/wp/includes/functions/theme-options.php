<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux_Framework_sample_config' ) ) {

        class Redux_Framework_sample_config {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }

            }

            public function initSettings() {

                // Just for demo purposes. Not needed per say.
                $this->theme = wp_get_theme();

                // Set the default arguments
                $this->setArguments();

                // Set a few help tabs so you can see how it's done
                $this->setHelpTabs();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }

                // If Redux is running as a plugin, this will remove the demo notice and links
                //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

                // Function to test the compiler hook and demo CSS output.
                // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
                //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);

                // Change the arguments after they've been declared, but before the panel is created
                //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

                // Change the default value of a field after it's been set, but before it's been useds
                //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

                // Dynamically add a section. Can be also used to modify sections/fields
                //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            /**
             * This is a test function that will let you see when the compiler hook occurs.
             * It only runs if a field    set with compiler=>true is changed.
             * */
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
                //print_r($options); //Option values
                //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

                /*
              // Demo of how to use the dynamic CSS and write your own static CSS file
              $filename = dirname(__FILE__) . '/style' . '.css';
              global $wp_filesystem;
              if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
              WP_Filesystem();
              }

              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             */
            }

            /**
             * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
             * Simply include this function in the child themes functions.php file.
             * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
             * so you must use get_template_directory_uri() if you want to use any of the built in icons
             * */
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                    'icon'   => 'el-icon-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );

                return $sections;
            }

            /**
             * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
             * */
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;

                return $args;
            }

            /**
             * Filter hook for filtering the default value of any given field. Very useful in development mode.
             * */
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';

                return $defaults;
            }

            // Remove the demo link and the notice of integrated demo from the redux-framework plugin
            function remove_demo() {

                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }

            public function setSections() {

                /**
                 * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
                 * */
                // Background Patterns Reader
                $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
                $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
                $sample_patterns      = array();

                if ( is_dir( $sample_patterns_path ) ) :

                    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) :
                        $sample_patterns = array();

                        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                                $name              = explode( '.', $sample_patterns_file );
                                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                                $sample_patterns[] = array(
                                    'alt' => $name,
                                    'img' => $sample_patterns_url . $sample_patterns_file
                                );
                            }
                        }
                    endif;
                endif;

                ob_start();

                $ct          = wp_get_theme();
                $this->theme = $ct;
                $item_name   = $this->theme->get( 'Name' );
                $tags        = $this->theme->Tags;
                $screenshot  = $this->theme->get_screenshot();
                $class       = $screenshot ? 'has-screenshot' : '';

                $customize_title = sprintf( __( 'Customize &#8220;%s&#8221;', 'redux-framework-demo' ), $this->theme->display( 'Name' ) );

                ?>
                <div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
                    <?php if ( $screenshot ) : ?>
                        <?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
                            <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
                               title="<?php echo esc_attr( $customize_title ); ?>">
                                <img src="<?php echo esc_url( $screenshot ); ?>"
                                     alt="<?php esc_attr_e( 'Current theme preview', 'redux-framework-demo' ); ?>"/>
                            </a>
                        <?php endif; ?>
                        <img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
                             alt="<?php esc_attr_e( 'Current theme preview', 'redux-framework-demo' ); ?>"/>
                    <?php endif; ?>

                    <h4><?php echo $this->theme->display( 'Name' ); ?></h4>

                    <div>
                        <ul class="theme-info">
                            <li><?php printf( __( 'By %s', 'redux-framework-demo' ), $this->theme->display( 'Author' ) ); ?></li>
                            <li><?php printf( __( 'Version %s', 'redux-framework-demo' ), $this->theme->display( 'Version' ) ); ?></li>
                            <li><?php echo '<strong>' . __( 'Tags', 'redux-framework-demo' ) . ':</strong> '; ?><?php printf( $this->theme->display( 'Tags' ) ); ?></li>
                        </ul>
                        <p class="theme-description"><?php echo $this->theme->display( 'Description' ); ?></p>
                        <?php
                            if ( $this->theme->parent() ) {
                                printf( ' <p class="howto">' . __( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'redux-framework-demo' ) . '</p>', __( 'http://codex.wordpress.org/Child_Themes', 'redux-framework-demo' ), $this->theme->parent()->display( 'Name' ) );
                            }
                        ?>

                    </div>
                </div>

                <?php
                $item_info = ob_get_contents();

                ob_end_clean();

                //Start Theme Preview in admin panel**************************************************
				
                
                $wpf_facebook_like_box = '
									<div class="wpf_fb_like_box">
										<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fwpfreeware&amp;width&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:62px;" allowTransparency="true"></iframe>				
									</div>
										';				
				
				$sampleHTML_sybarmagazine = '
				
					<div class="theme_preview_container">
						<img src="'.get_template_directory_uri() .'/img/themes/cybarmagazine .png" alt="viewport" />
						<div class="btn_container">
							<a href="http://wpfreeware.com/preview/sybarmagazine/" target="_blank">Preview</a> 
							<a href="http://www.wpfreeware.com/sybarmagazine-wordpress-magazine-theme/" target="_blank">Download</a>
						</div>
					</div>					
						';
						
                $sampleHTML_colormag = '
				
					<div class="theme_preview_container">
						<img src="'.get_template_directory_uri() .'/img/themes/ColorMag.png" alt="viewport" />
						<div class="btn_container">
							<a href="http://wpfreeware.com/preview/colormag/" target="_blank">Preview</a> 
							<a href="http://www.wpfreeware.com/colormag-free-wordpress-blogmagazine-theme/" target="_blank">Download</a>
						</div>
					</div>
					
					';
					
                $sampleHTML_viewport = '
				
					<div class="theme_preview_container">
						<img src="'.get_template_directory_uri() .'/img/themes/Viewport.png" alt="viewport" />
						<div class="btn_container">
							<a href="http://wpfreeware.com/preview/viewport-wordpress-magazine-theme/" target="_blank">Preview</a> 
							<a href="http://www.wpfreeware.com/viewport/" target="_blank">Download</a>
						</div>
					</div>  
					
					
					<p class="view_all_themes"><a href="http://wpfreeware.com/" target="_blank">View All Free Themes</a></p>					
					';
					
					//End Theme Preview in admin panel**************************************************
				
				/*
			   if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
                    Redux_Functions::initWpFilesystem();

                    global $wp_filesystem;

                    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
                }
					*/		

                // ACTUAL DECLARATION OF SECTIONS
				
				// General Settings *********************************************************
                $this->sections[] = array(
                    'title'  => __( 'General Settings', 'wpf' ),
                    'desc'   => __( '<b>magExpress has it\'s own features which will allow you to change everything according to your requirements. If you need any help or have any questions about magExpress, feel free let us know on the theme <a href="http://www.wpfreeware.com/magexpress-fancy-style-wordpress-magazine-theme/" target="_blank">Home Page</a>.</b>', 'wpf' ),
                    'icon'   => 'el-icon-cogs',
                    //'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),					
					
                        array(
                            'id'       => 'logo_uploader',
                            'type'     => 'media',
                            'url'      => true,
                            'title'    => __( 'Logo Uploader', 'wpf' ),
                            'desc'     => __( 'Upload your logo here. Best size for logo is 300x100', 'wpf' ),
                        ),	

                        array(
                            'id'       => 'favicon_uploader',
                            'type'     => 'media',
                            'url'      => true,
                            'title'    => __( 'Favicon Uploader', 'wpf' ),
                            'desc'     => __( 'Upload your favicon here.', 'wpf' ),
                        ),	
						
                    ),
                );
				
				// Slider Settings *********************************************************

                $this->sections[] = array(
                    'icon'   => 'el-icon-home-alt',
                    'title'  => __( 'Home Settings', 'wpf' ),
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),						
                        array(
                            'id'     => 'top_slider_area',
                            'type'   => 'info',
                            'style'  => 'success',
                            'icon'   => 'el-icon-info-sign',
                            'title'  => __( 'Top Slider Area Settings', 'wpf' ),
                            'desc'   => __( 'Put all the fields below for setting up top slider area in the home page template.', 'wpf' )
                        ),						
					
                        array(
                            'id'       => 'top_slider_category',
                            'type'     => 'select',
                            'data'     => 'categories',
                            'title'    => __( 'Select Category For Top Slider', 'wpf' ),
                            'desc'     => __( 'Select a category for top Slider. Slides will be displaying from selected category.', 'wpf' ),
                        ),
                        array(
                            'id'       => 'top_slider_slide_number',
                            'type'     => 'text',
                            'title'    => __( 'Top Slider - Slide Number', 'wpf' ),
                            'desc'     => __( 'Put a numeric value for slides. Ex: if you put 5 there will be 5 slide available.)', 'wpf' ),
                        ),

                        array(
                            'id'       => 'top_featured_content',
                            'type'     => 'select',
                            'data'     => 'categories',
                            'title'    => __( 'Top Featured Contents', 'wpf' ),
                            'desc'     => __( 'Select a category for Top slider area contents where you can display 4 posts beside the top slider from any category you choose. Content will be displaying from selected category.', 'wpf' ),
                        ),							

                        array(
                            'id'     => 'bottom_slider',
                            'type'   => 'info',
                            'style'  => 'success',
                            'icon'   => 'el-icon-info-sign',
                            'title'  => __( 'Bottom Slider Settings', 'wpf' ),
                            'desc'   => __( 'Put the informations below for bottom featured slider on home page template', 'wpf' )
                        ),	
						
                        array(
                            'id'       => 'bottom_slider_category',
                            'type'     => 'select',
                            'data'     => 'categories',
                            'title'    => __( 'Select Category For Bottom Slider', 'wpf' ),
                            'desc'     => __( 'Select a category for Bottom Slider. Content will be displaying from selected category.', 'wpf' ),
                        ),
                        array(
                            'id'       => 'bottom_slider_slide_number',
                            'type'     => 'text',
                            'title'    => __( 'Bottom Slider - Slide Number', 'wpf' ),
                            'desc'     => __( 'Put a numeric value for slides. Ex: if you put 5 there will be 5 slide available.)', 'wpf' ),
                        ),
							
                    )
                );	
				
				
				// Shortcodes Settings *********************************************************
				
                $this->sections[] = array(
                    'icon'   => 'el-icon-view-mode',
                    'title'  => __( 'Shortcodes', 'wpf' ),
                    'fields' => array(
					
						
                        array(
                            'id'     => 'home_shortcodes',
                            'type'   => 'info',
                            'style'  => 'critical',
                            'icon'   => 'el-icon-info-sign',
                            'title'  => __( 'Shortcodes', 'wpf' ),
							'subtitle' => __( 'We made some useful shortcodes which will allow you to easily display contents in your theme.', 'wpf' ),
                            //'desc'   => __( 'Us these shortcodes in the home page template editor. You can display contents from any category by using these shortcodes below.', 'wpf' )
                        ),	
						
                        array(
                            'id'   => 'home_content_style_1_shortcode',
                            'type' => 'info',
							'title'  => __( '1. Content Style one on Home page template', 'wpf' ),
                            'desc' => __( '<b>[home_content_style_1 title="title goes here." category="category name" show_post="4"]</b> <br/> Display contents from any category on the home page template.Note: use this shortcode in the home page template editor.', 'wpf' )
                        ),	
                        array(
                            'id'   => 'home_content_style_2_shortcode',
                            'type' => 'info',
							'title'  => __( '2. Content Style two on Home page template', 'wpf' ),
                            'desc' => __( '<b>[home_content_style_2 title1="" category1="category name 1" title2="" category2="category name 2" show_post="4"]</b> <br/> Display <b>2 column</b> contents from any category on the home page template.Note: use this shortcode in the home page template editor.', 'wpf' )
                        ),						
						
                        array(
                            'id'   => 'featured_sidebar_shortcodes',
                            'type' => 'info',
							'title'  => __( '3. Featured sidebar content shortcode', 'wpf' ),
                            'desc' => __( '<b>[featured_sidebar_content category="your category name" show_post="2"]</b> <br/> Display contents from any category in the home page featured sidebars.', 'wpf' )
                        ),
                        array(
                            'id'   => 'sidebar_category_content_shortcode',
                            'type' => 'info',
							'title'  => __( '4. Sidebar content shortcode', 'wpf' ),
                            'desc' => __( '<b>[sidebar_category_content category="your category name" show_post="3"]</b> <br/> Display contents from any category in the sidebar.', 'wpf' )
                        ),
                        array(
                            'id'   => 'sidebar_tab_shortcode',
                            'type' => 'info',
							'title'  => __( '5. Sidebar Tab shortcode', 'wpf' ),
                            'desc' => __( '<b>[right_sidebar_tab show_post="5"]</b> <br/> Display popular posts & recent posts in the sidebar in a Tab', 'wpf' )
                        ),	
                        array(
                            'id'   => 'video_shortcode',
                            'type' => 'info',
							'title'  => __( '6. Video Shortcode - only for youtube/vimeo', 'wpf' ),
                            'desc' => __( '<b>[video w="300" h="200" url="video url"]', 'wpf' )
                        ),
                        array(
                            'id'   => 'btn_shortcodes',
                            'type' => 'info',
							'title'  => __( '7. Button Shortcode', 'wpf' ),
                            'desc' => __( '<b>[button url="button url" target="_self" text="button text" color="btn-default"]</b> <br/> Note: Available button colors are: 
										btn-default, btn-basic, btn-success, btn-warning, btn-info, btn-danger, btn-primary', 'wpf' )
                        ),
						
                    )
                );					
				
				// Blog page styles setting *********************************************************
				
                $this->sections[] = array(
                    'icon'   => 'el-icon-asl',
                    'title'  => __( 'Blog Page Styles', 'wpf' ),
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),						
					
                        array(
                            'id'       => 'blog_page_selection',
                            'type'     => 'radio',
                            'title'    => __( 'Select Blog Page Style', 'wpf' ),
                            'desc'     => __( 'Select blog page style. 3 styles are available. <br/> Note: Category/Tag archive page is default set to Medium Thmubnail.', 'wpf' ),
                            //Must provide key => value pairs for radio options
                            'options'  => array(
                                '1' => 'Style 1 - Medium Thmubnail',
                                '2' => 'Style 2 - Small Thmubnail',
                                '3' => 'Style 3 - Big Thumbnail'
                            ),
                            'default'  => '1'
                        ),
                    )
                );					

				// Color Settings *********************************************************
				
                $this->sections[] = array(
                    'icon'   => 'el-icon-brush',
                    'title'  => __( 'Theme Colors', 'wpf' ),
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),						
					
                        array(
                            'id'     => 'theme_color_info',
                            'type'   => 'info',
                            'style'  => 'success',
                            'icon'   => 'el-icon-info-sign',
                            'title'  => __( 'Do you know?', 'wpf' ),
                            'desc'   => __( 'magExpress is suported unlimited color schemes. Choose yours.', 'wpf' )
                        ),						
					
                        array(
                            'id'       => 'magexpress_colors',
                            'type'     => 'color',
                            'title'    => __( 'Theme Color Schemes ', 'wpf' ),
                            'subtitle' => __( 'Select theme color (default: #FFA500).', 'wpf' ),
                            'default'  => '#FFA500',
                            'validate' => 'color',
                        ),
                    )
                );	
						
				
				// Add code Settings *********************************************************
				
                $this->sections[] = array(
                    'icon'   => 'el-icon-th-list',
                    'title'  => __( 'ADD Code', 'wpf' ),
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),						
					
                        array(
                            'id'       => 'add_code',
                            'type'     => 'textarea',
                            'title'    => __( 'Add Code', 'wpf' ),
                            'desc'     => __( 'Put add code here. Add size is 728x90. This add will appear in the header section', 'wpf' ),
                        ),
                    )
                );				
				
				// Footer Settings *********************************************************
				
                $this->sections[] = array(
                    'icon'   => 'el-icon-text-width',
                    'title'  => __( 'Footer Settings', 'wpf' ),
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),						
					
                        array(
                            'id'       => 'footer_text',
                            'type'     => 'text',
                            'title'    => __( 'Footer Text', 'wpf' ),
                            'desc'     => __( 'Put your footer/copyright texts here.', 'wpf' ),
                        ),
                        array(
                            'id'       => 'google_analytics',
                            'type'     => 'textarea',
                            'title'    => __( 'Google Analytics', 'wpf' ),
                            'desc'     => __( 'Put your entire google analytics code here which you will get from google.', 'wpf' ),
							'validate' => 'js',
                        ),
                    )
                );
				
				// Start More themes *********************************************************
				
                $this->sections[] = array(
                    'icon'   => 'el-icon-laptop',
                    'title'  => __( 'More Free Themes', 'wpf' ),
					'desc'     => __( 'We are releasing premium quality free wordpress themes which are 100% free. View all themes <a href="http://www.wpfreeware.com" target="_blank">here</a><br/>
									   Dont for to like our <a href="https://www.facebook.com/wpfreeware" target="_blank">Facebook Page</a> to get updated our newly released free themes.', 'wpf' ),
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),						
                        array(
                            'id'     => 'sybar_magazine_preview',
                            'type'   => 'info',
                            'style'  => 'critical',
                            'icon'   => 'el-icon-star',
                            'title'  => __( 'Sybar Magazine', 'wpf' ),
                            'desc'   => __( 'A Awesome responsive magazine theme', 'wpf' )
                        ),						
                        array(
                            'id'       => 'wpf_more_themes_sybarmagazine',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $sampleHTML_sybarmagazine,
                        ),
                        array(
                            'id'     => 'colormag_preview',
                            'type'   => 'info',
                            'style'  => 'critical',
                            'icon'   => 'el-icon-star',
                            'title'  => __( 'ColorMag', 'wpf' ),
                            'desc'   => __( 'Mashable style responsive blog & magazine theme.', 'wpf' )
                        ),						
                        array(
                            'id'       => 'wpf_more_themes_colormag',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $sampleHTML_colormag,
                        ),
                        array(
                            'id'     => 'viewport_preview',
                            'type'   => 'info',
                            'style'  => 'critical',
                            'icon'   => 'el-icon-star',
                            'title'  => __( 'ViewPort', 'wpf' ),
                            'desc'   => __( 'Responsive magazine style blog theme', 'wpf' )
                        ),						
                        array(
                            'id'       => 'wpf_more_themes_viewport',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $sampleHTML_viewport,
                        ),						
                    )
                );		

				//End more themes**************************************************
			

                $this->sections[] = array(
                    'title'  => __( 'Import / Export', 'wpf' ),
                    'desc'   => __( 'Import and Export your settings from file, text or URL.', 'wpf' ),
                    'icon'   => 'el-icon-refresh',
                    'fields' => array(
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),					
                        array(
                            'id'         => 'import_export_settings',
                            'type'       => 'import_export',
                            'title'      => 'Import Export',
                            'subtitle'   => 'Save and restore your options settings',
                            'full_width' => false,
                        ),
                    ),
                );


                $this->sections[] = array(
                    'icon'   => 'el-icon-info-sign',
                    'title'  => __( 'Theme Information', 'wpf' ),
                    'desc'   => __( '<p class="description">Theme details are available here.</p>', 'wpf' ),
                    'fields' => array(
					
                        array(
                            'id'       => 'fb_like_box',
                            'type'     => 'info',
                            'raw_html' => true,
                            'desc'     => $wpf_facebook_like_box,
                        ),
						
                        array(
                            'id'      => 'opt-raw-info',
                            'type'    => 'raw',
                            'content' => $item_info,
                        )
                    ),
                );

            }

            public function setHelpTabs() {

                // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-1',
                    'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
                );

                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-2',
                    'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
                );

                // Set the help sidebar
                $this->args['help_sidebar'] = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
            }

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                    'opt_name'             => 'magExpress',
                    // This is where your data is stored in the database and also becomes your global variable name.
                    'display_name'      => __(' | ') . $theme->get('Description'),
                    // Name that appears at the top of your panel
                    'display_version'      => $theme->get( 'Version' ),
                    // Version that appears at the top of your panel
                    'menu_type'            => 'menu',
                    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                    'allow_sub_menu'       => true,
                    // Show the sections below the admin menu item or not
                    'menu_title'           => __( 'magExpress', 'redux-framework-demo' ),
                    'page_title'           => __( 'magExpress', 'redux-framework-demo' ),
                    // You will need to generate a Google API key to use this feature.
                    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                    'google_api_key'       => '',
                    // Set it you want google fonts to update weekly. A google_api_key value is required.
                    'google_update_weekly' => false,
                    // Must be defined to add google fonts to the typography module
                    'async_typography'     => true,
                    // Use a asynchronous font on the front end or font string
                    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                    'admin_bar'            => true,
                    // Show the panel pages on the admin bar
                    //'menu_icon'         => get_template_directory_uri().'/img/infinity.svg',              
                    // Choose an icon for the admin bar menu
                    'admin_bar_priority' => 50,
                    // Choose an priority for the admin bar menu
                    'global_variable'      => '',
                    // Set a different name for your global variable other than the opt_name
                    'dev_mode'             => false,
                    // Show the time the page took to load, etc
                    //'update_notice'        => true,
                    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                    'customizer'           => true,
                    // Enable basic customizer support
                    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                    // OPTIONAL -> Give you extra features
                    'page_priority'        => null,
                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                    'page_parent'          => 'themes.php',
                    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                    'page_permissions'     => 'manage_options',
                    // Permissions needed to access the options panel.
                    'menu_icon'            => get_template_directory_uri().'/img/wpf-menu-logo.png', // this is menu icon
                    // Specify a custom URL to an icon
                    'last_tab'             => '',
                    // Force your panel to always open to a specific tab (by id)
                    'page_icon'            => 'icon-themes',
                    // Icon displayed in the admin panel next to your menu_title
                    'page_slug'            => '_options',
                    // Page slug used to denote the panel
                    'save_defaults'        => true,
                    // On load save the defaults to DB before user clicks save or not
                    'default_show'         => false,
                    // If true, shows the default value next to each field that is not the default value.
                    'default_mark'         => '',
                    // What to print by the field's title if the value shown is default. Suggested: *
                    'show_import_export'   => true,
                    // Shows the Import/Export panel when not used as a field.

                    // CAREFUL -> These options are for advanced use only
                    'transient_time'       => 60 * MINUTE_IN_SECONDS,
                    'output'               => true,
                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                    'output_tag'           => true,
                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                    'database'             => '',
                    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                    'system_info'          => false,
                    // REMOVE

                    // HINTS
                    'hints'                => array(
                        'icon'          => 'icon-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );

				/*
                // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
                $this->args['admin_bar_links'][] = array(
                    'id'    => 'redux-docs',
                    'href'   => 'http://docs.reduxframework.com/',
                    'title' => __( 'Documentation', 'redux-framework-demo' ),
                );

                $this->args['admin_bar_links'][] = array(
                    //'id'    => 'redux-support',
                    'href'   => 'https://github.com/ReduxFramework/redux-framework/issues',
                    'title' => __( 'Support', 'redux-framework-demo' ),
                );

                $this->args['admin_bar_links'][] = array(
                    'id'    => 'redux-extensions',
                    'href'   => 'reduxframework.com/extensions',
                    'title' => __( 'Extensions', 'redux-framework-demo' ),
                );

                // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
                $this->args['share_icons'][] = array(
                    'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                    'title' => 'Visit us on GitHub',
                    'icon'  => 'el-icon-github'
                    //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                    'title' => 'Like us on Facebook',
                    'icon'  => 'el-icon-facebook'
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'http://twitter.com/reduxframework',
                    'title' => 'Follow us on Twitter',
                    'icon'  => 'el-icon-twitter'
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'http://www.linkedin.com/company/redux-framework',
                    'title' => 'Find us on LinkedIn',
                    'icon'  => 'el-icon-linkedin'
                );
				
				*/

				/*
                // Panel Intro text -> before the form
                if ( ! isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
                    if ( ! empty( $this->args['global_variable'] ) ) {
                        $v = $this->args['global_variable'];
                    } else {
                        $v = str_replace( '-', '_', $this->args['opt_name'] );
                    }
                    $this->args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
                } else {
                    $this->args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
                }

                // Add content after the form.
                $this->args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );
				
				*/
            }

            public function validate_callback_function( $field, $value, $existing_value ) {
                $error = true;
                $value = 'just testing';

                /*
              do your validation

              if(something) {
                $value = $value;
              } elseif(something else) {
                $error = true;
                $value = $existing_value;
                
              }
             */

                $return['value'] = $value;
                $field['msg']    = 'your custom error message';
                if ( $error == true ) {
                    $return['error'] = $field;
                }

                return $return;
            }

            public function class_field_callback( $field, $value ) {
                print_r( $field );
                echo '<br/>CLASS CALLBACK';
                print_r( $value );
            }

        }

        global $reduxConfig;
        $reduxConfig = new Redux_Framework_sample_config();
    } else {
        echo "The class named Redux_Framework_sample_config has already been called. <strong>Developers, you need to prefix this class with your company name or you'll run into problems!</strong>";
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ):
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    endif;

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ):
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error = true;
            $value = 'just testing';

            /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            
          }
         */

            $return['value'] = $value;
            $field['msg']    = 'your custom error message';
            if ( $error == true ) {
                $return['error'] = $field;
            }

            return $return;
        }
    endif;

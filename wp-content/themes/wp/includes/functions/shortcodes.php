<?php 

//////////////////////////////////
// shortcode for youtube......
///////////////////////////////////

function video($atts){
	extract( shortcode_atts( array(
		'w' => '300',
		'h' => '200',
		'url' => ''
	), $atts, 'video' ) );
	
	$embedurl = magExpress_parse_url($url);
	
	return '<iframe src="'.$embedurl.'" frameborder="0" width="'.$w.'px" height="'.$h.'px" allowfullscreen></iframe>';
	
}
add_shortcode('video', 'video');

//////////////////////////////////
// shortcode for button......
///////////////////////////////////

function button($atts){
	extract( shortcode_atts( array(
		'url' => '',
		'target' => '_self',
		'text' => '',
		'color' => 'btn-default' // available btn-default, btn-basic, btn-success, btn-warning, btn-info, btn-danger, btn-primary
	), $atts, 'button' ) );
	
	return '<a class="btn '.$color.'" href="'.$url.'" target="'.$target.'">'.$text.'</a>';
	
}
add_shortcode('button', 'button');

//////////////////////////////////////////////
// Featured Left sidebar Category Post
//////////////////////////////////////////////

function wpf_bottom_featured_category_shortcode($atts){
	extract( shortcode_atts( array(
		'category' => '',
		'show_post' => '2'
	), $atts, 'featured_sidebar_content' ) );
	
    $q = new WP_Query(
        array( 'category_name' => $category, 'posts_per_page' => ''.$show_post.'', 'post_type' => 'post')
        );
				
		
		
$list = '<ul class="catg1_nav">';

while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'bottom_featured_left_img' );      
    $list .= '

	  <li>
		<div class="catgimg_container">
		  <a href="'.get_permalink().'" class="catg1_img">
			 '.$post_thumbnail.'
		  </a>
		</div>
		<h3 class="post_titile"><a href="'.get_permalink().'">'.get_the_title().'</a></h3>
	  </li>		
				

	';        
endwhile;
$list.= '</ul>';
wp_reset_query();
return $list;
}
add_shortcode('featured_sidebar_content', 'wpf_bottom_featured_category_shortcode');


//////////////////////////////////////////////
// Main Sidebar Category Post
//////////////////////////////////////////////

function wpf_main_sidebar_category_shortcode($atts){
	extract( shortcode_atts( array(
		'category' => '',
		'show_post' => '3'
	), $atts, 'sidebar_category_content' ) );
	
    $q = new WP_Query(
        array( 'category_name' => $category, 'posts_per_page' => ''.$show_post.'', 'post_type' => 'post')
        );
			
		
		
$list = '<ul class="small_catg popular_catg wow fadeInDown">';

while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'small_thmub_img' );      
    $list .= '

		<li>
		  <div class="media">
			<a href="'.get_permalink().'" class="media-left">
			  '.$post_thumbnail.'
			</a>
			<div class="media-body">
			  <h4 class="media-heading"><a href="'.get_permalink().'">'.get_the_title().'</a></h4> 
			  <p>'.get_the_excerpt().'</p>
			</div>
		  </div>
		</li>	  
				

	';        
endwhile;
$list.= '</ul>';
wp_reset_query();
return $list;
}
add_shortcode('sidebar_category_content', 'wpf_main_sidebar_category_shortcode');


//////////////////////////////////////////////
// Tab in Right sidebar 
//////////////////////////////////////////////

function wpf_right_sidebar_tab_shortcode($atts){
	extract( shortcode_atts( array(
		'show_post' => '5'
	), $atts, 'right_sidebar_tab' ) );
	
		
$list = '<ul role="tablist" class="nav nav-tabs custom-tabs">
                    <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#mostPopular">Most Popular</a></li>
                    <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="messages" href="#recentComent">Recent Post</a></li>
                  </ul>
                  <div class="tab-content">		'; 

// this query for popular posts
$q = new WP_Query( array( 'posts_per_page' => ''.$show_post.'','meta_key' => 'post_views_count','orderby' => 'meta_value_num','order' => 'DESC','post_type' => 'post' ) );		

$list .= '<div id="mostPopular" class="tab-pane fade in active" role="tabpanel">
						<ul class="small_catg popular_catg wow fadeInDown">'; 

while($q->have_posts()) : $q->the_post();

	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'small_thmub_img' ); 
    $list .= '
		
	  <li>
		<div class="media">
		  <a class="media-left" href="'.get_permalink().'">
			'.$post_thumbnail.'
		  </a>
		  <div class="media-body">
			<h4 class="media-heading"><a href="'.get_permalink().'">'.get_the_title().'</a></h4> 
			<p>'.get_the_excerpt().' </p>
		  </div>
		</div>
	  </li>		
				

	';        
endwhile;
$list.= '</ul></div>';


// this query for Recent posts
$q = new WP_Query( array( 'posts_per_page' => ''.$show_post.'', 'post_type' => 'post' ) );		

$list .= '<div id="recentComent" class="tab-pane fade" role="tabpanel">
						 <ul class="small_catg popular_catg">'; 

while($q->have_posts()) : $q->the_post();

	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'small_thmub_img' ); 
    $list .= '

	  <li>
		<div class="media">
		  <a class="media-left" href="'.get_permalink().'">
			'.$post_thumbnail.'
		  </a>
		  <div class="media-body">
			<h4 class="media-heading"><a href="'.get_permalink().'">'.get_the_title().'</a></h4> 
			<p>'.get_the_excerpt().' </p>
		  </div>
		</div>
	  </li>		
				

	';        
endwhile;
$list.= '</ul></div>';


$list.= '</div>';
wp_reset_query();
return $list;
}
add_shortcode('right_sidebar_tab', 'wpf_right_sidebar_tab_shortcode');


////////////////////////////////////////////////////
// category style 1
////////////////////////////////////////////////////

function wpf_home_content_style_1_shortcode($atts){
	extract( shortcode_atts( array(
		'title' => '',
		'category' => '',
		'show_post' => '4'
	), $atts, 'home_content_style_1' ) );
	

	$show_post_num = $show_post - 1; // real post number to display..
	
	// featured content loop
	
    $q = new WP_Query(
        array( 'category_name' => $category, 'posts_per_page' => '1', 'post_type' => 'post')
        );
		
    // Get the ID of a given category
    $category_id = get_cat_ID($category);
	$category_link = get_category_link( $category_id);		
	
	$list = '<div class="single_category wow fadeInDown">
                  <h2>                  
                    <span class="bold_line"><span></span></span>
                    <span class="solid_line"></span>
                    <a class="title_text" href="'.esc_url( $category_link ).'">'.$title.'</a>
                  </h2>';
				  
	$list .= '<div class="business_category_left wow fadeInDown">
                    <ul class="fashion_catgnav">';				  

	while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'home_content_featured_img' );      
    $list .= '

	  <li>
		<div class="catgimg2_container">
		  <a href="'.get_permalink().'">'.$post_thumbnail.'</a>
		</div>
		<h2 class="catg_titile"><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
		<div class="comments_box">
		  <span class="meta_date">'.get_the_time('d M, Y').'</span>
		  <span class="meta_comment"><a href="'.get_comments_link().'">'.get_comments_number().'</a></span>
		  <span class="meta_more"><a  href="'.get_permalink().'">Read More...</a></span>
		</div>
		<p>'.get_the_excerpt().'</p>
	  </li>  		
				

	';        
endwhile; // end featured content

$list.= '</ul></div>';

	// query 2 for bottom small content
    $q = new WP_Query(
        array( 'category_name' => $category, 'posts_per_page' => ''.$show_post_num.'', 'post_type' => 'post', 'offset' => '1')
        );	
	
	$list .= ' <div class="business_category_right wow fadeInDown">
                    <ul class="small_catg">';

	while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'small_thmub_img' );      
    $list .= ' 

	  <li>
		<div class="media wow fadeInDown">
		  <a class="media-left" href="'.get_permalink().'">
			'.$post_thumbnail.'
		  </a>
		  <div class="media-body">
			<h4 class="media-heading"><a href="'.get_permalink().'">'.get_the_title().'</a></h4> 
			<div class="comments_box">
			  <span class="meta_date">'.get_the_time('d M, Y').'</span>
			  <span class="meta_comment"><a href="'.get_comments_link().'">'.get_comments_number().'</a></span>
			</div>
		  </div>
		</div>
	  </li>	  
				

	';        
endwhile;
$list.= '</ul></div>'; // end beside small content

$list.= '</div>'; // end this category style container
wp_reset_query();
return $list;
}
add_shortcode('home_content_style_1', 'wpf_home_content_style_1_shortcode');


////////////////////////////////////////////////////
// Home content category style 2 (2 column)
////////////////////////////////////////////////////

function wpf_home_content_style_2_shortcode($atts){
	extract( shortcode_atts( array(
		'category1' => '',
		'category2' => '',
		'title1' => 'Category1 title',
		'title2' => 'Category2 title',
		'show_post' => '4'
	), $atts, 'home_content_style_2' ) );
	

	$show_post_num = $show_post - 1; // real post number to display..
	
	///////////////////// start column 1 content /////////////////////////////
	
	// featured content loop
	
    $q = new WP_Query(
        array( 'category_name' => $category1, 'posts_per_page' => '1', 'post_type' => 'post')
        );
		
    // Get the ID of a given category
    $category_id1 = get_cat_ID($category1);
	$category_link1 = get_category_link( $category_id1 );		
	
	$list = '<div class="games_fashion_area">';
	$list .= '<div class="games_category">
                    <div class="single_category">
                      <h2>                  
                        <span class="bold_line"><span></span></span>
                        <span class="solid_line"></span>
                        <a class="title_text" href="'.esc_url( $category_link1 ).'">'.$title1.'</a>
                      </h2>
					  <ul class="fashion_catgnav wow fadeInDown">';

	while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'home_content_featured_img' );      
    $list .= '
				

		<li>
		  <div class="catgimg2_container">
			<a href="'.get_permalink().'">'.$post_thumbnail.'</a>
		  </div>
		  <h2 class="catg_titile"><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
		  <div class="comments_box">
			<span class="meta_date">'.get_the_time('d M, Y').'</span>
			<span class="meta_comment"><a href="'.get_comments_link().'">'.get_comments_number().'</a></span>
			<span class="meta_more"><a  href="'.get_permalink().'">Read More...</a></span>
		  </div>
		  <p>'.get_the_excerpt().'</p>
		</li>		
				

	';        
endwhile; // end featured content

$list.= '</ul>';

	// query 2 for bottom small content
    $q = new WP_Query(
        array( 'category_name' => $category1, 'posts_per_page' => ''.$show_post_num.'', 'post_type' => 'post', 'offset' => '1')
        );	
	
	$list .= '<ul class="small_catg wow fadeInDown">';

	while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'small_thmub_img' );      
    $list .= ' 
	  

	<li>
	  <div class="media">
		<a class="media-left" href="'.get_permalink().'">
		 '.$post_thumbnail.'
		</a>
		<div class="media-body">
		  <h4 class="media-heading"><a href="'.get_permalink().'">'.get_the_title().'</a></h4> 
		  <div class="comments_box">
			<span class="meta_date">'.get_the_time('d M, Y').'</span>
			<span class="meta_comment"><a href="'.get_comments_link().'">'.get_comments_number().'</a></span>
		  </div>
		</div>
	  </div>
	</li>	  
				

	';        
endwhile;
$list.= '</ul>'; // end bottom small content

$list.= '</div></div>'; // end 1 column 



	////////////////////////// start column 2 content////////////////////////////////////
	
	// featured content loop
    $q = new WP_Query(
        array( 'category_name' => $category2, 'posts_per_page' => '1', 'post_type' => 'post')
        );	
		
    // Get the URL of this category
	$category_id2 = get_cat_ID($category2);
    $category_link2 = get_category_link( $category_id2 );		
	
	$list .= '<div class="fashion_category">
                    <div class="single_category">
                      <div class="single_category wow fadeInDown">
                        <h2>                  
                          <span class="bold_line"><span></span></span>
                          <span class="solid_line"></span>
                          <a class="title_text" href="'.esc_url( $category_link2 ).'">'.$title2.'</a>
                        </h2>
						<ul class="fashion_catgnav wow fadeInDown">';

	while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'home_content_featured_img' );      
    $list .= '

	  <li>
		<div class="catgimg2_container">
		  <a href="'.get_permalink().'">'.$post_thumbnail.'</a>
		</div>
		<h2 class="catg_titile"><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
		<div class="comments_box">
		  <span class="meta_date">'.get_the_time('d M, Y').'</span>
		  <span class="meta_comment"><a href="'.get_comments_link().'">'.get_comments_number().'</a></span>
		  <span class="meta_more"><a  href="'.get_permalink().'">Read More...</a></span>
		</div>
		<p>'.get_the_excerpt().'</p>
	  </li>  		
				

	';        
endwhile; 

$list.= '</ul>';	// end featured content

	// query 2 for bottom small content
    $q = new WP_Query(
        array( 'category_name' => $category2, 'posts_per_page' => ''.$show_post_num.'', 'post_type' => 'post', 'offset' => '1')
        );	
	
	$list .= '<ul class="small_catg wow fadeInDown">';

	while($q->have_posts()) : $q->the_post();



	$post_thumbnail= get_the_post_thumbnail( $post->ID, 'small_thmub_img' );      
    $list .= ' 

	  <li>
		<div class="media wow fadeInDown">
		  <a class="media-left" href="'.get_permalink().'">
			'.$post_thumbnail.'
		  </a>
		  <div class="media-body">
			<h4 class="media-heading"><a href="'.get_permalink().'">'.get_the_title().'</a></h4> 
			<div class="comments_box">
			  <span class="meta_date">'.get_the_time('d M, Y').'</span>
			  <span class="meta_comment"><a href="'.get_comments_link().'">'.get_comments_number().'</a></span>
			</div>
		  </div>
		</div>
	  </li>	  
				

	';        
endwhile;
$list.= '</ul>'; // end beside small content

$list.= '</div></div></div>'; // end 2 column 

$list.= '</div>'; // end this category style container
wp_reset_query();
return $list;
}
add_shortcode('home_content_style_2', 'wpf_home_content_style_2_shortcode');


?>
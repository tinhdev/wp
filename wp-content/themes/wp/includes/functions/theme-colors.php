<?php 

function magexpress_theme_colors() {
	
    global $magExpress; if(!empty($magExpress['magexpress_colors'])) { 
			// echo $magExpress['magexpress_colors']
			
	$theme_colors = '<style type="text/css">
	

.scrollToTop,.pagination_area ul li a:hover,.pagination_area ul li a.active{
  background-color: '.$magExpress['magexpress_colors'].';
  color: #fff;
}
.scrollToTop:hover,.scrollToTop:focus,.pagination_area ul li a{
  background-color: #fff;
  color: '.$magExpress['magexpress_colors'].';
  border-color: 1px solid '.$magExpress['magexpress_colors'].';
}
.top_nav li a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.search_form input[type="submit"]:hover{
  background-color: '.$magExpress['magexpress_colors'].';
}
.navbar-default {
  background-color: '.$magExpress['magexpress_colors'].';
  border-color: '.$magExpress['magexpress_colors'].';  
}
.custom_nav li a:hover{ 
  border-color: #FFAE00;  
}
.navbar-default .navbar-nav > li > a:hover,.navbar-default .navbar-nav > li > a:focus,.active > a{
  background-color: #fff !important;
  color: '.$magExpress['magexpress_colors'].' !important;
  border-color: '.$magExpress['magexpress_colors'].' !important;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
  background-color: #fff;
 color: '.$magExpress['magexpress_colors'].';
 border-color: #FFAE00;
  
}
.navbar-nav > li > .dropdown-menu {
  margin-top: 1px;
  background-color: '.$magExpress['magexpress_colors'].';
}
.dropdown-menu > li > a:hover,.dropdown-menu > li > a:focus{
  background-color: #fff;
  color: '.$magExpress['magexpress_colors'].';
  border-color: '.$magExpress['magexpress_colors'].'; 
  padding-left: 20px;
}
.navbar-default .navbar-nav .open .dropdown-menu > li > a {
  color: #fff;
}
.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus {
  background-color: #fcc259;

}
 .slick-prev, .slick-next {
  background-color: '.$magExpress['magexpress_colors'].';
}  
.slick-prev:hover, .slick-next:hover {  
  opacity: 0.80;
}
.bold_line span {
  background-color: '.$magExpress['magexpress_colors'].';
}
.catg1_nav li .post_titile a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.content_middle_middle:after {
  background-color: '.$magExpress['magexpress_colors'].'; 
}
.content_middle_middle:before {
  background-color: '.$magExpress['magexpress_colors'].';  
}
.single_featured_slide>h2 a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.catg_titile a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
span.meta_date:hover,span.meta_comment:hover,span.meta_more:hover,span.meta_comment a:hover,span.meta_more a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.media-heading a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.single_bottom_rightbar>h2{
  border-bottom: 3px solid '.$magExpress['magexpress_colors'].';
}
.nav-tabs {
  border-bottom: 1px solid '.$magExpress['magexpress_colors'].';
}
.nav-tabs > li.active > a,.nav-tabs > li.active > a:focus {  
  color: '.$magExpress['magexpress_colors'].';
}
.nav-tabs > li.active > a:hover{
  color: '.$magExpress['magexpress_colors'].' !important;
}
.nav-tabs > li > a:hover{
  background-color: '.$magExpress['magexpress_colors'].'; 
  color: #fff !important; 
}
.single_bottom_rightbar ul li>a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.labels_nav li a:hover,.tagcloud a:hover{
  background-color: '.$magExpress['magexpress_colors'].';
}  
.breadcrumb {
  background-color: '.$magExpress['magexpress_colors'].';
  border: 2px solid '.$magExpress['magexpress_colors'].';
}
.single_page_area > h2 {
  border-left: 5px solid '.$magExpress['magexpress_colors'].';
}
.post_commentbox a:hover,.post_commentbox span:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.single_page_content blockquote {
  border-color: #eee '.$magExpress['magexpress_colors'].';
  border-left: 5px solid '.$magExpress['magexpress_colors'].';
}
.single_page_content ul li:before {
  background: none repeat scroll 0 0 '.$magExpress['magexpress_colors'].';
}
.post_pagination {
  border-bottom: 2px solid '.$magExpress['magexpress_colors'].';
  border-top: 2px solid '.$magExpress['magexpress_colors'].';
}
.prev {  
  border-right: 2px solid '.$magExpress['magexpress_colors'].';
}
.angle_left {
  background-color: '.$magExpress['magexpress_colors'].';
}
.angle_right {
  background-color: '.$magExpress['magexpress_colors'].';
}
.error_page_content h1:after, .error_page_content h1:before {  
  border: 2px solid '.$magExpress['magexpress_colors'].';
}
.error_page_content p {
  border-bottom: 2px solid '.$magExpress['magexpress_colors'].';
  border-top: 2px solid '.$magExpress['magexpress_colors'].';
}
.error_page_content p:after {
  border-top: 1px solid '.$magExpress['magexpress_colors'].';  
}
.error_page_content p:before {
  border-top: 1px solid '.$magExpress['magexpress_colors'].';  
}
.error_page_content p>a:hover{
  color: '.$magExpress['magexpress_colors'].';
}
.our_office {
  border-top: 2px solid '.$magExpress['magexpress_colors'].';
}
.contact_us{
 border-top: 2px solid '.$magExpress['magexpress_colors'].';
}
.contact_form input[type="submit"]:hover,.wpcf7-submit:hover{
  background-color: '.$magExpress['magexpress_colors'].';
  color: #fff;
  border-color: '.$magExpress['magexpress_colors'].';
}
.our_office:before {
  border-bottom: 1px solid '.$magExpress['magexpress_colors'].';  
}
.contact_us:before {
  border-bottom: 1px solid '.$magExpress['magexpress_colors'].';  
}
.single_footer_top > h2 {
  color: '.$magExpress['magexpress_colors'].';
}
.similar_post h2 i{
  color: '.$magExpress['magexpress_colors'].';
}

	
					</style>';
		 
		 echo $theme_colors;			
			
			
			
			
	 } else { 
		echo '<link href="'.get_template_directory_uri() .'/css/theme.css" rel="stylesheet" media="screen">';
	 } 	
	
}
add_action('wp_head', 'magexpress_theme_colors');

?>
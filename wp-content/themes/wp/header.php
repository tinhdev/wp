<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '|', true, 'right' );?></title>
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>	
	
    <?php 
		global $magExpress;
		if(!empty($magExpress['favicon_uploader']['url'])) { ?>
			<!--Favicon-->
			<link rel="shortcut icon" href="<?php echo $magExpress['favicon_uploader']['url'] ?>" type="image/x-icon" />
    <?php } ?>
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<?php wp_head();?>
  </head>
<body>
  <!-- =========================
    //////////////This Theme Design and Developed //////////////////////
    //////////// by www.wpfreeware.com======================-->

<?php get_template_part('includes/templates/pre-loader');?>
  
  <div class="container">
    <!-- start header area -->
    <header id="header">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <!-- start header top -->
          <div class="header_top">
            <div class="header_top_left">
			  <?php wp_nav_menu( array( 'theme_location' => 'wpf-top-menu','menu_class' => 'top_nav') ); ?>
            </div>
			
            <div class="header_top_right">
			  <?php get_template_part('searchform');?>
            </div>
			
          </div><!-- End header top -->
		  
          <!-- start header bottom -->
          <div class="header_bottom">
            <div class="header_bottom_left">
			<!-- for img logo -->
		   <?php global $magExpress;if(!empty($magExpress['logo_uploader']['url'])) { ?>
				<a href="<?php bloginfo('url');?>">
					<img src="<?php echo $magExpress['logo_uploader']['url'];?>" alt="<?php bloginfo('name');?>" />
				</a>
			<?php } else { ?>
					<a class="logo" href="<?php bloginfo('url');?>">
					  <img src="<?php echo get_template_directory_uri(); ?>/img/magexpress-logo.png" alt="logo">
					</a>
			<?php } ?>				
			 
            </div>
			

			<?php 
				global $magExpress;
				if(!empty($magExpress['add_code'])) { ?>
					<div class="header_bottom_right">
					  <?php echo $magExpress['add_code'];?>
					</div>					
			<?php } ?>			
			
          </div><!-- End header bottom -->
        </div>
      </div>
    </header><!-- End header area -->
	
     <!-- Static navbar -->
      <div id="navarea">
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
		  
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>             
            </div>
			
            <div id="navbar" class="navbar-collapse collapse">
			
			<?php /* Main menu navigation */
				wp_nav_menu( array(
				  'menu' => 'wpf-main-menu',
				  'depth' => 2,
				  'container' => false,
				  'menu_class' => 'nav navbar-nav custom_nav',
				  //Process nav menu using our custom nav walker
				  'walker' => new wp_bootstrap_navwalker())
				);
			?>
			
       
			  
            </div><!--/.nav-collapse -->
          </div><!--/.container-fluid -->
        </nav>
      </div>
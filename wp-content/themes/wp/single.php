	<?php 
	
	/**
	 *
	 * @package  magExpress
	 * @file     single.php
	 * @author   WpFreeware Team
	 * @link 	 http://wpfreeware.com
	 */			
	
	get_header();?>
	  
	  
      <!-- start site main content -->
      <section id="mainContent">
	  
		  
          <!-- start main content bottom -->
          <div class="content_bottom">
				
			<!--Start Main content -->  
			
            <div class="col-lg-8 col-md-8">
            
              <div class="content_bottom_left">		
				<div class="single_page_area">
				
					<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
					
					
					<?php if (have_posts()) : ?><?php while(have_posts())  : the_post(); ?>

						  <h2 class="<?php the_permalink();?>"><?php the_title();?></h2>
						  <div class="single_page_content">
							<?php get_template_part('post-meta');?>
							<?php the_content();?>
						  </div> 
						  
					  <!-- start post pagination  -->

						<?php get_template_part('includes/templates/post-nav');?>
					  
					  <!-- End post pagination  -->
					  
					  <!-- start share post -->
					  
						<?php get_template_part('includes/templates/post-share');?>
					  
					  <!-- End share post -->							  
					  
					  <!-- start similar post-->
						<?php get_template_part('includes/templates/related-posts');?>
					  <!-- End similar post-->
					  
					  <div class="comment_section">
						<?php comments_template( '', true ); ?> 
					  </div>
					  

						  
						
					<?php endwhile; ?>
					
					<?php else : ?>
							<h3 style="text-align:center;margin:100px auto;font-weight:bold;font-size:30px;line-height:35px;"><?php _e('Sorry! Nothing Found', 'wpf'); ?></h3>
					<?php endif; ?>						
					
 					
				
				</div>
			  

				
              </div>  
			  

		  
			  
            </div>
			
			<!--End Main content -->  
			
            <!-- start Sidebar -->
			<?php get_sidebar();?>
			<!-- End Sidebar -->
			
		</div><!-- end main content bottom -->        
      </section><!-- End site main content -->
    </div> <!-- /.container -->
	
	<?php get_footer();?>
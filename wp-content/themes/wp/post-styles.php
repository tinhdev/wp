<?php 
	global $magExpress;
	$blogStyles = $magExpress['blog_page_selection'];
	
	// Blog page style 1
	if((!empty($blogStyles)) && ($blogStyles == 1)) { ?>
		<?php get_template_part('includes/templates/excerpt-template/post-excerpt');?>
<?php } ?>


<?php // Blog page style 2

if((!empty($blogStyles)) && ($blogStyles ==2)) { ?>
		<?php get_template_part('includes/templates/excerpt-template/post-excerpt-2');?>
<?php } ?>	

<?php  // Blog page style 3
	if((!empty($blogStyles)) && ($blogStyles ==3)) { ?>
		<?php get_template_part('includes/templates/excerpt-template/post-excerpt-3');?>
<?php } ?>	
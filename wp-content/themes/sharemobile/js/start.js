$(document).ready(function () {
	if($(".btn-top").length > 0){
		$(window).scroll(function () {
			var e = $(window).scrollTop();
			if (e > 200) {
				$(".btn-top").show()
			} else {
				$(".btn-top").hide()
			}
		});
		$(".btn-top").click(function () {
			$('body,html').animate({
				scrollTop: 0
			})
		});
	}
	if($(".show-scroll").length > 0){
		$(window).scroll(function () {
			var e = $(window).scrollTop();
			if (e > 70) {
				$(".show-scroll").addClass("show");
				$(".show-scroll-1").addClass("show");
				$(".search").hide();
				$(".btn-send-mail").hide();
			} else {
				$(".show-scroll").removeClass("show");
				$(".show-scroll-1").removeClass("show");
				$(".search").show();
				$(".btn-send-mail").show();
			}
		});
	}
	if($(".slider-spec").length > 0){
		$('.slider-spec').slick({
			  centerMode: true,
			  centerPadding: '60px',
			  slidesToShow: 1,
			  responsive: [
				{
				  breakpoint: 768,
				  settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				  }
				}
			  ]
			});
	} 
	if($(".flexslider").length > 0){
		$('.flexslider').slick({
			 dots: true
		});
	}	
	if($(".carousel-1").length > 0){
		$('.carousel-1').slick({
			  infinite: false,
			  speed: 300,
			  slidesToShow: 4,
			  slidesToScroll: 4,
			  responsive: [
				{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true
				  }
				},
				{
				  breakpoint: 600,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				}
			  ]
			});
	}
	$(".thumb").each( function() {
	  if($(this).height() > $(this).find('img').height()){
		 $(this).find('img').css({"height": "100%" , "width": "auto"});
	  }
	  else {
		  $(this).find('img').css({"height": "auto" , "width": "100%"});
	  }
	});
	if($(".btn-action").length > 0){
		$(".btn-action").click(function() {
			if($(".menu-main").hasClass("active")){
				$(".menu-main").removeClass("active");
			}
			else {
				$(".menu-main").addClass("active");
				if($(window).height() >  $(".menu-main").height() +  $(".header").height()){
					$(".menu-main").css("height","auto");
				}
				else
				{
					$(".menu-main").css({"height": $(window).height() - $(".header").height() , "overflow-y": "auto"});;
				}
			}
		});
	}
	if($(".btn-searchsg").length > 0){
		$(".btn-searchsg").click(function() {
			if($(".frm-search").hasClass("active")){
				$(".frm-search").removeClass("active");
			}
			else {
				$(".frm-search").addClass("active");
				return false;
			}
		});
	} 	 	
});
$(document).click(function() {
	$(".menu-main, .frm-search").removeClass("active");
});
$('.menu-main,.btn-action, .frm-search').click(function(event){
	event.stopPropagation();
}); 
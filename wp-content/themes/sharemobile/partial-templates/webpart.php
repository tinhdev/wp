<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 31/05/2015
 * Time: 09:43
 */
function detect_shortcode_webpart()
{
    global $post;
//    $page_id = get_the_ID();
//    $post = get_page($page_id);
//    echo $post->post_content; exit;
    $pattern = "/\[webpart(.*?)\]/";

    $check = preg_match_all($pattern, $post->post_content, $matches);
    if ($check) {
        $layout = '';
        foreach ($matches[0] as $shortcode) {
            $attrs = parse_atts($shortcode);
            //$list = get_posts($attrs['category']);
            $list = new WP_Query(
                array( 'category_name' => $attrs['category'], 'posts_per_page' => $attrs['number'], 'post_type' => 'post')
            );
            if (!isset($list->posts)) continue;
            $list = $list->posts;
            $size_list = sizeof($list);
            $max_size = $size_list < $attrs['number'] ? $size_list : $attrs['number'];
            if ($attrs['type'] == "one-image" && $max_size > 2) {
                //if ($attrs['color'] == "red") {
                    $count = 1;
                    foreach ($list as $item) {
                        $tags = wp_get_post_tags($item->ID);
                        //var_dump(get_post_mime_type($item)); exit;
                        if ($count == 1) {
                            if ($attrs['color'] == "red") {
                                $layout .= '<div class="highlight highlight-inside">';
                            }
                            if ($attrs['color'] == "purple") {
                                $layout .= '<div class="highlight highlight-outside">';
                            }
                            if ($attrs['color'] == "yellow") {
                                $layout .= '<div class="highlight highlight-social">';
                            }
                            $layout .= '<h3 class="title"><a href="#">'.$attrs['title'].'</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
                                            <article class="art-block">
                                                <a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.get_the_post_thumbnail($item->ID, 'medium').'</a>
                                                <h4><a href="#">'.$item->post_title.'</a></h4>
                                                <p>'.$item->post_excerpt.'</p>';
                            if ($tags) {
                                $layout .= '<span class="note">';
                                $number_tag = sizeof($tags);
                                $count_tag = 1;
                                foreach ($tags as $tag) {
                                    if ($count_tag < $number_tag)
                                        $layout .= '<a href="#">'.$tag->name.'</a>, ';
                                    else
                                        $layout .= '<a href="#">'.$tag->name.'</a>';
                                    $count_tag++;
                                }
                                $layout .= '</span>';
                            }

                            $layout .=  '</article>';
                        }
                        if ($count == 2) {
                            $layout .= '<ul class="list-new">
                                            <li>
                                                <a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.$item->post_title.'</a>
                                                <ul class="tool-bar">
                                                    <li><span class="txt-cm"><i class="fa fa-comments"></i> '.$item->comment_count.' bình luận</span></li>
                                                </ul>
                                            </li>';
                        }
                        if ($count > 2 && $count < $max_size) {
                            $layout .= '<li>
                                            <a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.$item->post_title.'</a>
                                            <ul class="tool-bar">
                                                <li><span class="txt-cm"><i class="fa fa-comments"></i> '.$item->comment_count.' bình luận</span></li>
                                            </ul>
                                        </li>';
                        }
                        if ($count == $max_size) {
                            $layout .= '
                                <li>
                                        <a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.$item->post_title.'</a>
                                        <ul class="tool-bar">
                                            <li><span class="txt-cm"><i class="fa fa-comments"></i> '.$item->comment_count.' bình luận</span></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>';
                            break;
                        }
                        $count++;
                    }
            }

            if ($attrs['type'] == "slider" && $max_size > 2) {
                $count = 1;
                foreach ($list as $item) {
                    if ($count == 1) {
                        $layout .= '<div class="highlight highlight-hot">
                                        <h3 class="title"><a href="#">'.$attrs['title'].'</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
                                        <div class="flexslider">
                                            <div>
                                                <div class="inner-flexslider">
                                                    <a href="'.get_post_permalink($item->ID).'">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                                    <h4><a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.$item->post_title.'</a></h4>
                                                </div>
                                            </div>';
                    }
                    if ($count > 1 && $count < $size_list) {
                        $layout .= '
                                     <div>
                                        <div class="inner-flexslider">
                                            <a href="'.get_post_permalink($item->ID).'">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                            <h4><a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.$item->post_title.'</a></h4>
                                        </div>
                                     </div>
                                    ';
                    }
                    if ($count == $max_size) {
                        $layout .= '
                                             <div>
                                                <div class="inner-flexslider">
                                                    <a href="'.get_post_permalink($item->ID).'">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                                    <h4><a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.$item->post_title.'</a></h4>
                                                </div>
                                             </div>
                                        </div>
                                    </div>
                                    ';
                    }
                    $count++;
                }
            }

            if ($attrs['type'] == "five-images" && $max_size > 1) {
                $count = 1;
                foreach ($list as $item) {
                    if ($count == 1) {
                        $layout .= '
                                     <div class="highlight highlight-fun">
                                        <h3 class="title"><a href="#">'.$attrs['title'].'</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
                                            <article class="art-block-1">
                                                <a href="'.get_post_permalink($item->ID).'" title="'.$item->post_title.'">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                                <h4><a href="'.get_post_permalink($item->ID).'">'.$item->post_title.'</a></h4>
                                            </article>
                                    ';
                    }
                    if ($count == 2 && $count < $max_size) {
                        $layout .= '
                                    <div class="carousel-1">
                                        <div>
                                            <div class="inner-item">
                                                <a class="thumb" href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                                <h4><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a></h4>
                                            </div>
                                        </div>
                                ';
                    }
                    if ($count == 2 && $count == $max_size) {
                        $layout .= '
                                    <div class="carousel-1">
                                        <div>
                                            <div class="inner-item">
                                                <a class="thumb" href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                                <h4><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a></h4>
                                            </div>
                                        </div>
                                   </div>
                                </div>';
                    }
                    if ($count >2 && $count < $max_size) {
                        $layout .= '
                                    <div>
                                        <div class="inner-item">
                                            <a class="thumb" href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                            <h4><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a></h4>
                                        </div>
                                    </div>
                                ';
                    }
                    if ($count > 2 && $count == $max_size) {
                        $layout .= '
                                            <div>
                                                 <div class="inner-item">
                                                    <a class="thumb" href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'large').'</a>
                                                    <h4><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a></h4>
                                                </div>
                                            </div>
                                        </div>
                                </div>';
                    }
                    $count++;
                }
            }
        }
        echo $layout;
    }
}
add_action( 'webpart_shortcode', 'detect_shortcode_webpart' );

do_action('webpart_shortcode');




<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 31/05/2015
 * Time: 09:43
 */
function detect_shortcode_featured_news()
{
    global $post;
    $pattern = "/\[featured(.*?)\]/";
    if (preg_match_all($pattern, $post->post_content, $matches))
    {
        $attrs = parse_atts($matches[0][0]);
        if($attrs['page']='home') {
            $tin_noi_bat_1 = new WP_Query(
                array( 'category_name' => $attrs['categorytop'], 'posts_per_page' => $attrs['numbertop'], 'post_type' => 'post')
            );
            $tin_noi_bat_1 = $tin_noi_bat_1->posts;
            $tin_noi_bat_2 = new WP_Query(
                array( 'category_name' => $attrs['categorybottom'], 'posts_per_page' => $attrs['numberbottom'], 'post_type' => 'post')
            );
            $tin_noi_bat_2 = $tin_noi_bat_2->posts;
            $layout = '<div class="highlight highlight-top">';
            //Build tin bai noi bat 1
            $layout .= '<h3 class="title"><a href="javascript:void(0)">'.$attrs['titletop'].'</a></h3>';
            $layout .= '
                <article class="art-new art-feature">
                    <div class="inner-art">
                        <a href="'.get_post_permalink($tin_noi_bat_1[0]->ID).'" title="">'.get_the_post_thumbnail($tin_noi_bat_1[0]->ID, 'large').'</a>
                        <a href="'.get_post_permalink($tin_noi_bat_1[0]->ID).'" title="">'.$tin_noi_bat_1[0]->post_title.'</a>
                        <span class="txt-absolute txt-video">Video</span>
                    </div>
                    <h2><div><a href="'.get_post_permalink($tin_noi_bat_1[0]->ID).'" title="">'.$tin_noi_bat_1[0]->post_title.'</a><span class="txt-cm"><i class="fa fa-comments"></i> '.$tin_noi_bat_1[0]->comment_count.'</span></div></h2>
                </article>';
            //Finished tin bai noi bat 1
            $count = 1;
            $max_list_2 = sizeof($tin_noi_bat_2);
            if ($max_list_2 >= 4) {
                foreach($tin_noi_bat_2 as $item) {
                    if ($count == 1) $layout .= '<table class="tbl-1">';
                    if ($count % 2 == 1) {
                        $layout .= '
                            <tr>
                                <td>
                                    <article class="art-new">
                                        <div class="inner-art">
                                            <a href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'medium').'</a>
                                        </div>
                                        <h2><div><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a><span class="txt-cm"><i class="fa fa-comments"></i> '.$item->comment_count.'</span></div></h2>
                                    </article>
                                </td>';
                    } else {
                        $layout .= '
                                <td>
                                    <article class="art-new">
                                        <div class="inner-art">
                                            <a href="'.get_post_permalink($item->ID).'" title="">'.get_the_post_thumbnail($item->ID, 'medium').'</a>
                                        </div>
                                        <h2><div><a href="'.get_post_permalink($item->ID).'" title="">'.$item->post_title.'</a><span class="txt-cm"><i class="fa fa-comments"></i> '.$item->comment_count.'</span></div></h2>
                                    </article>
                                </td>
                            </tr>
                ';
                    }
                    if ($count == 4) {
                        $layout .= '</table>';
                        break;
                    }
                    $count++;

                }
            }
            $layout .= '</div>';
            echo $layout;
        }
        return;

    }
}
add_action( 'featured_news_shortcode', 'detect_shortcode_featured_news' );

do_action('featured_news_shortcode');
?>



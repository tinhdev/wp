	<?php 
	
	/**
	 *
	 * @package  123share
	 * @file     single.php
	 * @author   HoanVo
	 * @link 	 http://123share.net
	 */			
	
	get_header();?>
    <div class="container-fluid main">
        <div class="banner-top"><img alt="" src="<?php bloginfo('template_directory'); ?>/images/banner-5.jpg"></div>
        <div class="row">
            <section class="content col-md-8 col-sm-7">
                <ol class="breadcrumb">
                    <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                </ol>
                <?php
                    // Start the Loop.
                    while ( have_posts() ) : the_post();

                        /*
                         * Include the post format-specific template for the content. If you want to
                         * use this in a child theme, then include a file called called content-___.php
                         * (where ___ is the post format) and that will be used instead.
                         */
                        get_template_part( 'content', get_post_format() );

                        ?>
                <div class="highlight">

                <?php

                // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        }
                ?>
                </div>
                <?php
                    endwhile;
                ?>
            </section>
            <aside class="sidebar col-md-4 col-sm-5">
                <?php get_sidebar('content'); ?>
            </aside>
        </div>
    </div>
    <?php get_footer(); ?>
<?php
add_theme_support( 'post-thumbnails' );
//set_post_thumbnail_size( 672, 372, true );
//add_image_size( 'featured_home_top_img', 659, 363, true ); // top featured left img
//add_image_size( 'featured_home_bottom_img', 484, 273, true ); // bottom featured left img

function shortcode_featured_news_region($atts) {
    extract( shortcode_atts( array(
        'page' => '',
        'titletop' => '',
        'categorytop' => '4'
    ), $atts, 'featured' ) );

    //var_dump($typepage); exit;
}
add_shortcode( 'featured', 'shortcode_featured_news_region');

function shortcode_webpart_region($atts) {
    extract( shortcode_atts( array(
        'type' => '',
        'title' => '',
        'category' => '4'
    ), $atts, 'webpart' ) );

    //var_dump($typepage); exit;
}
add_shortcode( 'webpart', 'shortcode_webpart_region');


//function get_page_id_by_slug($slug){
//    global $wpdb;
//    $id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$slug."'AND post_type = 'page'");
//    return $id;
//}
add_theme_support( 'post-formats', array(
    'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
) );

function dump($args) {
    var_dump($args);
    exit;
}


function parse_atts( $content ) {
    preg_match_all( '/([^ ]*)=(\'([^\']*)\'|\"([^\"]*)\"|([^ ]*))/', trim( $content ), $c );
    list( $dummy, $keys, $values ) = array_values( $c );
    $c = array();
    foreach ( $keys as $key => $value ) {
        $value = trim( $values[ $key ], "\"'" );
        $type = is_numeric( $value ) ? 'int' : 'string';
        $type = in_array( strtolower( $value ), array( 'true', 'false' ) ) ? 'bool' : $type;
        switch ( $type ) {
            case 'int': $value = (int) $value; break;
            case 'bool': $value = strtolower( $value ) == 'true'; break;
        }
        $c[ $keys[ $key ] ] = $value;
    }
    return $c;
}

function getRecentPost($limit) {
    $args = array(
        'numberposts' => $limit,
        'offset' => 0,
        'category' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'post',
        'post_status' => 'publish',
        'suppress_filters' => true );

    return wp_get_recent_posts($args);

}

function getMostViewPost($limit) {
    $query = new WP_Query( array(
        'meta_key' => 'post_views_count',
        'orderby' => 'meta_value_num',
        'posts_per_page' => $limit
    ) );
    return $query->posts;
}

function getMostCommentPost($limit) {
    $query = new WP_Query('orderby=comment_count&posts_per_page='.$limit);
    return $query->posts;
}

function dzo_widgets_init() {
    require get_template_directory() . '/inc/widgets.php';
    register_widget( 'Sharemobile_Ephemera_Widget' );

    register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'sharemobile' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Main sidebar that appears on the left.', 'sharemobile' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Content Sidebar', 'sharemobile' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Additional sidebar that appears on the right.', 'sharemobile' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer Widget Area', 'sharemobile' ),
        'id'            => 'sidebar-3',
        'description'   => __( 'Appears in the footer section of the site.', 'sharemobile' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );
}
add_action( 'widgets_init', 'dzo_widgets_init' );

?>
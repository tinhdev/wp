<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 31/05/2015
 * Time: 09:31
 */
?>
<?php
$recent_posts = getRecentPost(5);
$most_view_post = getMostViewPost(5);
$most_comment_post = getMostCommentPost(5);
//dump($most_view_post);

?>
<div class="block-bar block-mostview">
    <h3><a href="#" title="">Xem nhiều nhất</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
    <ul>
        <?php $count = 1; ?>
        <?php foreach ($most_view_post as $item): ?>
            <?php if($count ==1): ?>
                <li>
                    <a class="thumb" href="#" title="<?php echo $item->post_title; ?>"><?php echo get_the_post_thumbnail($item->ID, 'medium'); ?><span class="number">1</span></a>
                    <h4><a href="<?php echo get_post_permalink($item->ID); ?>" title="<?php echo $item->post_title; ?>"><?php echo $item->post_title; ?></a> <span class="txt-cm"><i class="fa fa-comments"></i> <?php echo $item->comment_count; ?></span></h4>
                </li>
            <?php else: ?>
                <li>
                    <span class="number"><?php echo $count_+ 1; ?></span>
                    <h4><a href="<?php echo get_post_permalink($item->ID); ?>" title=""><?php echo $item->post_title; ?></a><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo $item->comment_count; ?></span></h4>
                </li>
            <?php endif; ?>
            <?php $count++; ?>

        <?php endforeach; ?>
    </ul>
</div>
<div class="block-bar">
    <h3><a href="#" title="">Mới nhất</a></h3>
    <ul class="list-new">
        <?php foreach ($recent_posts as $item): ?>
            <?php
                $cat = get_the_category($item['ID']);
                $cat_name = $cat[0]->name;
            ?>
            <li>
                <a href="<?php echo get_post_permalink($item['ID']); ?>" title=""><?php echo $item['post_title']; ?></a>
                <ul class="tool-bar list-inline">
                    <li><a href="#" title=""><?php echo $cat_name; ?></a></li>
                    <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo $item['comment_count']; ?> bình luận</span></li>
                </ul>
            </li>

        <?php endforeach;?>
    </ul>
</div>
<!--
<div class="block-bar">
    <h3><a href="#" title="">Share nhiều nhất</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
    <ul class="list-new-1">
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-6.jpg"></a>
            <h4><a href="" title="">Nông dân thuê... hợp tác xã làm ruộng</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <h4><a href="" title="">​Hải quân Việt Nam kỷ niệm 60 năm ngày thành lập</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><span class="txt-video">Video</span><img alt="" src="images/photo/img-7.jpg"></a>
            <h4><a href="" title="">Tuyển ứng viên đi học nước ngoài theo đề án Chính phủ</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><span class="txt-photo">photo</span><img alt="" src="images/photo/img-8.jpg"></a>
            <h4><a href="" title="">Giá sầu riêng đầu mùa giảm mạnh</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
        <li>
            <a class="thumb" href="#" title=""><img alt="" src="images/photo/img-9.jpg"></a>
            <h4><a href="" title="">Cầu mong đất nước luôn yên bình</a></h4>
            <ul class="tool-bar list-inline">
                <li><a href="#" title="">Chuyện thiên hạ</a></li>
                <li><span class="txt-cm"><i class="fa fa-comments"></i> 11 bình luận</span></li>
            </ul>
        </li>
    </ul>
</div>
<div class="block-bar">
    <img width="100%" alt="" src="images/qc.jpg">
</div>
-->
<div class="block-bar">
    <h3><a href="#" title="">Bình luận nhiều nhất</a> <a class="link-more" href="#"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a></h3>
    <ul class="list-new-1">
        <?php foreach ($most_comment_post as $item): ?>
            <?php
            $cat = get_the_category($item->ID);
            $cat_name = $cat[0]->name;
            ?>
            <li>
                <a class="thumb" href="<?php echo get_post_permalink($item->ID); ?>" title=""><img alt="" src="<?php echo get_the_post_thumbnail($item->ID, 'thumbnail'); ?>"></a>
                <h4><a href="<?php echo get_post_permalink($item->ID); ?>" title="<?php echo $item->post_title; ?>"><?php echo $item->post_title; ?></a></h4>
                <ul class="tool-bar list-inline">
                    <li><a href="#" title=""><?php echo $cat_name; ?></a></li>
                    <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo $item->comment_count; ?> bình luận</span></li>
                </ul>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<!--
<div class="block-bar">
    <img width="100%" alt="" src="images/fb-sample.jpg">
</div>
-->

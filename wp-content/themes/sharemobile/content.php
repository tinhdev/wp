<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div class="outer-tool">
    <span class="date pull-left">15.02.2015 GMT + 7</span>
    <ul class="tool-bar list-inline pull-right">
        <li><span class="txt-cm"><i class="fa fa-comments"></i> 11</span></li>
        <li><img alt="" src="<?php bloginfo('template_directory'); ?>/images/social-img.png"></li>
    </ul>
</div>
<article class="fck">
    <header>
        <?php if ( is_single() ) :
        the_title( '<h1 class="entry-title">', '</h1>' );
        else :
        the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
        endif;
        ?>
        <p class="bold"><?php the_excerpt(); ?></p>
    </header>
    <section>
        <?php
        /* translators: %s: Name of current post */
        the_content( sprintf(
            __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'dzomobile_old' ),
            the_title( '<span class="screen-reader-text">', '</span>', false )
        ) );

        wp_link_pages( array(
            'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'dzomobile_old' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
        ) );
        ?>
    </section>
    <footer>
        <p>P.T.H</p>
    </footer>
    <ul class="outer-tag list-inline">
        <li><a href="javascript:void(0)" title=""><i class="fa fa-tags"></i> Tags</a></li>
        <li><a href="#" title="">Nepal</a></li>
        <li><a href="#" title="">động đất</a></li>
        <li><a href="#" title="">tâm chấn</a></li>
        <li><a href="#" title="">Vũng Tàu</a></li>
        <li><a href="#" title="">Đà Lạt</a></li>
    </ul>
</article>



<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Dzo
 * @since Dzo 1.0
 * @autho Hoanvo <vvhoan2004@gmail.com>
 */
get_header(); ?>
    <div class="container">
        <div class="row">
             <?php
            // Start the Loop.
            while ( have_posts() ) : the_post();

                /*
                 * Include the post format-specific template for the content. If you want to
                 * use this in a child theme, then include a file called called content-___.php
                 * (where ___ is the post format) and that will be used instead.
                 */
                get_template_part( 'content', get_post_format() );

            endwhile;

wp_footer();
             get_sidebar( 'content' );

        ?>
            </div>
        </div>
    </div><!-- #container -->

<?php

get_footer();

<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
<div class="container">
    <div class="row">
        <section class="content col-sm-8">
            <ol class="breadcrumb">
                <li class="active">Tìm kiếm</li>
            </ol>
            <div class="highlight highlight-top">
                <div class="outer-search-1">
                    <?php
//                    global $query_string;
//
//                    $query_args = explode("&", $query_string);
//                    $search_query = array();
//
//                    foreach($query_args as $key => $string) {
//                        $query_split = explode("=", $string);
//                        $search_query[$query_split[0]] = urldecode($query_split[1]);
//                    } // foreach
//
//                    $search = new WP_Query($search_query);
                    ?>

                    <form class="form-inline frm-search-1" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="form-group">
                            <input type="text" name="s" id="s" class="form-control txt-tk" placeholder="Nội dung cần tìm">
                        </div>
                        <button type="submit" id="searchsubmit" class="btn btn-danger">Tìm kiếm</button>
                        <!--<div class="checkbox">
                            <label>
                                <input type="radio" name="type" value="1"> Tìm tất cả
                            </label>
                            <label>
                                <input type="radio" name="type" value="2"> Tìm theo tiêu đề
                            </label>
                            <label>
                                <input type="radio" name="type" value="3"> Tìm theo nội dung
                            </label>
                        </div>

                       <select class="form-control" name="category">
                            <option value='-1'>Chọn mục</option>
                            <?php
                                //$args = array('numberposts'=>99, 'child_of'=>2);
                                //$cats = get_categories($args);
                            //$cats = get_categories();
                            //foreach($cats as $cat) { ?>
                                <option value="<?php //echo $cat->slug; ?>"><?php// echo $cat->cat_name; ?></option>
                            <?php //} ?>
                        </select>

                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
                            </div>
                        </div>-->
                    </form>

                </div>
                <div class="highlight-ctn row">
                    <div id="custom_list">
                        <?php if ( have_posts() ) : ?>
                            <?php
                                // Start the Loop.
                                while ( have_posts() ) : the_post();
                                    $title_post = get_the_title();
                                    $excerpt = stringTrunc(get_the_excerpt(), 35);
                                    $format_post = get_post_format(get_the_ID());
                                    $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                                    //$tag = the_tags( '<a>', '</a>, <a>', '</a>' );
                                    ?>
                                    <article class="art">
                                        <a class="resize" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                            <?php echo echoSpan($format_post); ?>
                                        </a>
                                        <h2><a href="<?php echo the_permalink(); ?>" title=""><?php echo $title_post; ?></a></h2>
                                        <ul class="tool-bar list-inline">
                                            <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li> |
                                            <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                        </ul>
                                        <p><?php echo $excerpt; ?></p>
                                        <span class="note">
                                            <?php the_tags( '<a>', '</a>, <a>', '</a>' ); ?>
                                        </span>
                                    </article>
                            <?php  endwhile;

                            else :
                                // If no content, include the "No posts found" template.
                                get_template_part( 'content', 'none' );

                            endif;
                        ?>
                    </div>

                </div>
            </div>
        </section>
        <aside class="sidebar col-sm-4">
        <?php dynamic_sidebar( 'sidebar-3' ); ?>
        </aside>
    </div> <!-- #row -->
</div><!-- #container -->

<?php
get_footer();

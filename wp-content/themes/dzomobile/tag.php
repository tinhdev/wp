<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header();
?>
    <div class="container">
        <div class="row">
            <section class="content col-sm-8">
               <ol class="breadcrumb">
				  <li class="active">Tag</li>
				</ol>
				<p class="kq-tag">Kết quả từ khóa <span style="color:#00AC77;">'<?php echo single_tag_title( '', false ); ?>'</span></p>
                <div class="highlight-ctn row">
                    <div id="custom_list">
                        <?php if ( have_posts() ) : ?>
                            <?php
                            // Start the Loop.
                            while ( have_posts() ) : the_post();
                                $title_post = get_the_title();
                                $excerpt = stringTrunc(get_the_excerpt(), 35);
                                $format_post = get_post_format(get_the_ID());
                                $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail' );
                                //$tag = the_tags( '<a>', '</a>, <a>', '</a>' );
                                ?>
                                <article class="art">
                                    <a class="resize" href="<?php echo the_permalink(); ?>" title="<?php echo $title_post; ?>"><img style="background-image: url(<?php echo $url[0]; ?>)" alt="" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">
                                        <?php echo echoSpan($format_post); ?>
                                    </a>
                                    <h2><a href="<?php echo the_permalink(); ?>" title=""><?php echo $title_post; ?></a></h2>
                                    <ul class="tool-bar list-inline">
                                        <li><span class="txt-cm"><i class="fa fa-comments"></i> <?php echo get_comments_number(); ?> bình luận</span></li> |
                                        <li class="date"><?php echo the_time('j.m.Y'); ?> GMT + 7</li>
                                    </ul>
                                    <p><?php echo $excerpt; ?></p>
                                    <span class="note">
                                        <?php the_tags( '<a>', '</a>, <a>', '</a>' ); ?>
                                    </span>
                                </article>
                            <?php  endwhile;

                        else :
                            // If no content, include the "No posts found" template.
                            get_template_part( 'content', 'none' );

                        endif;
                        ?>
                    </div>
                </div>
            </section>
            <aside class="sidebar col-sm-4">
                <?php dynamic_sidebar( 'sidebar-3' ); ?>
            </aside>
        </div> <!-- #row -->
    </div><!-- #container -->

<?php
get_footer();

<?php
/**
 * The Content Sidebar
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

if ( ! is_active_sidebar( 'sidebar-3' ) ) {
	return;
}
?>
<aside class="sidebar col-sm-4">
	<?php dynamic_sidebar( 'sidebar-3' ); ?>
</aside>

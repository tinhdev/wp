<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
    <div class="container-fluid main">
        <!--<div class="banner-top"><img alt="" src="images/banner-5.jpg"></div> -->
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
        <div class="row">
            <section class="content col-md-8 col-sm-7">
                <?php dynamic_sidebar( 'sidebar-2' ); ?>
            </section>
            <aside class="sidebar col-md-4 col-sm-5">
                <?php dynamic_sidebar( 'sidebar-3' ); ?>
            </aside>

    </div><!-- #container -->
<?php
get_footer();

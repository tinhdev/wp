<?php
/**
 * dzopc functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link https://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage dzopc
 * @since dzopc 1.0
 */

/**
 * Set up the content width value based on the theme's design.
 *
 * @see twentyfourteen_content_width()
 *
 * @since Twenty Fourteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

if ( ! function_exists('dzo_setup') ) :
/**
 * Twenty Fourteen setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Twenty Fourteen 1.0
 */
function dzo_setup() {

	/*
	 * Make Twenty Fourteen available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Fourteen, use a find and
	 * replace to change 'twentyfourteen' to the name of your theme in all
	 * template files.
	 */
	//load_theme_textdomain( 'dzopc', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.
	//add_editor_style( array( 'css/editor-style.css', dzopc_font_url(), 'genericons/genericons.css' ) );

	// Add RSS feed links to <head> for posts and comments.
	//add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
//	set_post_thumbnail_size( 303, 170, true );
//    add_image_size( 'medium', 484, 273, true );
//    add_image_size( 'large', 659, 363, true );
	//add_image_size( 'twentyfourteen-full-width', 1038, 576, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'left'   => __( 'Menu trái', 'dzopc' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
//	add_theme_support( 'custom-background', apply_filters( 'dzopc_custom_background_args', array(
//		'default-color' => 'f5f5f5',
//	) ) );

	// Add support for featured content.
//	add_theme_support( 'featured-content', array(
//		'featured_content_filter' => 'dzopc_get_featured_posts',
//		'max_posts' => 6,
//	) );

	// This theme uses its own gallery styles.
	//add_filter( 'use_default_gallery_style', '__return_false' );
}
endif; // dzo_setup
add_action( 'after_setup_theme', 'dzo_setup' );

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Twenty Fourteen 1.0
 */
//function dzopc_content_width() {
//	if ( is_attachment() && wp_attachment_is_image() ) {
//		$GLOBALS['content_width'] = 810;
//	}
//}
//add_action( 'template_redirect', 'dzopc_content_width' );

/**
 * Getter function for Featured Content Plugin.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return array An array of WP_Post objects.
 */
//function dzopc_get_featured_posts() {
//	/**
//	 * Filter the featured posts to return in Twenty Fourteen.
//	 *
//	 * @since Twenty Fourteen 1.0
//	 *
//	 * @param array|bool $posts Array of featured posts, otherwise false.
//	 */
//	return apply_filters( 'dzopc_get_featured_posts', array() );
//}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return bool Whether there are featured posts.
 */
//function dzopc_has_featured_posts() {
//	return ! is_paged() && (bool) dzopc_get_featured_posts();
//}

/**
 * Register three Twenty Fourteen widget areas.
 *
 * @since Twenty Fourteen 1.0
 */
function dzo_widgets_init() {
	//require get_template_directory() . '/inc/widgets.php';
	//register_widget( 'Twenty_Fourteen_Ephemera_Widget' );
    require get_template_directory() . '/inc/dzo_featured_widget.php';
    register_widget( 'Dzo_featured_widget' );
    require get_template_directory() . '/inc/dzo_webpart_widget.php';
    register_widget( 'Dzo_webpart_widget' );
    require get_template_directory() . '/inc/dzo_most_comment_widget.php';
//    register_widget( 'dzo_most_comment_widget' );
//    require get_template_directory() . '/inc/dzo_most_viewed_widget.php';
//    register_widget( 'dzo_most_viewed_widget' );
//    require get_template_directory() . '/inc/dzo_customlist_widget.php';
//    register_widget( 'dzo_customlist_widget' );

    register_sidebar( array(
		'name'          => __( 'Top Region', 'dzopc' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Hiển thị dữ liệu top.', 'dzopc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Left Region', 'dzopc' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Hiển thị dữ liệu bên trái.', 'dzopc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Right Region', 'dzopc' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Hiển thị dữ liệu cột phải.', 'dzopc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'dzo_widgets_init' );

/**
 * Register Lato Google font for Twenty Fourteen.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return string
 */
//function dzopc_font_url() {
//	$font_url = '';
//	/*
//	 * Translators: If there are characters in your language that are not supported
//	 * by Lato, translate this to 'off'. Do not translate into your own language.
//	 */
//	if ( 'off' !== _x( 'on', 'Lato font: on or off', 'dzopc' ) ) {
//		$query_args = array(
//			'family' => urlencode( 'Lato:300,400,700,900,300italic,400italic,700italic' ),
//			'subset' => urlencode( 'latin,latin-ext' ),
//		);
//		$font_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
//	}
//
//	return $font_url;
//}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Fourteen 1.0
 */
function dzo_scripts() {
	// Add Lato font, used in the main stylesheet.
	//wp_enqueue_style( 'dzopc-lato', dzopc_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	//wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.0.3' );

	// Load our main stylesheet.
	//wp_enqueue_style( 'dzopc-style', get_stylesheet_uri() );
    //wp_enqueue_script('disqus_embed','https://a.disquscdn.com/embed.js');

	// Load the Internet Explorer specific stylesheet.
	//wp_enqueue_style( 'dzopc-ie', get_template_directory_uri() . '/css/ie.css', array( 'dzopc-style' ), '20131205' );
	//wp_style_add_data( 'dzopc-ie', 'conditional', 'lt IE 9' );

//	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
//		wp_enqueue_script( 'comment-reply' );
//	}

//	if ( is_singular() && wp_attachment_is_image() ) {
//		wp_enqueue_script( 'twentyfourteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
//	}

//	if ( is_active_sidebar( 'sidebar-3' ) ) {
//		wp_enqueue_script( 'jquery-masonry' );
//	}

//	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
//		wp_enqueue_script( 'dzopc-slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), '20131205', true );
//		wp_localize_script( 'dzopc-slider', 'featuredSliderDefaults', array(
//			'prevText' => __( 'Previous', 'dzopc' ),
//			'nextText' => __( 'Next', 'dzopc' )
//		) );
//	}

	//wp_enqueue_script( 'twentyfourteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150315', true );
   // wp_enqueue_script( 'dzo_ajax.js', get_template_directory_uri() . "/js/dzo_ajax.js", array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'dzo_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Twenty Fourteen 1.0
 */
function dzopc_body_classes($classes) {

    global $wp_query;
    $temp = 'page-detail';
    if( !is_404() && !is_front_page() ){
        $post_id = $wp_query->post->ID;

        if(is_single($post_id) || is_search()){

           // $classes[] = get_post_meta($post_id, 'body_class', true);
            $temp = 'page-detail';

        }else{
            if (!isset($_GET['r'])) return;
            else $slug = explode('/', $_GET['r']);
            $size_slug = sizeof($slug);
            $cat_slug = isset($slug[$size_slug-1]) ? $slug[$size_slug-1] : '';
            if ($cat_slug == "nong-tung-centimet" || $cat_slug == "cuoc-song-quanh-ta") $temp = 'page category-hot';
            if ($cat_slug == "cuoi-chut-choi") $temp = 'page category-fun';
            if ($cat_slug == "chuyen-thien-ha" || $cat_slug == "the-gioi-muon-mau") $temp = 'page category-outside';
            if ($cat_slug == "cong-dong-mang") $temp = 'page category-network';
            if ($cat_slug == "chuyen-trong-nha") $temp = 'page category-inside';
        }


    }

    if (is_front_page()) {
        $temp = 'page-home';
    }

    return array($temp);// return the $classes array

}

add_filter('body_class','dzopc_body_classes');
/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
//function dzopc_post_classes( $classes ) {
//	if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
//		$classes[] = 'has-post-thumbnail';
//	}
//
//	return $classes;
//}
//add_filter( 'post_class', 'dzopc_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function dzopc_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'dzopc' ), max( $paged, $page ) );
	}

	return $title;
}
//add_filter( 'wp_title', 'dzopc_wp_title', 10, 2 );

// Implement Custom Header features.
//require get_template_directory() . '/inc/custom-header_old.php';

// Custom template tags for this theme.
//require get_template_directory() . '/inc/template-tags.php';

// Add Customizer functionality.
//require get_template_directory() . '/inc/customizer.php';


/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
//if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
//	require get_template_directory() . '/inc/featured-content.php';
//}

function getPostByCatAndStick($catname, $limit, $stick=false) {
    if ($stick == true) {
        $args = array(
            'category_name' => $catname,
            'post__in' => get_option( 'sticky_posts'),
            'showposts' => $limit
        );
    } else {
        $args = array(
            'category_name' => $catname,
            'showposts' => $limit
        );
    }
    $the_query = new WP_Query( $args );
    return $the_query;
}

function dump($str){
    var_dump($str);
    exit;
}

function stringTrunc($phrase, $max_words) {
    $phrase_array = explode(' ',$phrase);
    if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
    return $phrase;
}

//Add field nick name
add_action( 'admin_menu', 'my_create_post_nickname_box' );
add_action( 'save_post', 'my_save_post_nickname_box', 10, 2 );

function my_create_post_nickname_box() {
    add_meta_box( 'my-meta-box', 'Nick name', 'my_post_nickname_box', 'post', 'normal', 'high' );
}

function my_post_nickname_box( $object) { ?>
    <p>
        <input type="text" name="nick-name" id="nick-name" style="width: 50%;" value="<?php echo wp_specialchars( get_post_meta( $object->ID, 'nick-name', true ), 1 ); ?>">
        <input type="hidden" name="my_meta_nickname_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
    </p>
<?php }

function my_save_post_nickname_box( $post_id) {

    if (!isset($_POST['my_meta_nickname_nonce']) || !wp_verify_nonce( $_POST['my_meta_nickname_nonce'], plugin_basename( __FILE__ ) ) )
        return $post_id;

    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    $meta_value = get_post_meta( $post_id, 'nick-name', true );
    $new_meta_value = stripslashes( $_POST['nick-name'] );

    if ( $new_meta_value && '' == $meta_value )
        add_post_meta( $post_id, 'nick-name', $new_meta_value, true );

    elseif ( $new_meta_value != $meta_value )
        update_post_meta( $post_id, 'nick-name', $new_meta_value );

    elseif ( '' == $new_meta_value && $meta_value )
        delete_post_meta( $post_id, 'nick-name', $meta_value );
}

//Add file original link
add_action( 'admin_menu', 'my_create_post_original_link_box' );
add_action( 'save_post', 'my_save_post_original_link_box' );

function my_create_post_original_link_box() {
    add_meta_box( 'my-original_link-box', 'Original Link', 'my_post_original_link_box', 'post', 'normal', 'high' );
}

function my_post_original_link_box( $object) { ?>
    <p>
        <input type="text" name="original-link" id="nick-name" style="width: 50%;" value="<?php echo wp_specialchars( get_post_meta( $object->ID, 'original-link', true ), 1 ); ?>">
        <input type="hidden" name="my_original_link_box_nonce" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />
    </p>
<?php }

function my_save_post_original_link_box( $post_id) {

    if ( !isset($_POST['my_original_link_box_nonce']) || !wp_verify_nonce( $_POST['my_original_link_box_nonce'], plugin_basename( __FILE__ ) ) )
        return $post_id;

    if ( !current_user_can( 'edit_post', $post_id ) )
        return $post_id;

    $meta_value = get_post_meta( $post_id, 'original-link', true );
    $new_meta_value = stripslashes( $_POST['original-link'] );

    if ( $new_meta_value && '' == $meta_value )
        add_post_meta( $post_id, 'original-link', $new_meta_value, true );

    elseif ( $new_meta_value != $meta_value )
        update_post_meta( $post_id, 'original-link', $new_meta_value );

    elseif ( '' == $new_meta_value && $meta_value )
        delete_post_meta( $post_id, 'original-link', $meta_value );
}

//Disqus comment
//add_action( 'disqus_comment', 'disqus_embed' );
//function disqus_embed($disqus_shortname) {
//    global $post;
//    //wp_enqueue_script('disqus_embed','https://a.disquscdn.com/embed.js');
//    echo '<div id="disqus_thread"></div>
//    <script type="text/javascript">
//        var disqus_shortname = "'.$disqus_shortname.'";
//        var disqus_title = "'.$post->post_title.'";
//        var disqus_url = "'.get_permalink($post->ID).'";
//        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
//    </script>';
//}

function getRecentPost($limit) {
    $args = array(
        'numberposts' => $limit,
        'offset' => 0,
        'category' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'post',
        'post_status' => 'publish',
        'suppress_filters' => true );

    return wp_get_recent_posts($args);

}


//function getMostViewPost($limit) {
//    $query = new WP_Query( array(
//        'meta_key' => 'post_views_count',
//        'orderby' => 'meta_value_num',
//        'posts_per_page' => $limit
//    ) );
//    return $query->posts;
//}

//function popularPosts($num) {
//    global $wpdb;
//    $posts = $wpdb->get_results("SELECT comment_count, ID, post_title FROM $wpdb->posts ORDER BY comment_count DESC LIMIT 0 , $num");
//    return $posts;
//}
function the_breadcrumb () {

    // Settings
    $separator  = '&gt;';
    $id         = 'breadcrumbs';
    $class      = 'breadcrumbs';
    $home_title = 'Trang chủ';

    // Get the query & post information
    global $post,$wp_query;
    $category = get_the_category();

    // Build the breadcrums

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Home page
        echo '<li><a href="<?php echo get_site_url(); ?>" title="' . $home_title . '">' . $home_title . '</a></li>';
        if ( is_single() ) {
            echo '<li><a class="active" href="' . get_category_link($category[0]->term_id ) . '">' . $category[0]->cat_name . '</a></li>';
        } else if ( is_category() ) {

            // Category page
            //echo '<li class="item-current item-cat-' . $category[0]->term_id . ' item-cat-' . $category[0]->category_nicename . '"><strong class="bread-current bread-cat-' . $category[0]->term_id . ' bread-cat-' . $category[0]->category_nicename . '">' . $category[0]->cat_name . '</strong></li>';

        } else if ( is_page() ) {

            // Standard page
//            if( $post->post_parent ){
//
//                // If child page, get parents
//                $anc = get_post_ancestors( $post->ID );
//
//                // Get parents in the right order
//                $anc = array_reverse($anc);
//
//                $parents = '';
//                // Parent page loop
//                foreach ( $anc as $ancestor ) {
//                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
//                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
//                }
//
//                // Display parent pages
//                echo $parents;
//
//                // Current page
//                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
//
//            } else {
//
//                // Just display current page if not parents
//                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
//
//            }

        } else if ( is_tag() ) {

            // Tag page

            // Get tag information
//            $term_id = get_query_var('tag_id');
//            $taxonomy = 'post_tag';
//            $args ='include=' . $term_id;
//            $terms = get_terms( $taxonomy, $args );
//
//            // Display the tag name
//            echo '<li class="item-current item-tag-' . $terms[0]->term_id . ' item-tag-' . $terms[0]->slug . '"><strong class="bread-current bread-tag-' . $terms[0]->term_id . ' bread-tag-' . $terms[0]->slug . '">' . $terms[0]->name . '</strong></li>';

        } elseif ( is_day() ) {

            // Day archive

            // Year link
//            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
//            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
//
//            // Month link
//            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
//            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
//
//            // Day display
//            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_month() ) {

            // Month Archive

            // Year link
//            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
//            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
//
//            // Month display
//            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_year() ) {

            // Display year archive
           // echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

        } else if ( is_author() ) {

            // Auhor archive

            // Get the author information
//            global $author;
//            $userdata = get_userdata( $author );
//
//            // Display author name
//            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

        } else if ( get_query_var('paged') ) {

            // Paginated archives
           // echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

        } else if ( is_search() ) {

            // Search results page
            //echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';

        } elseif ( is_404() ) {

            // 404 page
           // echo '<li>' . 'Error 404' . '</li>';
        }

    }

}

//function my_get_posts_for_pagination() {
//    $paged = $_GET['page']; // Page number
//    $html = '';
//    $pag = 0;
//    if( filter_var( intval( $paged ), FILTER_VALIDATE_INT ) ) {
//        $pag = $paged;
//        $args = array(
//            'paged' => $pag, // Uses the page number passed via AJAX
//            'posts_per_page' => 2 // Change this as you wish
//        );
//        $loop = new WP_Query( $args );
//
//        if( $loop->have_posts() ) {
//            while( $loop->have_posts() ) {
//                $loop->the_post();
//                // Build the HTML string with your post's contents
//            }
//
//            wp_reset_query();
//        }
//    }
//
//    echo $html;
//    exit();
//
//}
//
//add_action( 'wp_ajax_my_pagination', 'my_get_posts_for_pagination' );
//add_action( 'wp_ajax_nopriv_my_pagination', 'my_get_posts_for_pagination' );

function MyAjaxFunction(){
    $paged = $_POST['page_number'];
    $cat_slug = $_POST['cat'];
    $number = $_POST['limit'];
    $query_args = array(
        'post_type' => 'post',
        'category_name' =>  $cat_slug,
        'posts_per_page' => $number,
        'paged' => $paged
    );
    $the_query = new WP_Query( $query_args );
    if ( $the_query->have_posts() ) {
        $result_return = '<div id="custom_list">';
        $result_return .= '<input type="hidden" value="'.$cat_slug.'" class="cat_name"/>';
        $result_return .= '<input type="hidden" value="'.$number.'" class="page_limit"/>';
        while ($the_query->have_posts()) : $the_query->the_post(); // run the loop
            $link_post = get_permalink();
            $title_post = get_the_title();
            $tags = wp_get_post_tags(get_the_ID());
            $tags_tag_html = '';
            $size_tags = sizeof($tags);
            $counter = 1;
            foreach($tags as $tag) {
                if($counter < $size_tags)
                    $tags_tag_html .= '<a href="'.get_tag_link($tag->term_id).'"">'.$tag->name.'</a>, ';
                else
                    $tags_tag_html .= '<a href="'.get_tag_link($tag->term_id).'"">'.$tag->name.'</a>';

                $counter++;
            }
            $excerpt = stringTrunc(get_the_excerpt(), 35);
            $format_post = get_post_format(get_the_ID());
            $url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
            $date = get_the_time("j.m.Y");
            $result_return .= '<article class="art">';
            $result_return .= '<a class="resize" href="' . $link_post . '" title="'.$title_post.'"><img style="background-image: url('.$url[0].')" alt="" src="'.get_template_directory_uri().'/images/transparent.png">';
            $result_return .= echoSpan($format_post);
            $result_return .= '</a>';
            $result_return .= '<h2><a href="'.$link_post.'" title="' . $title_post . '">' . $title_post . '</a></h2>';
            $result_return .= '<ul class="tool-bar list-inline">';
            $result_return .= '<li><span class="txt-cm"><i class="fa fa-comments"></i>' . get_comments_number() . ' bình luận</span></li> |';
            $result_return .= '<li class="date">'.$date.' GMT + 7</li>';
            $result_return .= '</ul>';
            $result_return .= '<p>' . $excerpt . '</p>';
            $result_return .= '<span class="note">'.$tags_tag_html.'</span>';
            $result_return .= '</article>';
        endwhile;
        $result_return .= '</div>';

        //wp_reset_query();
        echo $result_return;
    } else {
        echo 0;
    }
}
// creating Ajax call for WordPress
//add_action( 'wp_ajax_nopriv_MyAjaxFunction', 'MyAjaxFunction' );
add_action( 'wp_ajax_MyAjaxFunction', 'MyAjaxFunction' );
function echoSpan($type) {
    if ($type == "video") {
        return '<span class="txt-absolute txt-video">Video</span>';
    }
    if ($type == "image") {
        return '<span class="txt-absolute txt-photo">Photo</span>';
    }
    return '';
}

function echoImg($link, $title) {
    echo '<img style="background-image: url('.$link.')" alt="'.$title.'" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png">';
}

function dzo_makeUrl($slug) {
    return get_home_url().'/tin-tuc/'.$slug;
}

// advanced search functionality
//function advanced_search_query($query) {
//
//    if($query->is_search()) {
//        // category terms search.
//        if (isset($_GET['category']) && !empty($_GET['category'])) {
//            $query->set('cat', array(array(
//                'field' => 'slug',
//                'terms' => array($_GET['category']) )
//            ));
//        }
//        return $query;
//    }
//}
//add_action('pre_get_posts', 'advanced_search_query', 1000);

function rewrite_link_post_vedette($link, $slug) {
    return str_replace("vedette",$slug,$link);
}
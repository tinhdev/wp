<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 07/06/2015
 * Time: 22:12
 */
?>
<footer class="footer">
    <div class="container-fluid">
        <ul class="row">
            <li class="col-sm-3 col-xs-6">
                <dl>
                    <dt><a href="home.html">TRANG CHỦ</a></dt>
                    <dd><a href="category-chuyentrongnha.html">CHUYỆN TRONG NHÀ</a></dd>
                    <dd><a href="category-chuyenthienha.html">CHUYỆN THIÊN HẠ</a></dd>
                    <dd><a href="category-congdongmang.html">CỘNG ĐỒNG MẠNG</a></dd>
                    <dd><a href="category-tungcentimet.html">ĐẸP TỪNG CENTIMET</a></dd>
                    <dd><a href="category-cuoichutchoi.html">CƯỜI CHÚT CHƠI</a></dd>
                </dl>
            </li>
            <li class="col-sm-3 col-xs-6">
                <dl>
                    <dt><a href="#">DANH SÁCH</a></dt>
                    <dd><a href="#">ĐỌC NHIỀU NHẤT</a></dd>
                    <dd><a href="#">SHARE NHIỀU NHẤT</a></dd>
                    <dd><a href="#">BÌNH LUẬN NHIỀU NHẤT</a></dd>
                    <dd><a href="#">XEM THEO NGÀY</a></dd>
                </dl>
            </li>
            <li class="col-sm-3 col-xs-6">
                <dl>
                    <dt><a href="#">LIÊN HỆ</a></dt>
                    <dd><a href="#">EMAIL: hvthe01@gmail.com</a></dd>
                    <dd><a href="#">SKYPE: abc_zxy</a></dd>
                    <dd><a href="#">PHONE: 0965.302.302</a></dd>
                    <dd><a href="#">ĐỊA CHỈ: Cty TNHH "Gặp là dzô"</a></dd>
                </dl>
            </li>
            <li class="col-sm-3 col-xs-6">
                <img alt="" src="<?php echo get_template_directory_uri(); ?>/images/social-img.png">
            </li>
        </ul>
    </div>
</footer>
<div class="menu-main">
    <ul>
        <li><a href="home.html" title=""><i class="fa fa-home"></i>Tổng quan</a></li>
        <li><a href="category-chuyentrongnha.html" title=""><i class="fa fa-beer"></i>Chuyện trong nhà</a></li>
        <li><a href="category-chuyenthienha.html" title=""><i class="fa fa-globe"></i>Chuyện thiên hạ</a></li>
        <li><a href="category-congdongmang.html" title=""><i class="fa fa-users"></i>Cộng đồng mạng</a></li>
        <li><a href="category-tungcentimet.html" title=""><i class="fa fa-sun-o"></i>Nóng từng centimet</a></li>
        <li><a href="category-cuoichutchoi.html" title=""><i class="fa fa-smile-o"></i>Cười chút chơi</a></li>
    </ul>
    <ul>
        <li><a href="#" title=""><i class="fa fa-eye"></i>Xem nhiều nhất</a></li>
        <li><a href="#" title=""><i class="fa fa-share-square"></i>Share nhiều nhất</a></li>
        <li><a href="#" title=""><i class="fa fa-comments"></i>Bình luận nhiều</a></li>
        <li><a href="#" title=""><i class="fa fa-calendar"></i>Xem theo ngày</a></li>
        <li class="social-img"><a href="#"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/social.jpg"></a></li>
        <li class="sec"><a href="#"><i class="fa fa-envelope-o"></i>Liên hệ qua email</a></li>
        <li class="sec"><a href="#"><i class="fa fa-phone"></i> 0964.201.810</a></li>
    </ul>
    <p>® 2015 Blog tin tức tổng hợp<br><b>123dzo.net</b></p>
</div>
<a class="btn-top" href="#" title="top"><i class="fa fa-chevron-up"></i></a>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/start.js"></script>
</body>
</html>

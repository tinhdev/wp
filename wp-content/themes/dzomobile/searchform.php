<?php
/**
 * Created by PhpStorm.
 * User: hoanvo
 * Date: 12/06/2015
 * Time: 09:23
 */
?>
<form class="frm-search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="text" class="txt-search" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'dzopc' ); ?>" />
    <button id="searchsubmit"  class="btn-search"><i class="fa fa-search"></i></button>
</form>

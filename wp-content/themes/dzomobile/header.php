<?php
/**
 * Created by PhpStorm.
 * User: HoanVo
 * Date: 07/06/2015
 * Time: 22:09
 */
?>
<!DOCTYPE HTML>
<!--[if IE 9]><html lang="en" class="ie9"></html><![endif]-->
<!-- [if gt IE 9] <!-->
<html lang="en">
<!-- <![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php bloginfo( 'name' ); ?></title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="index, follow" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/slick.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/screen.css" rel="stylesheet">
</head>
<body>
<header class="header">
    <div class="container-fluid">
        <nav class="nav-main">
            <ul class="">
                <li><a class="btn-action" id="btn-menu" href="javascript:void(0)" title=""><i class="fa fa-th-list"></i></a></li>
                <li class="logo"><a href="<?php echo get_site_url(); ?>" title=""><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a></li>
                <li class="search"><a class="btn-searchsg" href="javascript:void(0)" title=""><i class="fa fa-search"></i></a>
                    <?php //get_search_form(); ?>
                    <form class="frm-search">
                        <div class="form-group">
                            <input type="text" name="" id="" class="form-control" placeholder="Tìm kiếm...">
                        </div>
                    </form>
                </li>
                <li class="btn-send-mail"><a href="#" title=""><i class="fa fa-envelope-o"></i></a></li>
            </ul>
        </nav>
    </div>
</header>

<?php
/**
 * @name        :   Core_Articles
 * @author      :   PhongTX
 * @copyright   :   Fpt Online
 * Date: 24/12/14
 * Time: 08:45
 */
class Core_Articles
{
    /**
     * PhongTX
     * @param $strLink
     * @return array
     */
    public static function crawler($strLink){
        try{
            /**
             * Response format
             */
            $response = array(
                'status' => 1,
                'data' => array(
                    'title' => '',
                    'lead' => '',
                    'content' => ''
                )
            );
            if($strLink != '') {
                require_once ABSPATH . 'wp-admin/includes/phpQuery/phpQuery/phpQuery.php';
                require_once ABSPATH . 'wp-admin/includes/Filter/Filter.php';
                $arrLink = explode('/', $strLink);
                if (strpos($arrLink[2],'vnexpress.net') !== false) {
                    $htmlContent = phpQuery::newDocumentFileHTML($strLink, $charset = 'utf-8');
                    // Get title
                    $title = pq('div.main_content_detail div.title_news h1', $htmlContent);
                    $title = str_ireplace('</h1>','',str_ireplace('<h1>','',$title));
                    //Get Description
                    $desc = pq('div.main_content_detail div.short_intro', $htmlContent);
                    $desc = str_ireplace('</div>','',str_ireplace('<div class="short_intro txt_666">','',$desc));
                    if($arrLink[3] == 'photo'){
                        // get main content
                        $content = pq('div.main_content_detail #article_content', $htmlContent);
                    }else{
                        // get main content
                        $content = pq('div.main_content_detail div.fck_detail', $htmlContent);
                    }
                    // remove script tags
                    pq('script', $content)->remove();
                } else if(strpos($arrLink[2],'forum.diadiemanuong.com') !== false) {
                    $htmlContent = phpQuery::newDocumentFileHTML($strLink, $charset = 'utf-8');
                    // Get title
                    $title = pq('.postbody .postrow h2.posttitle span', $htmlContent);
                    $title = str_ireplace('</h1>','',str_ireplace('<h1>','',$title));
                    //Get Description
                    $desc = '';//pq('div.detailCT p.lead', $htmlContent);
                    $desc = str_ireplace('</div>','',str_ireplace('<div class="short_intro txt_666">','',$desc));
                    // get main content
                    $content = pq('.postbody .postrow .content', $htmlContent);
                    // remove script tags
                    pq('script', $content)->remove();
                } else if(strpos($arrLink[2],'diadiemanuong.com') !== false) {
                    $htmlContent = phpQuery::newDocumentFileHTML($strLink, $charset = 'utf-8');
                    // Get title
                    $title = pq('#recipe-detail h3', $htmlContent);
                    $title = str_ireplace('</h1>','',str_ireplace('<h1>','',$title));
                    //Get Description
                    $desc = '';//pq('div.detailCT p.lead', $htmlContent);
                    $desc = str_ireplace('</div>','',str_ireplace('<div class="short_intro txt_666">','',$desc));
                    // get main content
                    $content = pq('#recipe-detail #body_editor', $htmlContent);
                    // remove script tags
                    pq('script', $content)->remove();
                } else{
                    switch ($arrLink[2]) {
                        case 'ngoisao.net':
                            $htmlContent = phpQuery::newDocumentFileHTML($strLink, $charset = 'utf-8');
                            // Get title
                            $title = pq('div.detailCT h1.title', $htmlContent);
                            $title = str_ireplace('</h1>','',str_ireplace('<h1>','',$title));
                            //Get Description
                            $desc = pq('div.detailCT p.lead', $htmlContent);
                            $desc = str_ireplace('</div>','',str_ireplace('<div class="short_intro txt_666">','',$desc));
                            // get main content
                            $content = pq('div.detailCT div.fck_detail', $htmlContent);
                            // remove script tags
                            pq('script', $content)->remove();
                            break;
                        case 'foody.vn':
                            $htmlContent = phpQuery::newDocumentFileHTML($strLink, $charset = 'utf-8');
                            // Get title
                            $title = pq('div.blog-area-left h1.blog-title', $htmlContent);
                            $title = str_ireplace('</h1>','',str_ireplace('<h1>','',$title));
                            //Get Description
                            $desc = '';//pq('div.detailCT p.lead', $htmlContent);
                            $desc = str_ireplace('</div>','',str_ireplace('<div class="short_intro txt_666">','',$desc));
                            // get main content
                            $content = pq('div.blog-area-left div.content', $htmlContent);
                            // remove script tags
                            pq('script', $content)->remove();
                            break;
                        case 'ivivu.com':
                            $htmlContent = phpQuery::newDocumentFileHTML($strLink, $charset = 'utf-8');
                            // Get title
                            $title = pq('#content h1.entry-title', $htmlContent);
                            $title = str_ireplace('</h1>','',str_ireplace('<h1>','',$title));
                            $title = utf8_decode($title);
                            //Get Description
                            $desc = '';//pq('div.detailCT p.lead', $htmlContent);
                            $desc = str_ireplace('</div>','',str_ireplace('<div class="short_intro txt_666">','',$desc));
                            // get main content
                            $content = pq('#content div.entry-content', $htmlContent);
                            // remove script tags
                            pq('script', $content)->remove();
                            $content = utf8_decode($content);
                            $content = preg_replace('#</iframe>#siU', '', preg_replace('#<iframe.*>#siU', '', $content));
                            $content = preg_replace('/<div class="ltt-contentbox white">([\\S\\s]*?)<\/div>/','',$content);
                            break;
                    }
                }
            }
            $response['data']['title'] = Core_Filter::purify(trim(strip_tags($title)));
            $response['data']['lead'] = Core_Filter::purify(trim(strip_tags($desc)));
            $response['data']['content'] = Core_Filter::purify($content);
            //Remove href
            $response['data']['content'] = preg_replace('#</a>#siU', '', preg_replace('#<a.*>#siU', '', $response['data']['content']));
        }catch (Exception $ex){

        }
        return $response;
    }
}
?>
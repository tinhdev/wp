<?php
$id=(isset($_REQUEST['id']) and $_REQUEST['id'])?$_REQUEST['id']:0;
$site=DB::fetch('select * from site where id='.$id);
$site_structure=DB::fetch_all('select *,field_name as id from site_structure where site_id='.$id);
if( (isset($_REQUEST['action']) and $_REQUEST['action']=='save_structure') || $cmd=='del-template') {
    Feed::save_site_structure($cmd,$id,$site);
}
?>
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: PAGE TITLE & BREADCRUMB -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li class="active">
                    Quản lý mẫu lấy tin tự động
                </li>
            </ol>
            <div class="page-header">
                <div class="col-md-8">
                    <h1>Chi tiết các trường thông tin của mẫu:<br><span class="require"><?php echo $site['name'];?></span></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="javascript:void(0)" onclick="save_site_structure();" class="btn btn-primary" data-placement="top" data-toggle="tooltip" title="Ghi lại">
                        <i class="icon-save"></i> Ghi lại</a>
                    <?php if (isset($site_structure) && $site_structure) { ?>
                        <a href="declaration_site.php?cmd=del-template&id=<?php echo $id;?>" class="btn btn-danger" data-placement="top" data-toggle="tooltip" title="Không lấy nội dung tin">
                            <i class="icon-remove"></i> Xóa Chi tiết</a>
                    <?php } ?>
                </div>
                <div style="clear: both;"></div>
            </div>
            <!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
    </div>
    <!-- end: PAGE HEADER -->

    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-content">
                <form name="EditDeclarationSite" id="EditDeclarationSite" method="post">
                    <div style="margin-bottom:10px;">
                    <label>Thay thế đường dẫn ảnh trong nội dung:</label>
                    <input name="image_content_left" type="text" id="image_content_left" style="width:35%;" value='<?php echo isset($site['image_content_left'])?$site['image_content_left']:'';?>'  /> ==>
                    <input name="image_content_right" type="text" id="image_content_right" style="width:35%;" value='<?php echo isset($site['image_content_right'])?$site['image_content_right']:'';?>'  />
                    </div>
                    <table width="100%" cellpadding="5" cellspacing="0" border="1" style="border-collapse:collapse" bordercolor="#cccccc">
                        <tr bgcolor="#efefef">
                            <th>Trường dữ liệu</th>
                            <th>Mẫu cần lấy</th>
                            <th>Mẫu đối tượng cần xóa</th>
                        </tr>
                        <tr>
                            <td>name</td>
                            <td><input name="field[name][extra]" type="text" id="name_extra" style="width:90%;" value="<?php echo isset($site_structure['name']['extra'])?$site_structure['name']['extra']:'';?>" /></td>
                            <td><input name="field[name][element_delete]" type="text" id="name_element_delete" style="width:90%;" value="<?php echo isset($site_structure['name']['element_delete'])?$site_structure['name']['element_delete']:'';?>" /></td>
                        </tr>
                        <tr>
                            <td>brief</td>
                            <td><input name="field[brief][extra]" type="text" id="brief_extra" style="width:90%;" value="<?php echo isset($site_structure['brief']['extra'])?$site_structure['brief']['extra']:'';?>" /></td>
                            <td><input name="field[brief][element_delete]" type="text" id="brief_element_delete" style="width:90%;" value="<?php echo isset($site_structure['brief']['element_delete'])?$site_structure['brief']['element_delete']:'';?>" /></td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td><input name="field[description][extra]" type="text" id="description_extra" style="width:90%;" value="<?php echo isset($site_structure['description']['extra'])?$site_structure['description']['extra']:'';?>" /></td>
                            <td><input name="field[description][element_delete]" type="text" id="description_element_delete" style="width:90%;" value="<?php echo isset($site_structure['description']['element_delete'])?$site_structure['description']['element_delete']:'';?>" /></td>
                        </tr>
                    </table>
                    <input type="hidden" name="action" id="action" value="" />
                </form>
            </div>
        </div>
    </div>
</div>
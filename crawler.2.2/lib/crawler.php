<?php
ob_start ();
class Feed
{
	static $items = array();
	static function save_site_structure($cmd,$id,$site){
		if($cmd=='template' and $id and $site){
			// Cập nhật đường dẫn ảnh trong nội dung
			$image_content_left=(isset($_REQUEST['image_content_left']) and $_REQUEST['image_content_left'])?$_REQUEST['image_content_left']:'';
			$image_content_right=(isset($_REQUEST['image_content_right']) and $_REQUEST['image_content_right'])?$_REQUEST['image_content_right']:'';
			DB::update('site',array('image_content_left'=>$image_content_left,'image_content_right'=>$image_content_right),'id='.$id);
			// Lưu dữ liệu cấu trúc trang chi tiết
			DB::delete('site_structure','site_id='.$id);
			$field=(isset($_REQUEST['field']) and $_REQUEST['field'])?$_REQUEST['field']:false;
			if($field){
				foreach($field as $key=>$value){
					$arr=array(
						'field_name'=>$key,
						'extra'=>$value['extra'],
						'element_delete'=>$value['element_delete'],
						'site_id'=>$id
					);
					DB::insert('site_structure',$arr);
				}
			}
			header('Location:declaration_site.php?cmd=template&id='.$id);
		} elseif($cmd=='del-template' and $id and $site){
            // Lưu dữ liệu cấu trúc trang chi tiết
            DB::delete('site_structure','site_id='.$id);
            header('Location:declaration_site.php?cmd=edit&id='.$id);
        }
	}
	static function save_site($cmd,$id){
		if($cmd=='edit' and $id and $site=DB::fetch('select * from site where id='.$id)){
			$site_edit=array();
			$site_edit['name']=(isset($_REQUEST['name']) and $_REQUEST['name'])?$_REQUEST['name']:'';
			$site_edit['host']=(isset($_REQUEST['host']) and $_REQUEST['host'])?$_REQUEST['host']:'';
			$site_edit['url']=(isset($_REQUEST['url']) and $_REQUEST['url'])?$_REQUEST['url']:'';
			$site_edit['table_name']=(isset($_REQUEST['table_name']) and $_REQUEST['table_name'])?$_REQUEST['table_name']:'';
			$site_edit['pattern_bound']=(isset($_REQUEST['pattern_bound']) and $_REQUEST['pattern_bound'])?$_REQUEST['pattern_bound']:'';
			$site_edit['extra']=(isset($_REQUEST['extra']) and $_REQUEST['extra'])?$_REQUEST['extra']:'';
            $site_edit['field_name']=(isset($_REQUEST['field_name']) and $_REQUEST['field_name'])?$_REQUEST['field_name']:'';
            $site_edit['field_brief']=(isset($_REQUEST['field_brief']) and $_REQUEST['field_brief'])?$_REQUEST['field_brief']:'';
            $site_edit['category_id']=(isset($_REQUEST['category_id']) and $_REQUEST['category_id'])?$_REQUEST['category_id']:'';
			$site_edit['page_num']=(isset($_REQUEST['page_num']) and $_REQUEST['page_num'])?$_REQUEST['page_num']:'';
			$site_edit['image_pattern']=(isset($_REQUEST['image_pattern']) and $_REQUEST['image_pattern'])?$_REQUEST['image_pattern']:'';
			$site_edit['image_dir']=(isset($_REQUEST['image_dir']) and $_REQUEST['image_dir'])?$_REQUEST['image_dir']:'';
			$site_edit['image_content_left']=(isset($_REQUEST['image_content_left']) and $_REQUEST['image_content_left'])?$_REQUEST['image_content_left']:'';
			$site_edit['image_content_right']=(isset($_REQUEST['image_content_right']) and $_REQUEST['image_content_right'])?$_REQUEST['image_content_right']:'';
			$site_edit['begin']=(isset($_REQUEST['begin']) and $_REQUEST['begin'])?$_REQUEST['begin']:'';
			$site_edit['end']=(isset($_REQUEST['end']) and $_REQUEST['end'])?$_REQUEST['end']:'';
			DB::update('site',$site_edit,'id='.$id);
			header('Location:declaration_site.php?cmd=edit&id='.$id);
		}else{
			$site_edit=array();
			$site_edit['name']=(isset($_REQUEST['name']) and $_REQUEST['name'])?$_REQUEST['name']:'';
			$site_edit['host']=(isset($_REQUEST['host']) and $_REQUEST['host'])?$_REQUEST['host']:'';
			$site_edit['url']=(isset($_REQUEST['url']) and $_REQUEST['url'])?$_REQUEST['url']:'';
			$site_edit['table_name']=(isset($_REQUEST['table_name']) and $_REQUEST['table_name'])?$_REQUEST['table_name']:'';
			$site_edit['pattern_bound']=(isset($_REQUEST['pattern_bound']) and $_REQUEST['pattern_bound'])?$_REQUEST['pattern_bound']:'';
			$site_edit['extra']=(isset($_REQUEST['extra']) and $_REQUEST['extra'])?$_REQUEST['extra']:'';
            $site_edit['field_name']=(isset($_REQUEST['field_name']) and $_REQUEST['field_name'])?$_REQUEST['field_name']:'';
            $site_edit['field_brief']=(isset($_REQUEST['field_brief']) and $_REQUEST['field_brief'])?$_REQUEST['field_brief']:'';
			$site_edit['category_id']=(isset($_REQUEST['category_id']) and $_REQUEST['category_id'])?$_REQUEST['category_id']:'';
			$site_edit['page_num']=(isset($_REQUEST['page_num']) and $_REQUEST['page_num'])?$_REQUEST['page_num']:'';
			$site_edit['image_pattern']=(isset($_REQUEST['image_pattern']) and $_REQUEST['image_pattern'])?$_REQUEST['image_pattern']:'';
			$site_edit['image_dir']=(isset($_REQUEST['image_dir']) and $_REQUEST['image_dir'])?$_REQUEST['image_dir']:'';
			$site_edit['image_content_left']=(isset($_REQUEST['image_content_left']) and $_REQUEST['image_content_left'])?$_REQUEST['image_content_left']:'';
			$site_edit['image_content_right']=(isset($_REQUEST['image_content_right']) and $_REQUEST['image_content_right'])?$_REQUEST['image_content_right']:'';
			$site_edit['begin']=(isset($_REQUEST['begin']) and $_REQUEST['begin'])?$_REQUEST['begin']:'';
			$site_edit['end']=(isset($_REQUEST['end']) and $_REQUEST['end'])?$_REQUEST['end']:'';
            $id=DB::insert('site',$site_edit);
            header('Location:declaration_site.php?cmd=edit&id='.$id);
		}
	}
	static function delete_site($id){
		if($id and $site=DB::fetch('select * from site where id='.$id)){
			DB::delete('site_structure','site_id='.$id);
			DB::delete('site','id='.$id);
		}
		header('Location:declaration_site.php');
	}
	static function feed_data($cmd, $is_checked){
		if($cmd=='insert_database'){
			$dir = 'cache/temp_data.cache.php';
			if(file_exists($dir)){
                $arr_map = array(
                    'description' => 'post_content',
                    'brief' => 'post_excerpt',
                    'name' => 'post_title',
                );

				require $dir;
				if(isset($items) and $items){
					foreach($items as $key=>$value){
                        if (!in_array($key, $is_checked)) {
                            continue;
                        }

						//Feed::debug($value);
						$table=$value['table'];
                        if(!DB::fetch('select id from '.$table.' where `'.$arr_map['name'].'`="'.str_replace('"','\"',$value['name']).'"')){
							unset($value['table']);
                            unset($value['category_id']);
                            $image_url = $value['image_url'];
                            unset($value['image_url']);
                            foreach($arr_map as $f => $cf) {
                                if (isset($value[$f])) {
                                    $value[$cf] = $value[$f];
                                    unset($value[$f]);
                                }
                            }
                            // bo bot va add them cac field map
                            $value['to_ping'] = $value['pinged'] = $value['post_content_filtered'] = '';
                            $value['post_date'] = $value['post_date_gmt'] = $value['post_modified'] = $value['post_modified_gmt'] = date("Y-m-d H:i:s");
                            $value['post_parent'] = 0;
                            $value['post_status'] = CRAWLER_POST_STATUS;
                            $value['post_type'] = 'post';
                            $value['post_name'] = Feed::url_title(Feed::removesign($value['post_title']));
							$pid = DB::insert($table, $value);
                            $value['source_link'] = '';

                            $path = ROOT_PATH ."/". $image_url;
                            list($width, $height, $type, $attr) = getimagesize($path);
                            $type = image_type_to_mime_type ($type);

                            $value['post_parent'] = $pid;
                            $value['post_content'] = '';
                            $value['post_excerpt'] = '';
                            $value['guid'] = $image_url;
                            $value['post_type'] = 'attachment';
                            $value['post_mime_type'] = $type;
                            $value['post_status'] = 'inherit';
                            $image_id = DB::insert($table, $value);

                            $tmp = array(
                                'post_id' => $pid,
                                'meta_key' => '_thumbnail_id',
                                'meta_value' => $image_id,
                            );
                            DB::insert('dzo_postmeta', $tmp);

                            $tmp = array(
                                'post_id' => $image_id,
                                'meta_key' => '_wp_attached_file',
                                'meta_value' => str_replace('/resources/','',$image_url),
                            );
                            DB::insert('dzo_postmeta', $tmp);

                            $basename = basename($image_url);
                            $metadata = array(
                                'width' => $width,
                                'height' => $height,
                                'file' => str_replace('/resources/','',$image_url),
                                'sizes' => array (
                                    'thumbnail' => array (
                                        'file' => $basename,
                                        'width' => $width,
                                        'height' => $height,
                                        'mime-type' => $type,
                                    ),
                                    'medium' => array (
                                        'file' => $basename,
                                        'width' => 138,
                                        'height' => 92,
                                        'mime-type' => $type,
                                    ),
                                    'thumbnail' => array (
                                        'file' => $basename,
                                        'width' => 382,
                                        'height' => 215,
                                        'mime-type' => $type,
                                    )
                                ),
                                'image_meta' => array (
                                    'aperture' => 0,
                                    'credit' => '',
                                    'camera' => '',
                                    'caption' => 'caption',
                                    'created_timestamp' => time(),
                                    'copyright' => '',
                                    'focal_length' => 0,
                                    'iso' => 0,
                                    'shutter_speed' => 0,
                                    'title' => 'title',
                                    'orientation' => 0,
                                ));
                            $value = array(
                                'post_id' => $image_id,
                                'meta_key' => '_wp_attachment_metadata',
                                'meta_value' => serialize($metadata),
                            );
                            DB::insert('dzo_postmeta', $value);
						}
					}
					@unlink($dir);
				}
			}
			header('Location:'.$_SERVER['REQUEST_URI']);
		}else
		if($cmd=='feed' and $temps=$_REQUEST['temps']){
			// Lấy tin
			$temps = implode(',',$temps);
            require_once 'lib/simple_html_dom.php';
			require_once 'lib/crawler.php';
			if($sites = Feed::get_site('site.id in ('.$temps.')'))
			{
//				Feed::debug($sites);die;
				foreach($sites as $key=>$value)
				{
					$check_page = strpos($value['url'],'*');
					if($check_page===false){
						Feed::get_data($value,Feed::get_pattern($key));
					}else{
						if($page_num = $value['page_num']){
							$check_page_num = strpos($page_num,'-');
							if($check_page_num===false){
								$value['url'] = str_replace('*',$page_num,$value['url']);
								Feed::get_data($value,Feed::get_pattern($key));
							}else{
								$arr_page = explode('-',$page_num);
								for($i=$arr_page[1];$i>=$arr_page[0];$i--){
									$site = $value;
									$site['url'] = str_replace('*',$i,$value['url']);
									Feed::get_data($site,Feed::get_pattern($key));
								}
							}
						}else{
							$value['url'] = str_replace('*','1',$value['url']);
							Feed::get_data($value,Feed::get_pattern($key));
						}
					}
				}
				// Lưu tin đã lấy vào file cache
				$path = 'cache/temp_data.cache.php';
				$content = '<?php $items = '.var_export(Feed::$items,true).';?>';
				$handler = fopen($path,'w+');
				fwrite($handler,$content);
				fclose($handler);
			}
			header('Location:'.$_SERVER['REQUEST_URI']);
		}
	}
	static function get_template()
	{
		return DB::fetch_all('
			SELECT
				site.*,
				site.name as site_name,
				category.name as category_title
			FROM
				site
				LEFT OUTER JOIN category ON site.category_id=category.id
			ORDER BY
				site.name
		');
	}
	static function get_items(){
		$data='';
		$dir = 'cache/temp_data.cache.php';
		if(file_exists($dir)){
			require $dir;
			if(isset($items) and $items){
				foreach($items as $key=>$value){
					$data .= '<li><input type="checkbox" name="is_checked[]" value="'.$key.'" checked>'.$value['name'].'</li>';
				}
			}
		}
		return $data;
	}	
	static function get_site($cond = 1)
	{
		return DB::fetch_all('
			SELECT
				site.*
			FROM
				site 
			WHERE
				'.$cond.'
			ORDER BY	
				site.id DESC
		');
	}
	static function get_pattern($site_id)
	{
		return DB::fetch_all('
			SELECT
				*
			FROM
				site_structure
			WHERE
				site_id='.$site_id.'		
			ORDER BY
				id desc	
		');
	}
	static function format_link($source,$format=false)
	{
		if($format)
		{
			$source = str_replace(' ','%20',$source);	
		}
		else
		{
			if(strrpos($source,'?')===true)
			{
				$source = substr($source,0,strrpos($source,'?'));
			}
			$source = str_replace(' ','',$source);	
		}
		return $source;
	}
	static function save($sour,$dest)
	{
		$sour = Feed::format_link($sour,true);
		if(!file_put_contents($dest, file_get_contents($sour))){
			$dest = '';
		}
	}
	static function parse_row($link, $pattern, $site)
	{
        $item = array();

        if(isset($site['image_url']) and $site['image_url'])
        {
            $item['image_url'] = $site['image_url'];
        }
        if(isset($site['value_name']) and $site['value_name'])
        {
            $item['name'] = $site['value_name'];
        }
        if(isset($site['value_brief']) and $site['value_brief'])
        {
            $item['brief'] = $site['value_brief'];
        }
//var_dump(array($item, $pattern)); die;
        if (!$pattern) {
            $check = false;

            if(isset($item['name']))
            {
                foreach(Feed::$items as $key=>$value){
                    if($value['name']==$item['name']) {
                        $check=true;
                        break;
                    }
                }
                if(!$check){
                    if(isset($site['source_link']) and $site['source_link'])
                    {
                        $item['source_link'] = $site['source_link'];
                    }

                    $item['description'] = '';

                    $item += array(
                        'category_id'=>$site['category_id'],
                        'table'=>$site['table_name']
                    );
                    Feed::$items[] = $item;
                }
            }

            return false;
        }

        $html=Feed::html_no_comment($link);
		if($html){
			$html = str_get_html($html);
			$check = false;
			if(isset($site['image_url']) and $site['image_url'])
			{
				$item['image_url'] = $site['image_url'];
			}
			if($pattern)
			{
				foreach($pattern as $key=>$value)
				{
                    $element_delete = $value['element_delete'];
					if($detail_pattern = $value['extra']){
                        $tmp = explode(":", $detail_pattern);
                        if (isset($tmp[1])) {
                            $element = $html->find($tmp[0], $tmp[1]);
                            $item[$value['field_name']] = $element->innertext;
                        } else {
                            foreach($html->find($detail_pattern) as $element)
                            {
                                if($element_delete){
                                    $arr = explode(',',$element_delete);
                                    for($i=0;$i<count($arr);$i++){
                                        foreach($element->find($arr[$i]) as $e){
                                            $e->outertext='';
                                        }
                                    }
                                }
                                if($value['field_name']=='name' or $value['field_name']=='brief'){
                                    $text = trim($element->plaintext);
                                    $item[$value['field_name']] = html_entity_decode($text);
                                }else{
                                    if($value['field_name'] == 'description'){
                                        // detect lấy ảnh bự
                                        $items = $element->find('img');
                                        if($items and count($items)){
                                            $folder = $site['folder'];
                                            $flag = false;
                                            foreach($items as $img){
                                                $image_url=$img->src;

                                                $source = Feed::check_link($image_url,$site['host']);

                                                $img->src = $source;

                                                if (!$flag) {
                                                    $basename = basename($source);
                                                    // Thư mục chứa ảnh
                                                    if (file_exists(ROOT_PATH . $folder . '/' . $basename)) {
                                                        $dest = $folder . '/' . time() . '_' . $basename;
                                                    } else {
                                                        $dest = $folder . '/' . $basename;
                                                    }
                                                    Feed::save($source, ROOT_PATH . $dest);
                                                    $item['image_url'] = $dest;

                                                    //break;
                                                    $flag = true;
                                                }
                                            }
                                        }
                                    }
                                    $item[$value['field_name']] = $element->innertext;
                                }
                                break;
                            }
                        }
					}
				}
				if(isset($item['name']))
				{
					foreach(Feed::$items as $key=>$value){
						if($value['name']==$item['name']) $check=true;
					}
					if(!$check){
                        if(isset($site['source_link']) and $site['source_link'])
                        {
                            $item['source_link'] = $site['source_link'];
                        }

						// Viết lại đường dẫn ảnh trong nội dung
						if(isset($item['description']) and $item['description']){
							$item['description']=str_replace($site['image_content_left'],$site['image_content_right'],$item['description']);
						}
						$item+= array(
							'category_id'=>$site['category_id'],
							'table'=>$site['table_name']
						);
						Feed::$items[] = $item;
					}
				}
			}

			$html->clear();
			unset($html);
		}
	}
	static function get_data($site,$pattern)
	{
		$html=Feed::html_no_comment($site['url']);
        if($html){
			//Feed::debug($html);
			$hd = $site['begin'];
			$ft = $site['end'];

			if(!$hd or !($bg = strpos($html,$hd))) $bg = 0;
			if(!$ft or !($end = strpos($html,$ft))) $end = strlen($html);

			$html = substr($html,$bg+strlen($hd),$end-$bg-strlen($hd));
//            Feed::debug($html);
			$html = str_get_html($html);

			$host = $site['host'];
			$pattern_bound = $site['pattern_bound'];
			$pattern_link = $site['extra'];
			$pattern_img = $site['image_pattern'];
			
			$folder = $site['image_dir'] ."/". date("Y/m"); // Thư mục chứa ảnh
            if(!is_dir(ROOT_PATH . $folder)) @mkdir(ROOT_PATH . $folder,0755,true);
            $site['folder'] = $folder;

			$num=0;
			$maxitem=1000;
			foreach($html->find($pattern_bound) as $item)
			{
				if($num>=$maxitem) break;
				$num++;
				if ($pattern_link=='.') {
                    $link = Feed::check_link($item->getAttribute('href'), $host);
                } else {
                    foreach ($item->find($pattern_link) as $link) {
                        $link = Feed::check_link($link->getAttribute('href'), $host);
                    }
                }
                
                $unique_link = $link;//md5($link); khong md5 nua , dung link view detail
                $exists = DB::fetch("select count(*) as total from {$site['table_name']} where source_link='".$unique_link."'", "total");
                if(!$exists && Feed::check_url($link)){
                    $site['source_link'] = $unique_link;

					$items = $item->find($pattern_img);
					if($items and count($items)){
						foreach($items as $img){
							$image_url=$img->src;
						}
                        $source = Feed::check_link($image_url, $site['host']);
                        $basename = basename($source);
						// Thư mục chứa ảnh
						if(file_exists(ROOT_PATH . $folder.'/'.$basename)){
							$dest = $folder.'/'.time().'_'.$basename;
						}else{
							$dest = $folder.'/'.$basename;
						}
						Feed::save($source, ROOT_PATH . $dest);
						$site['image_url'] = $dest;
					} else {
						$site['image_url'] = '';
					}

                    // lay tieu de mau tin
                    if($site['field_name']){
                        foreach($item->find($site['field_name']) as $element)
                        {
                            $text = trim($element->plaintext);
                            if ($text) {
                                $site['value_name'] = html_entity_decode($text);
                                break;
                            }
                        }
                    }

                    // lay mo ta mau tin
                    if($site['field_brief']){
                        foreach($item->find($site['field_brief']) as $element)
                        {
                            $text = trim($element->plaintext);
                            if ($text) {
                                $site['value_brief'] = html_entity_decode($text);
                                break;
                            }
                        }
                    }

					//Feed::debug($site);
					Feed::parse_row($link,$pattern,$site);
				}
			}
		}
		$html->clear();
		unset($html);
	}
	static function check_link($url,$host='')
	{
        if(strpos($url,'http')===false) {
           return $host . $url;
        }
//		if((strpos($url,'http://')===false) and (preg_match_all('/http:\/\/(.*)\.([a-z]+)\//',$host,$matches,PREG_SET_ORDER)))
//		{
//			while ($url{0}=='/'){
//				$url=substr($url,1);
//			}
//			if($matches[0][0]{strlen($matches[0][0])-1}!='/'){
//				$matches[0][0]=$matches[0][0].'/';
//			}
//			$url = $matches[0][0].$url;
//		}
		return $url;
	}
	static function check_url($url=NULL){
		if($url == NULL) return false;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);//lay code tra ve cua http
		curl_close($ch);
		return ($httpcode>=200 && $httpcode<300);
	}
	static function _isCurl(){
		return function_exists('curl_version');
	}
	static function _urlencode($url){
		$output="";
		for($i = 0; $i < strlen($url); $i++) 
		$output .= strpos("/:@&%=?.#", $url[$i]) === false ? urlencode($url[$i]) : $url[$i]; 
		return $output;
	}
	static function file_get_contents_curl($url) {
		//$url=urlencode($url);
		//debug($url);
		$ch = curl_init();
	
		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	
		$data = curl_exec($ch);
		curl_close($ch);
	
		return $data;
	}
	static function html_no_comment($url) {
		// create HTML DOM
		$check_curl=Feed::_isCurl();
		if(!$html=file_get_html($url)){
			if(!$html=str_get_html(Feed::file_get_contents_curl($url)) or !$check_curl){
				return false;
			}
		}
		// remove all comment elements
		foreach($html->find('comment') as $e)
		
			$e->outertext = '';
	
		$ret = $html->save();
		
		// clean up memory
		$html->clear();
		unset($html);
		return $ret;
	}
	static function debug($arr){
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
		exit();
	}

    static function removesign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }

    static function url_title($str, $separator = '-', $lowercase = true)
    {
        if ($separator == 'dash')
        {
            $separator = '-';
        }
        else if ($separator == 'underscore')
        {
            $separator = '_';
        }

        $q_separator = preg_quote($separator);

        $trans = array(
            '&.+?;'                 => '',
            '[^a-z0-9 _-]'          => '',
            '\s+'                   => $separator,
            '('.$q_separator.')+'   => $separator
        );

        $str = strip_tags($str);

        foreach ($trans as $key => $val)
        {
            $str = preg_replace("#".$key."#i", $val, $str);
        }

        if ($lowercase === TRUE)
        {
            $str = strtolower($str);
        }

        return trim($str, $separator);
    }
}
?>
<?php
$category=DB::fetch_all('select * from category');
$id=(isset($_REQUEST['id']) and $_REQUEST['id'])?$_REQUEST['id']:0;
if($cmd=='edit'){
	$site=DB::fetch('select * from site where id='.$id);
    $site_structure=DB::fetch_all('select *,field_name as id from site_structure where site_id='.$id);
}
if(isset($_REQUEST['action']) and $_REQUEST['action']=='save') {
    Feed::save_site($cmd, $id);
}
?>
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: PAGE TITLE & BREADCRUMB -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li class="active">
                    Quản lý mẫu lấy tin tự động
                </li>
            </ol>
            <div class="page-header">
                <div class="col-md-8">
                    <h1><?= ($cmd=='edit')?'Cập nhật':'Thêm'; ?> mẫu lấy tin tự động</h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="javascript:void(0)" onclick="save_declaration_site();" class="btn btn-primary" data-placement="top" data-toggle="tooltip" title="Ghi lại">
                        <i class="icon-save"></i> Ghi lại</a>
            <?php if($cmd=='edit') { ?>
                    <a href="declaration_site.php?cmd=template&id=<?php echo $id;?>" class="btn btn-teal" data-placement="top" data-toggle="tooltip" title="Chi tiết mẫu tin">
                        <i class="icon-edit"></i> Chi tiết</a>
            <?php } ?>
            <?php if (isset($site_structure) && $site_structure) { ?>
                    <a href="declaration_site.php?cmd=del-template&id=<?php echo $id;?>" class="btn btn-danger" data-placement="top" data-toggle="tooltip" title="Không lấy nội dung tin">
                        <i class="icon-remove"></i> Xóa Chi tiết</a>
            <?php } ?>
                </div>
                <div style="clear: both;"></div>
            </div>
            <!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
    </div>
    <!-- end: PAGE HEADER -->

    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- thêm hoặc sửa mẫu lấy tin -->
            <div class="form-content">
                <form name="EditDeclarationSite" id="EditDeclarationSite" method="post">
                    <table width="100%" cellpadding="5" cellspacing="0" border="1" style="border-collapse:collapse" bordercolor="#cccccc">
                        <tr>
                            <td><label>Tên mẫu (<span class="require">*</span>)</label></td>
                            <td><input name="name" type="text" id="name" class="search-field" value="<?php echo isset($site['name'])?$site['name']:'';?>" /></td>
                        </tr>
                        <tr>
                            <td><label>Host (<span class="require">*</span>)</label></td>
                            <td><input name="host" type="text" id="host" class="search-field" value="<?php echo isset($site['host'])?$site['host']:'';?>" /></td>
                        </tr>
                        <tr>
                            <td><label>Url (<span class="require">*</span>)</label></td>
                            <td><input name="url" type="text" id="url" class="search-field" value="<?php echo isset($site['url'])?$site['url']:'';?>" /></td>
                        </tr>
                        <tr>
                            <td><label>Chèn vào bảng (<span class="require">*</span>)</label></td>
                            <td><input name="table_name" type="text" id="table_name" class="search-field" value="<?php echo isset($site['table_name'])?$site['table_name']:'';?>" /></td>
                        </tr>
                        <tr>
                            <td><label>Mẫu bao ngoài một đối tượng (<span class="require">*</span>)</label></td>
                            <td><input name="pattern_bound" type="text" id="pattern_bound" class="search-field" value='<?php echo isset($site['pattern_bound'])?$site['pattern_bound']:'';?>' /></td>
                        </tr>
                        <tr>
                            <td><label>Mẫu liên kết một tin (<span class="require">*</span>)</label></td>
                            <td><input name="extra" type="text" id="extra" class="search-field" value='<?php echo isset($site['extra'])?$site['extra']:'';?>'  /></td>
                        </tr>
                        <tr>
                            <td><label>Tiêu đề Mẫu tin</label></td>
                            <td><input name="field_name" type="text" id="field_name" class="search-field" value='<?php echo isset($site['field_name'])?$site['field_name']:'';?>'/>
                            <i>Nếu không cần lấy nội dung tin, thì có thể chỉ định tại bước này</i>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Mô tả Mẫu tin</label></td>
                            <td><input name="field_brief" type="text" id="field_brief" class="search-field" value='<?php echo isset($site['field_brief'])?$site['field_brief']:'';?>'/>
                                <i>Nếu không cần lấy nội dung tin, thì có thể chỉ định tại bước này</i>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Chèn vào danh mục</label></td>
                            <td><select name="category_id" id="category_id" style="width:20%;">
                            <?php
                            if($category)
                            foreach($category as $key=>$value){
                                echo '<option value="'.$key.'"'.((isset($site['category_id']) and $site['category_id']==$key)?' selected':'').'>'.$value['name'].'</option>';
                            }
                            ?>
                            </select></td>
                        </tr>
                        <tr>
                            <td><label>Số trang cần lấy</label></td>
                            <td><input name="page_num" type="text" id="page_num" style="width:20%;" value="<?php echo isset($site['page_num'])?$site['page_num']:'';?>"  /> (Ví dụ điền "1-9" sẽ thay ký tự "*" trên url lần lượt thành các trang từ 1 tới 9)</td>
                        </tr>
                        <tr>
                            <td><label>Mẫu ảnh đại diện</label></td>
                            <td><input name="image_pattern" type="text" id="image_pattern" style="width:60%;" value='<?php echo isset($site['image_pattern'])?$site['image_pattern']:'';?>'  /></td>
                        </tr>
                        <tr>
                            <td><label>Thư mục chứa ảnh đại diện</label></td>
                            <td><input name="image_dir" type="text" id="image_dir" style="width:60%;" value="<?php echo isset($site['image_dir'])?$site['image_dir']:'';?>"  /></td>
                        </tr>
                       <tr>
                            <td><label>Bắt đầu và kết thúc vùng cần lấy</label></td>
                            <td><input name="begin" type="text" id="begin" style="width:35%;" value='<?php echo isset($site['begin'])?str_replace('"','&quot;',$site['begin']):'';?>'  /> ==>
                                 <input name="end" type="text" id="end" style="width:35%;" value='<?php echo isset($site['end'])?str_replace('"','&quot;',$site['end']):'';?>'  />
                             </td>
                       </tr>
                    </table>
                    <input type="hidden" name="action" value="" id="action" />
                </form>
            </div>
        </div>
    </div>
</div>
<?php
if(isset($_REQUEST['action']) and $_REQUEST['action']=='delete'){
	$ids=(isset($_REQUEST['selected_ids']) and $_REQUEST['selected_ids'])?$_REQUEST['selected_ids']:0;
	if($ids){
		foreach($ids as $id){
			Feed::delete_site($id);
		}
	}
}
?>
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: PAGE TITLE & BREADCRUMB -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="/">Trang chủ</a>
                </li>
                <li class="active">
                    Quản lý mẫu lấy tin tự động
                </li>
            </ol>
            <div class="page-header">
                <div class="col-md-8">
                    <h1>Quản lý mẫu lấy tin tự động</h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="declaration_site.php?cmd=add" class="btn btn-primary" data-placement="top" data-toggle="tooltip" title="Thêm mẫu tin"><i class="icon-plus"></i> Thêm</a>
                    <a href="javascript:void(0)" onclick="delete_declaration_site();" class="btn btn-danger" data-placement="top" data-toggle="tooltip" title="Xóa mẫu tin">
                        <i class="glyphicon glyphicon-ok-sign"></i> Xóa</a>
                </div>
                <div style="clear: both;"></div>
            </div>
            <!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
    </div>
    <!-- end: PAGE HEADER -->

    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <!-- bắt đầu danh sách -->
            <div class="form-content">
                <form name="DeclarationSite" method="post">
                    <table class="table table-striped table-bordered table-hover" id="sample-table-2">
                        <thead>
                        <tr class="ht">
                            <td width="1%" align="center"><input id="DeclarationSite_all_checkbox" onclick="select_all_checkbox(this.checked);" title="Tất cả" type="checkbox" value="1" /></th>
                            <th align="left" nowrap>Tên mẫu</th>
                            <th width="27%" align="left" nowrap>Đường dẫn</th>
                            <th width="1%" align="left" nowrap>Bảng dữ liệu</th>
                            <th width="1%" align="left" nowrap>Danh mục</th>
                            <th width="10%" align="left">Mẫu ảnh đại diện</th>
                            <th nowrap width="1%" align="center">Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($items as $key=>$value){ ?>
                            <tr valign="middle">
                                <td width="1%" align="center">
                                    <input name="selected_ids[]" type="checkbox" value="<?php echo $key;?>" />
                                </td>
                                <td align="left" nowrap><?php echo $value['name'];?></td>
                                <td align="left" nowrap style="width:250px;"><div style="word-wrap:break-word;"><a href="<?php echo $value['url'];?>" target="_blank"><?php echo $value['url'];?></a></div></td>
                                <td align="left" nowrap><?php echo $value['table_name'];?></td>
                                <td align="left" nowrap><?php echo $value['category_title'];?></td>
                                <td align="left" nowrap><?php echo $value['image_pattern'];?></td>
                                <td align="center" nowrap width="1%">
                                    <a href="declaration_site.php?cmd=edit&id=<?php echo $key;?>"><img src="images/edit.jpg" title="Sửa" /></a>
                                    <a href="declaration_site.php?cmd=template&id=<?php echo $key;?>"><img src="images/txt.png" title="Cấu trúc" /></a>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table><br />
                    <input type="hidden" name="action" value="" id="action" />
                </form>
                <!-- kết thúc danh sách -->
            </div>
        </div>
    </div>
</div>

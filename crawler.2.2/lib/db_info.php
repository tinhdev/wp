<?php
$env = getenv('ENVIRONMENT') ? getenv('ENVIRONMENT') : 'production';

defined('CRAWLER_POST_STATUS') || define('CRAWLER_POST_STATUS', $env == 'production' ? 'draft' : 'publish');

$_envdb['production'] = array(
    "sqlserver"     => "127.0.0.1",
    "sqluser"       => "root",
    "sqlpassword"   => "dev!@#$%^",
    "dbname"        => "123share",
);

$_envdb['development'] = array(
    "sqlserver"     => "localhost",
    "sqluser"       => "root",
    "sqlpassword"   => "",
    "dbname"        => "123share",
);

$sqlserver      =$_envdb[$env]["sqlserver"];
$sqluser        =$_envdb[$env]["sqluser"];
$sqlpassword    =$_envdb[$env]["sqlpassword"];
$dbname         =$_envdb[$env]["dbname"];
?>

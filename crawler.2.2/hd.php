<?php
$cmd=(isset($_REQUEST['cmd']) and $_REQUEST['cmd'])?$_REQUEST['cmd']:'';
require 'lib/database.php';
require 'lib/crawler.php';
$items=Feed::get_template();
?>
<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3 Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title>Lấy tin tự động</title>
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/style.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/main-responsive.css">
    <link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="assets/css/theme_light.css" id="skin_color">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
<!-- start: HEADER -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->
            <!-- start: LOGO -->
            <a class="navbar-brand" href="/crawler.2.2">
                Crawler.2.2 <i class="clip-clip"></i>
            </a>
            <!-- end: LOGO -->
        </div>
        <div class="navbar-tools">
            <ul class="nav navbar-left" style="margin-left: 22px;padding-right: 0px;">
                <!-- start: MENU DROPDOWN -->
                <li>
                    <a class="dropdown-toggle" href="/">
                        <i class="glyphicon glyphicon-home"></i>
                        <span class="username"></span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" href="/wp-admin">
                        <i class="clip-cog-2"></i>
                        <span class="username">Vào Admin</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" href="declaration_site.php">
                        <i class="icon-tasks"></i>
                        <span class="username">Quản lý mẫu lấy tin</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" href="hd.php">
                        <i class="clip-info"></i>
                        <span class="username">Hướng dẫn thêm mẫu tin</span>
                    </a>
                </li>
                <!-- end: USER DROPDOWN -->
            </ul>
        </div>
    </div>
    <!-- end: TOP NAVIGATION CONTAINER -->
</div>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <!-- start: PAGE -->
    <div class="main-content" style="margin-left: 0!important;">
        <!-- end: SPANEL CONFIGURATION MODAL FORM -->
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li>
                            <i class="clip-home-3"></i>
                            <a href="/">Trang chủ</a>
                        </li>
                        <li class="active">
                            Lấy tin tự động
                        </li>
                    </ol>
                    <div class="page-header">
                        <div class="col-md-8">
                            <h1>Hướng dẫn sử dụng</h1>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->

            <!-- start: PAGE CONTENT -->
            <div class="row">
                <div class="col-sm-12">
                    <div id="page_print">
            <p>Sau khi các bạn setup xong vào trang chính (ví dụ trên máy tôi là crawler.minhtc).</p>
            <p><img src="./images/laytin.png" alt="Lấy tin" width="620" height="194"></p>
            <p>Đây là giao diện trang lấy tin. Gồm danh sách các mẫu lấy tin tôi đã khai báo sẵn. Các bạn có thể khai báo thêm tùy thích.</p>
            <p>Bên phải của tên mẫu có đường <strong>link</strong> tới trang muốn lấy, các bạn có thể xem chi tiết.</p>
            <p>Phần danh mục của tin tôi không làm phần quản trị mà để 2 danh mục là <strong>Danh mục tin tức</strong> và <strong>Music</strong> làm demo.</p>
            <p>&nbsp;</p>
            <h3>Quá trình lấy tin</h3>
            <p>Để lấy tin các bạn tich chọn mẫu cần lấy rồi nhấn nút <strong>Lấy tin.</strong> Quá trình lấy tin có thể nhanh hay chậm tùy thuộc vào trang cần lấy, tốc độ mạng …</p>
            <p><img src="./images/danglaytin.png" alt="Đang lấy tin" width="620" height="192"></p>
            <p>Sau khi lấy xong</p>
            <p><img src="./images/danhsachtinkhilayxong.png" alt="khi lấy xong tin" width="620" height="203"></p>
            <p>Lúc này dữ liệu đã được lưu vào file temp và nút <strong>Chèn vào Database</strong> sẽ sẵn sàng để bạn ghi dữ liệu đã lấy vào database.</p>
            <p>Sau khi nhấn nút <strong>Chèn vào Database </strong>các bạn vào trang <strong>Danh sách tin đã lấy</strong> (news.php) để xem các tin đã lấy về.</p>
            <p><img src="./images/danhsachtindalay.png" alt="danh sách tin đã lấy" width="620" height="318"></p>
            <p>Nhấn vào chi tiết một tin để xem chi tiết.</p>
            <p><img src="./images/chitiettindalay.png" alt="chi tiết tin" width="620" height="318"></p>
            <h3>Quản lý mẫu lấy tin</h3>
            <p>Các bạn vào trang <strong>Quản lý mẫu lấy tin</strong> (declaration_site.php).</p>
            <p><img src="./images/danhsachmaulaytin.png" alt="danh sách mẫu lấy tin" width="620" height="207"></p>
            <p>Để cụ thể tôi sẽ lấy mẫu <strong>Showbiz Việt – Báo ngoisao.net</strong> làm demo.</p>
            <p>Các bạn nhấn vào nút sửa mầu vàng bên phải mẫu.</p>
            <p><img src="./images/chitietmotmaulaytin.png" alt="chi tiết mẫu lấy tin" width="620" height="282"></p>
            <p>Thông tin có dấu <span style="color: #ff0000;">*</span> là bắt buộc phải nhập. Tôi sẽ giải thích từng trường thông tin:</p>
            <ol>
                <li><strong>Tên mẫu</strong>: Các bạn đặt tên cho mẫu để dễ nhớ. VD: <strong>Showbiz Việt - Báo ngoisao.net</strong></li>
                <li><strong>Host</strong>: Là trang chính của trang cần lấy. VD: <strong>http://ngoisao.net/</strong></li>
                <li><strong>Url</strong>: Là đường dẫn cụ thể của trang cần lấy. VD: <strong>http://ngoisao.net/tin-tuc/showbiz-viet/</strong></li>
                <li><strong>Chèn vào bảng</strong>: Các bạn nhập tên bảng dữ liệu cần đưa dữ liệu vào. VD ở đây là bảng <strong>news</strong></li>
                <li><strong>Mẫu bao ngoài một đối tượng:</strong>
                    <p>Các bạn vào link cần lấy dữ liệu <a href="http://ngoisao.net/tin-tuc/showbiz-viet/" target="_blank">http://ngoisao.net/tin-tuc/showbiz-viet/</a></p>
                    <p><img src="./images/vung_can_lay_du_lieu.png" alt="vùng cần lấy dữ liệu" width="620" height="318"></p>
                    <p>Sau khi xác định được vùng cần lấy dữ liệu, các bạn xác định mẫu bao ngoài một đối tượng. Tôi định nghĩa mẫu bao ngoài một đối tượng là mẫu chứa đường link tới trang chi tiết tin và ảnh đại diện của tin.</p>
                    <p>Để xác định mẫu bao ngoài một đối tượng các bạn sử dụng <span>Firebug, trên FF hay Chrome , bấm F12 cho nó chạy, <span>Firebug là 1 plugin của FF, Chrome thì có sẵn. Sau đó nhấn chuột phải vào đối tượng cần xem mẫu và chọn <strong>Kiểm tra phần tử</strong> (<span><strong>Inspect Element With Firebug</strong>). Hoặc các bạn có thể làm như sau: Nhấn F12</span><br></span></span></p>
                    <p>Với Chrome Trong tab Elements các bạn nhấn vào nút Select an element in the page to inspect it (hình kính lúp bên dưới vị trí thứ 3 từ trái sang)</p>
                    <p>Với FF các bạn nhấn nút Select an element in the page to inspect ở vị trí thứ 2 từ trái sang của Firebug.</p>
                    <p>sau đó di chuột vào vùng mẫu bao ngoài một đối tượng mà tôi đã khoanh mầu xanh ở hình trên. Các bạn sẽ thấy như hình dưới:</p>
                    <p><img src="./images/mau_bao_ngoai_mot_doi_tuong.png" alt="mẫu bao ngoài một đối tượng" width="620" height="318"></p>
                    <p>Các bạn để ý thấy mẫu bao ngoài một đối tượng sẽ có dạng <strong>ul.news li</strong></p>
                    <p>&nbsp;</p>
                </li>
                <li><strong>Mẫu liên kết một tin</strong>:
                    <p>Là đường link tới trang chi tiết của tin cần lấy nằm trong mẫu bao ngoài một đối tượng đã xác định ở trên. Ở bước này các bạn chỉ cần xác định tới thẻ a chứa liên kết là được. Cách làm tương tự như trên ta sẽ thấy trong vùng mẫu bao ngoài một đối tượng có 2 liên kết tới trang chi tiết</p>
                    <p><img src="./images/mau_lien_ket_mot_tin.png" alt="mẫu liên kết một tin" width="620" height="318"></p>
                    <p>vì vậy ta có 2 cách xác định Mẫu liên kết một tin như sau:</p>
                    <p>- <strong>a.ptw</strong></p>
                    <p>- <strong>h3 a</strong></p>
                    <p>Cả 2 cách trên đều được.</p>
                </li>
                <li><strong>Chèn vào danh mục</strong>:
                    <p>Là danh mục cần lấy tin trên hệ thống của bạn. VD: <strong>Danh mục tin tức</strong></p>
                </li>
                <li><strong>Số trang cần lấy</strong>:
                    <p>Ví dụ các bạn muốn lấy tin ở trang 1 và trang 2 trong mục <strong>http://ngoisao.net/tin-tuc/showbiz-viet/ </strong>lúc này đường dẫn phần Url sẽ phải thay đổi một chút thành:</p>
                    <p><strong>http://ngoisao.net/tin-tuc/showbiz-viet/?p=*</strong></p>
                    <p>và trong mục này các bạn điền: <strong>1-2</strong></p>
                    <p>Ví dụ các bạn muốn lấy 9 trang từ 1 tới 9 thì điền "1-9" khi lấy hệ thống sẽ thay ký tự "*" trên url lần lượt thành các trang từ 1 tới 9.</p>
                    <p>&nbsp;</p>
                </li>
                <li><strong>Mẫu ảnh đại diện</strong>:
                    <p>Là ảnh đại diện của tin. Cách xác định mẫu giống như mục 5 và mục 6.</p>
                    <p>Vậy ở ví dụ này mẫu ảnh đại chỉ cần ghi là <strong>img</strong>.</p>
                    <p><span style="color: #ff0000;"><strong>Lưu ý: cách xác định mẫu liên kết một tin, mẫu ảnh đại diện đều tính từ bên trong mẫu bao ngoài một tin.</strong></span></p>
                    <p>&nbsp;</p>
                </li>
                <li><strong>Thư mục chứa ảnh đại diện</strong>:
                    <p>Là đường dẫn chứa ảnh đại diện của tin trên hệ thống khi đã lấy về.</p>
                    <p>&nbsp;</p>
                </li>
                <li><strong>Thay thế đường dẫn ảnh trong nội dung</strong>:
                    <p>Là đường dẫn ảnh trong phần chi tiết một tin.</p>
                    <p><img src="./images/duong_dan_anh_trong_noi_dung.png" alt="đường dẫn ảnh trong trang chi tiết một tin" width="620" height="318"></p>
                    <p>Các bạn thấy đường dẫn ảnh ở đây là đường dẫn trên server của trang cần lấy. Khi lấy về ảnh này sẽ không hiện thị được vì thiếu phần tên miền. Vậy ta phải thêm phần têm miền vào đường dẫn ảnh. Trong ví dụ này mục Thay thế đường dẫn ảnh trong nội dung các bạn điền như sau:</p>
                    <p><strong>/Files/</strong> ==&gt; <strong>http://ngoisao.net/Files/</strong></p>
                    <p>&nbsp;</p>
                </li>
                <li><strong>Bắt đầu và kết thúc vùng cần lấy</strong>:
                    <p>Đây là tùy chọn các bạn có thể điền hoặc không. Để quá trình phân tích trang được nhanh hơn nghĩa là thời gian lấy tin nhanh hơn các bạn xác định đoạn đầu và đoạn cuối bao vùng cần lấy dữ liệu. Ở đây không cần xác định mẫu mà chỉ là một đoạn html của trang cần lấy.</p>
                    <p>Trong ví dụ này các bạn có thể điền như sau:</p>
                    <p><strong>class="navigationDetail"</strong> ==&gt; <strong>class="divTag"</strong></p>
                </li>
            </ol>
            <p>Sau khi điền xong các thông tin trên các bạn nhấn nút <strong>Ghi lại</strong></p>
            <p>Tiếp đến các bạn nhấn vào nút <strong>Chi tiết </strong>để sang phần khai báo các trường thông tin chi tiết cần lấy trong trang chi tiết một tin.</p>
            <p>Ở đây ta sẽ có 3 trường thông tin cần khai báo là:</p>
            <ol>
                <li><strong>Name</strong>:
                    <p>Là tiêu đề tin.</p>
                    <p>- Mẫu cần lấy. VD: <strong>h1.Title</strong></p>
                    <p>- Mẫu đối tượng cần xóa. VD: Trong ví dụ này không có đối tượng cần xóa</p>
                </li>
                <li><strong>Brief</strong>:
                    <p>Là phần tóm tắt tin.</p>
                    <p>- Mẫu cần lấy. VD: <strong>h2.Lead</strong></p>
                    <p>- Mẫu đối tượng cần xóa. VD: Trong ví dụ này không có đối tượng cần xóa</p>
                </li>
                <li><strong>Description</strong>:
                    <p>Là phần chi tiết tin.</p>
                    <p>- Mẫu cần lấy. VD: <strong>.detailCT</strong></p>
                    <p>- Mẫu đối tượng cần xóa. VD:</p>
                    <p><strong>.topDetail,h1,h2,.RelatedLeadSubject,.detailNS,.relateNewsDetail</strong></p>
                </li>
            </ol>
            <p>Sau khi điền xong các bạn nhấn nút <strong>Ghi lại.</strong></p>
            <p><strong>Vậy là ta đã khai báo xong một mẫu lấy tin. Các bạn ra trang lấy tin để thử.</strong></p>
        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        2013 &copy; 123dzo.net
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/blockUI/jquery.blockUI.js"></script>
<script src="assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="assets/js/main.js"></script>

<script src="js/crawler.js" type="text/javascript"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function() {
        Main.init();
    });
</script>
</body>
<!-- end: BODY -->
</html>
<?php
date_default_timezone_set("Asia/Saigon");
if (!ini_get('display_errors')) {
    ini_set('display_errors', '1');
}

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . "/../"));

$cmd=(isset($_REQUEST['cmd']) and $_REQUEST['cmd'])?$_REQUEST['cmd']:'';
$is_checked = (isset($_REQUEST['is_checked']) and $_REQUEST['is_checked']) ? $_REQUEST['is_checked'] : array();

set_time_limit(0);

require 'lib/database.php';
require 'lib/crawler.php';
$temps=Feed::get_template();
$items=Feed::get_items();
Feed::feed_data($cmd, $is_checked);
?>
<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3 Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title>Lấy tin tự động</title>
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/style.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/main-responsive.css">
    <link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="assets/css/theme_light.css" id="skin_color">
    <!--[if IE 7]>
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
    <![endif]-->
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
<!-- start: HEADER -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->
            <!-- start: LOGO -->
            <a class="navbar-brand" href="/crawler.2.2">
                Crawler.2.2 <i class="clip-clip"></i>
            </a>
            <!-- end: LOGO -->
        </div>
        <div class="navbar-tools">
            <ul class="nav navbar-left" style="margin-left: 22px;padding-right: 0px;">
                <!-- start: MENU DROPDOWN -->
                <li>
                    <a class="dropdown-toggle" href="/">
                        <i class="glyphicon glyphicon-home"></i>
                        <span class="username"></span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" href="/wp-admin">
                        <i class="clip-cog-2"></i>
                        <span class="username">Vào Admin</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" href="declaration_site.php">
                        <i class="icon-tasks"></i>
                        <span class="username">Quản lý mẫu lấy tin</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" href="hd.php">
                        <i class="clip-info"></i>
                        <span class="username">Hướng dẫn thêm mẫu tin</span>
                    </a>
                </li>
                <!-- end: USER DROPDOWN -->
            </ul>
        </div>
    </div>
    <!-- end: TOP NAVIGATION CONTAINER -->
</div>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
<!-- start: PAGE -->
<div class="main-content" style="margin-left: 0!important;">
<!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->
        <div class="row">
            <div class="col-sm-12">
                <!-- start: PAGE TITLE & BREADCRUMB -->
                <ol class="breadcrumb">
                    <li>
                        <i class="clip-home-3"></i>
                        <a href="/">Trang chủ</a>
                    </li>
                    <li class="active">
                        Lấy tin tự động
                    </li>
                </ol>
                <div class="page-header">
                    <div class="col-md-8">
                        <h1>Lấy tin tự động</h1>
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="javascript:void(0)" onclick="feed_data();" class="btn btn-primary" data-placement="top" data-toggle="tooltip" title="" data-original-title="Lấy tin"><i class="icon-plus"></i> Lấy tin</a>
                        <a  href="javascript:void(0)" onclick="insert_data();" class="btn btn-success btnActionList" data-placement="top" data-toggle="tooltip" title="" data-original-title="Chèn vào Databases"><i class="glyphicon glyphicon-ok-sign"></i> Chèn vào Databases</a>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <!-- end: PAGE TITLE & BREADCRUMB -->
            </div>
        </div>
        <!-- end: PAGE HEADER -->

        <!-- start: PAGE CONTENT -->
        <div class="row">
            <form name="feedForm" method="post">
            <div class="col-md-5">
                <!-- start: TABLE WITH IMAGES PANEL -->
                <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="icon-external-link-sign"></i>
                    Danh sách
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="icon-remove"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                <table class="table table-striped table-bordered table-hover" id="sample-table-2">
                <thead>
                <tr>
                    <th class="center">
                        <div class="checkbox-table">
                            <label>
                                <input type="checkbox" value="1" class="flat-grey" id="feedForm_all_checkbox" onclick="select_all_checkbox(this.checked);" title="Tất cả">
                            </label>
                        </div>
                    </th>
                    <th>Mẫu lấy tin</th>
                    <th>Chèn vào danh mục</th>
                    <th>Sửa</th>
                </tr>
                </thead>
                <tbody>
    <?php foreach($temps as $key=>$value){ ?>
                <tr>
                    <td class="center">
                        <div class="checkbox-table">
                            <label>
                                <input name="temps[<?php echo $key;?>]" value="<?php echo $key;?>" id="temps[<?php echo $key;?>]" type="checkbox" class="flat-grey">
                            </label>
                        </div></td>
                    <td><label for="temps[<?php echo $key;?>]"><?php echo $value['site_name'];?></label>[ <a href="<?php echo $value['url'];?>" target="_blank" title="<?php echo $value['url'];?>">link</a> ]</td>
                    <td><?php echo $value['category_title'];?></td>
                    <td class="center">
                        <a href="declaration_site.php?cmd=edit&id=<?php echo $key;?>" class="btn btn-teal btn-xs tooltips" data-placement="top" data-original-title="Edit"><i class="icon-edit"></i></a>
                    </td>
                </tr>
    <?php }?>
                </tbody>
                </table>
                </div>
                </div>
            <!-- end: TABLE WITH IMAGES PANEL -->
            </div>
            <div class="col-md-7">
                <div align="center"><img src="images/animated_loading.gif" class="loading" style="display: none;"/></div>

                <?php if(isset($items) and $items){ ?>
                    <div class="template-data fr">
                        <h2 style="margin-top:0;">Dữ liệu đã lấy</h2>
                        <ol id="data_result"><?php echo $items;?></ol>
                    </div>
                <?php }?>

                <input type="hidden" name="cmd" value="" id="cmd" />
            </div>
        </div>
        </form>
    <!-- end: PAGE CONTENT-->
    </div>
</div>
<!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            2013 &copy; 123dzo.net
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/blockUI/jquery.blockUI.js"></script>
<script src="assets/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="assets/js/main.js"></script>

<script src="js/crawler.js" type="text/javascript"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function() {
        Main.init();
    });
</script>
</body>
<!-- end: BODY -->
</html>
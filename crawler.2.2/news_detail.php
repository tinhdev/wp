<?php
require 'lib/database.php';
$id=(isset($_REQUEST['id']) and $_REQUEST['id'])?$_REQUEST['id']:0;
$item=DB::fetch('select * from dzo_posts where ID="'.$id.'"');

$arr_map = array(
    'id' => 'ID',
    'image_url' => 'guid',
    'description' => 'post_content',
    'brief' => 'post_excerpt',
    'name' => 'post_title',
);

if (isset($arr_map['image_url'])) $item['image_url'] = $item[$arr_map['image_url']];
if (isset($arr_map['description'])) $item['description'] = $item[$arr_map['description']];
if (isset($arr_map['brief'])) $item['brief'] = $item[$arr_map['brief']];
if (isset($arr_map['name'])) $item['name'] = $item[$arr_map['name']];
if (isset($arr_map['id'])) $item['id'] = $item[$arr_map['id']];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $item['name'];?></title>
<meta name="generator" content="minhtc.net" />
<link href="css/style.css" rel="stylesheet" />
</head>
<body>
<a href="javascript:void(0)" onclick="history.go(-1)">Quay lại</a>
<h1><?php echo $item['name'];?></h1>
<p><strong><?php echo $item['brief'];?></strong></p>
<div><?php echo $item['description'];?></div>
</body>
</html>
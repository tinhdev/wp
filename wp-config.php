<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
$env = getenv('ENVIRONMENT') ? getenv('ENVIRONMENT') : 'production';
$_envdb['production'] = array(
    "sqlserver"     => "127.0.0.1",
    "sqluser"       => "root",
    "sqlpassword"   => "dev!@#$%^",
    "dbname"        => "123share",
);

$_envdb['development'] = array(
    "sqlserver"     => "localhost",
    "sqluser"       => "root",
    "sqlpassword"   => "",
    "dbname"        => "123share",
);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $_envdb[$env]['dbname']);

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', $_envdb[$env]['sqlpassword']);

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

//define('WPCACHEHOME','/var/www/html/dzo/wp-content/plugins/wp-super-cache/');
define('WPCACHEHOME', dirname ( __FILE__ ) . '/wp-content/plugins/wp-super-cache/');
define('WP_CACHE', true); //Added by WP-Cache Manager
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ue^E,nI_.E3lSy!I^Tn8xC}j1b1x%ynHYCq%+ZDYSm@+6PKu|iF,uq,=(I90kr@b');
define('SECURE_AUTH_KEY',  'eBOd$ IfNkFNls66Zt-jJ?S7$o+?CJ?eIEV)/ZI4RSR)*Rhgxk>elVnxw9n+^?BX');
define('LOGGED_IN_KEY',    '.r3(C4(EiibFb^Lo|R=H5%%sO?J_7!**hkq_G4/J=jiX/j/22|&o_vNTzAzmvLB|');
define('NONCE_KEY',        'D0PDcO24u~)v(tiI^q5?&rZg0u7s=!O,(S<<UWz(fyBK9wx&?9_.mALB9Z@K+!^{');
define('AUTH_SALT',        'TW<3J3S}If9T5AqeiUrJg{97VwYvcwgoF8q7K=V6&?BoIbnvolPgd(63JB;k4~R3');
define('SECURE_AUTH_SALT', '+(!_TCto9:8bH`JJo&SPfl@WL,~=zz9tThg&[Dv0?=p X97QELN+u{[)o-__c66D');
define('LOGGED_IN_SALT',   'O)lDHq6cG=~uUm7E?;!0=F%*E8r!.p0W{&M%-Q|b}b[*Q$lp-s;S7cMzW,b=}[5V');
define('NONCE_SALT',       'M+H.l1mH55 x/1Y> k]aGR?0<LCZI/f^eY,4~IUoC[*2x#_&V6/rFfs=y-sFU,#h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dzo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

date_default_timezone_set('Asia/Ho_Chi_Minh');
//$resource_path = 'resources'.'/'.date('Y').'/'.date('m').'/'.date('d');
define('UPLOADS', resources);
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
